﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COR.ObjectFramework
{
    public class OL_ErrorLogDetails
    {
        public  int errorBookingId { get; set; }
        public string errorSource { get; set; }
        public string errorBlock { get; set; }
        public string ErrorMessage { get; set; }
        public int errorCreatedBy { get; set; }

    }
}
