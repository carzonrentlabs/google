﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace COR.ObjectFramework
{
    public class BaseClassObject
    {
        #region Fields Name ...
        private DataTable _objDataTable;
        private DataSet _objDataSet;
        private int? _dbOperationStatus;
        private string _commonMessage;
       
        #endregion
        #region Object Properties ...
        public DataTable ObjDataTable
        {
            get { return _objDataTable; }
            set { _objDataTable = value; }
        }
        public DataSet ObjDataSet
        {
            get { return _objDataSet; }
            set { _objDataSet = value; }
        }
        public int? DbOperationStatus
        {
            get { return _dbOperationStatus; }
            set { _dbOperationStatus = value; }
        }
        public string CommonMessage
        {
            get { return _commonMessage; }
            set { _commonMessage = value; }
        }
        #endregion
    }
}
