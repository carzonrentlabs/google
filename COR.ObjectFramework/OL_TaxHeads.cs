﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COR.ObjectFramework
{
    public class OL_TaxHeads
    {
        public double ServiceTaxPercent { get; set; }
        public double EduCessPercent { get; set; }
        public double HduCessPercent { get; set; }
        public double DSTPercent { get; set; }
        public bool KMWisePackage { get; set; }
        public double SwachhBharatTaxPercent { get; set; }
        public double KrishiKalyanTaxPercent { get; set; }
        public bool GSTEnabledYN { get; set; }
        public double CGSTPercent { get; set; }
        public double SGSTPercent { get; set; }
        public double IGSTPercent { get; set; }
        public int ClientGSTId { get; set; }

    }
}
