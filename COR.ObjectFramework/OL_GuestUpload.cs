﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COR.SolutionFramework;

namespace COR.ObjectFramework
{
    public class OL_GuestUpload : BaseClassObject
    {

        public int ClientCoId { get; set; }
        public int ClientCoAddId { get; set; }
        public string Fname   { get; set; }
        public string Mname  { get; set; }
        public string Lname  { get; set; }
        public string Phone1  { get; set; }
        public string EmailId  { get; set; }
        public string PaymentTerms  { get; set; }
        public string Remarks  { get; set; }
        public string Gender  { get; set; }
        public string EmpCode { get; set; }
        public string Pass { get; set; }
        public int UploadStatus { get; set; }
        public string UploadType { get; set; }
        public string OutStatus { get; set; }
        public int status { get; set; }
        public bool Active { get; set; }
        public bool IsVIP { get; set; }
        public bool PreAuthNotRequire { get; set; }
           
    }
}
