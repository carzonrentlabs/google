﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COR.SolutionFramework;

namespace COR.ObjectFramework
{
    public class OL_Booking : OL_LoginUser
    {
        #region Field Name...
        private int _bookingId;
        private int _pickUpCityId;
        private int _clientCoIndivId;
        private int _facilitatorId;
        private int _sessionId;
        private int _flag;
        private string _serviceType;
        private int _serviceUnitId;
        private string _serviceUnitName;
        private string _paymentMode;
        private int _modleId;
        private DateTime _pickupDate;
        private DateTime _dropOffDate;
        private string _pickupTime;
        private string _dropoffTime;
        private string _pickUpAdd;
        private int _outstationYN;
        private string _airPort;
        private string _flightNo;
        private string _reportContact;
        private string _visitedCitiesID;
        private int _indicatedPkgID;
        private int _indicatedPkgHrsTrue;
        private double _indicatedPkgHrs;
        private double _indicatedPkgKMs;
        private double _indicatedExtraHr;
        private double _indicatedExtraKM;
        private double _indicatedNightStayAmt;
        private double _indicatedOutStnAmt;
        private double _indicatedPrice;
        private double _indicatedPkgRate;
        private double _indicatedDiscountPC;
        private double _IndicatedDepositAmt;
        private int _approvalNo;
        private int _noNight;
        private string _status;
        private string _remarks;
        private int _createdBy;
        private int _contDSID;
        private string _houseNo;
        private string _streetNo;
        private string _landmark;
        private int _pickUpLocation;
        private string _trackid;
        private string _transactionid;
        private string _authorizationid;
        private double _approvalAmt;
        private int _cCType;
        private int _paymentStatus;
        private string _cCNo;
        private string _creditCardExp;
        private int _preAuthNotRequire;
        private string _miscellaneous1;
        private string _miscellaneous2;
        private string _miscellaneous3;
        private string _miscellaneous4;
        private string _miscellaneous5;
        private string _miscellaneous6;
        private string _miscellaneous7;
        private string _pickHour;
        private string _pickMin;
        private string _dropOffHour;
        private string _dropOffMin;
        private float _pLat;
        private float _pLon;
        private float _dLat;
        private float _dLon;
        private string _dAddress;
        private int _customYN;
        private int _noOfPac;
        private string _visitedCities;
        private int _NewPreAuthStatus;
        private int _customPackageId;
        private string _guestName;
        private string _guestEmailId;
        private string _guestPhoneNo;
        private string _facilitaorName;
        private string _facilitatorEmailId;
        private string _faclitatorPhoneNo;
        private string _EXPYYMM;
        private int _bookingApprovalYN;
        private string _pickUpCityName;
        private string _carModelName;
        private bool _mailStatus;
        private bool _smsStatus;
        private int _smsYN;
        private string _subsidiary;
        private bool _isPackageAvailable;
        private bool _isCustomYN;
        private int _packageType;
        private int _continuousBooking;
        private string _continuousBookingFromDate;
        private string _continuousBookingToDate;
        private string _clientCoName;
        private int _bookingScheme;
        private bool _escort;
        private string airportType;
        private bool _guestRegisteredYN;
        private string _authCode;
        private string _PGId;
        private DateTime _modifyDate;
        private int _modifiedBy;
        private int _modifiedStatus;
        private string _cancelReason;
        private string _additionalEmaiId;
        private string _additionalMobileNo;
        private int _bookerID;
        private int? _ImplantId;
        private double _cgstTaxPercent;
        private double _sgstTaxPercent;
        private double _igstTaxPercent;
        private double _cgstTaxAmt;
        private double _sgstTaxAmt;
        private double _igstTaxAmt;
        private int _clientGSTId;
        private double _indicatedGSTSurChargeAmount;

        #endregion

        #region Object Properties...
        public double IndicatedGSTSurChargeAmount
        {
            get { return _indicatedGSTSurChargeAmount; }
            set { _indicatedGSTSurChargeAmount = value; }
        }
        public double CGSTTaxPercent
        {
            get { return _cgstTaxPercent; }
            set { _cgstTaxPercent = value; }
        }
        public double SGSTTaxPercent
        {
            get { return _sgstTaxPercent; }
            set { _sgstTaxPercent = value; }
        }
        public double IGSTTaxPercent
        {
            get { return _igstTaxPercent; }
            set { _igstTaxPercent = value; }
        }
        public double CGSTTaxAmt
        {
            get { return _cgstTaxAmt; }
            set { _cgstTaxAmt = value; }
        }
        public double SGSTTaxAmt
        {
            get { return _sgstTaxAmt; }
            set { _sgstTaxAmt = value; }
        }
        public double IGSTTaxAmt
        {
            get { return _igstTaxAmt; }
            set { _igstTaxAmt = value; }
        }
        public int ClientGSTId
        {
            get { return _clientGSTId; }
            set { _clientGSTId = value; }
        }
        public DateTime ModifyDate
        {
            get { return _modifyDate; }
            set { _modifyDate = value; }
        }
        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }
        public int BookingId
        {
            get
            {
                return _bookingId;
            }
            set
            {
                if (GeneralUtility.IsInteger(value) && value > 0)
                {
                    _bookingId = value;
                }

                else
                {
                    throw new Exception("Invalid Booking Id");
                }
            }
        }
        public int PickUpCityId
        {
            get
            {
                return _pickUpCityId;
            }
            set
            {
                _pickUpCityId = value;
            }
        }
        public int FacilitatorId
        {
            get
            {
                return _facilitatorId;
            }
            set
            {
                _facilitatorId = value;
            }
        }
        public int SessionId
        {
            get
            {
                return _sessionId;
            }
            set
            {
                _sessionId = value;
            }
        }
        public int Flag
        {
            get
            {
                return _flag;
            }
            set
            {
                _flag = value;
            }
        }
        public int ClientCoIndivId
        {
            get
            {
                return _clientCoIndivId;
            }
            set
            {
                _clientCoIndivId = value;
            }
        }
        public string ServiceType
        {
            get
            {
                return _serviceType;
            }
            set
            {
                _serviceType = value;
            }
        }
        public int ServiceUnitId
        {
            get
            {
                return _serviceUnitId;
            }
            set
            {
                _serviceUnitId = value;
            }
        }
        public string PaymentMode
        {
            get
            {
                return _paymentMode;
            }
            set
            {
                _paymentMode = value;
            }
        }
        public int ModleId
        {
            get
            {
                return _modleId;
            }
            set
            {
                _modleId = value;
            }
        }
        public DateTime PickupDate
        {
            get
            {
                return _pickupDate;
            }
            set
            {
                _pickupDate = value;
            }
        }
        public DateTime DropOffDate
        {
            get
            {
                return _dropOffDate;
            }
            set
            {
                _dropOffDate = value;
            }
        }
        public string PickupTime
        {
            get
            {
                return _pickupTime;
            }
            set
            {
                _pickupTime = value;
            }
        }
        public string DropoffTime
        {
            get
            {
                return _dropoffTime;
            }
            set
            {
                _dropoffTime = value;
            }
        }
        public string PickUpAdd
        {
            get
            {
                return _pickUpAdd;
            }
            set
            {
                _pickUpAdd = value;
            }
        }
        public int OutstationYN
        {
            get
            {
                return _outstationYN;
            }
            set
            {
                _outstationYN = value;
            }
        }
        public string AirPort
        {
            get
            {
                return _airPort;
            }
            set
            {
                _airPort = value;
            }
        }
        public string FlightNo
        {
            get
            {
                return _flightNo;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _flightNo = string.Empty;
                }
                else
                {
                    _flightNo = value;
                }

            }
        }
        public string ReportContact
        {
            get
            {
                return _reportContact;
            }
            set
            {
                _reportContact = value;
            }
        }

        public string VisitedCitiesID
        {
            get
            {
                return _visitedCitiesID;
            }
            set
            {
                _visitedCitiesID = value;
            }
        }
        public int IndicatedPkgID
        {
            get
            {
                return _indicatedPkgID;
            }
            set
            {
                _indicatedPkgID = value;
            }
        }
        public double IndicatedPkgHrs
        {
            get
            {
                return _indicatedPkgHrs;
            }
            set
            {
                _indicatedPkgHrs = value;
            }
        }
        public double IndicatedPkgKMs
        {
            get
            {
                return _indicatedPkgKMs;
            }
            set
            {
                _indicatedPkgKMs = value;
            }
        }
        public double IndicatedExtraHr
        {
            get
            {
                return _indicatedExtraHr;
            }
            set
            {
                _indicatedExtraHr = value;
            }
        }
        public double IndicatedExtraKM
        {
            get
            {
                return _indicatedExtraKM;
            }
            set
            {
                _indicatedExtraKM = value;
            }
        }
        public double IndicatedNightStayAmt
        {
            get
            {
                return _indicatedNightStayAmt;
            }
            set
            {
                _indicatedNightStayAmt = value;
            }
        }
        public double IndicatedOutStnAmt
        {
            get
            {
                return _indicatedOutStnAmt;
            }
            set
            {
                _indicatedOutStnAmt = value;
            }
        }
        public double IndicatedPrice
        {
            get
            {
                return _indicatedPrice;
            }
            set
            {
                _indicatedPrice = value;
            }
        }
        public int IndicatedPkgHrsTrue
        {
            get
            {
                return _indicatedPkgHrsTrue;
            }
            set
            {
                _indicatedPkgHrsTrue = value;
            }
        }
        public int ApprovalNo
        {
            get
            {
                return _approvalNo;
            }
            set
            {
                _approvalNo = value;
            }
        }
        public int NoNight
        {
            get
            {
                return _noNight;
            }
            set
            {
                _noNight = value;
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }
        public string Remarks
        {
            get
            {
                return _remarks;
            }
            set
            {
                _remarks = value;
            }
        }
        public int CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                _createdBy = value;
            }
        }
        public int ContDSID
        {
            get
            {
                return _contDSID;
            }
            set
            {
                _contDSID = value;
            }
        }

        public string HouseNo
        {
            get
            {
                return _houseNo;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _houseNo = null;
                }
                else
                {
                    _houseNo = value;
                }
            }
        }
        public string StreetNo
        {
            get
            {
                return _streetNo;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _streetNo = null;
                }
                else
                {
                    _streetNo = value;
                }
            }
        }
        public string Landmark
        {
            get
            {
                return _landmark;
            }
            set
            {
                _landmark = value;
            }
        }
        public int PickUpLocation
        {
            get
            {
                return _pickUpLocation;
            }
            set
            {
                if (GeneralUtility.IsInteger(value))
                {
                    _pickUpLocation = value;
                }
                else
                {
                    throw new Exception("Invalid location id.");
                }

            }
        }
        public string Trackid
        {
            get
            {
                return _trackid;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _trackid = null;
                }
                else
                {
                    _trackid = value;
                }

            }
        }
        public string Transactionid
        {
            get
            {
                return _transactionid;
            }
            set
            {
                if (string.IsNullOrEmpty(value.ToString()))
                {
                    _transactionid = null;
                }
                else
                {
                    _transactionid = value;
                }

            }
        }
        public string Authorizationid
        {
            get
            {
                return _authorizationid;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _authorizationid = null;
                }
                else
                {
                    _authorizationid = value;
                }
            }
        }
        public double ApprovalAmt
        {
            get
            {
                return _approvalAmt;
            }
            set
            {
                _approvalAmt = value;
            }
        }

        public int CCType
        {
            get
            {
                return _cCType;
            }
            set
            {
                _cCType = value;
            }
        }
        public int PaymentStatus
        {
            get
            {
                return _paymentStatus;
            }
            set
            {
                _paymentStatus = value;
            }
        }
        public string CCNo
        {
            get
            {
                return _cCNo;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _cCNo = null;
                }
                else
                {
                    _cCNo = value;
                }

            }
        }
        public string CreditCardExp
        {
            get
            {
                return _creditCardExp;
            }
            set
            {
                _creditCardExp = value;
            }
        }
        public int PreAuthNotRequire
        {
            get
            {
                return _preAuthNotRequire;
            }
            set
            {
                if (string.IsNullOrEmpty(value.ToString()))
                {
                    _preAuthNotRequire = 0;
                }
                else
                {
                    _preAuthNotRequire = value;
                }
            }
        }
        public string Miscellaneous1
        {
            get
            {
                return _miscellaneous1;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _miscellaneous1 = null;
                }
                else
                {
                    _miscellaneous1 = value;
                }


            }
        }
        public string Miscellaneous2
        {
            get
            {
                return _miscellaneous2;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _miscellaneous2 = null;
                }
                else
                {
                    _miscellaneous2 = value;
                }

            }
        }
        public string Miscellaneous3
        {
            get
            {
                return _miscellaneous3;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _miscellaneous3 = null;
                }
                else
                {
                    _miscellaneous3 = value;
                }

            }
        }
        public string Miscellaneous4
        {
            get
            {
                return _miscellaneous4;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _miscellaneous4 = null;
                }
                else
                {
                    _miscellaneous4 = value;
                }

            }
        }
        public string Miscellaneous5
        {
            get
            {
                return _miscellaneous5;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _miscellaneous5 = null;
                }
                else
                {
                    _miscellaneous5 = value;
                }
            }
        }
        public string Miscellaneous6
        {
            get
            {
                return _miscellaneous6;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _miscellaneous6 = null;
                }
                else
                {
                    _miscellaneous6 = value;
                }

            }
        }
        public string Miscellaneous7
        {
            get
            {
                return _miscellaneous7;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _miscellaneous7 = null;
                }
                else
                {
                    _miscellaneous7 = value;
                }

            }
        }
        public string PickHour
        {
            get
            {
                return _pickHour;
            }
            set
            {
                _pickHour = value;
            }
        }
        public string PickMin
        {
            get
            {
                return _pickMin;
            }
            set
            {
                _pickMin = value;
            }
        }
        public string DropOffHour
        {
            get
            {
                return _dropOffHour;
            }
            set
            {
                _dropOffHour = value;
            }
        }
        public string DropOffMin
        {
            get
            {
                return _dropOffMin;
            }
            set
            {
                _dropOffMin = value;
            }
        }
        public float PLat
        {
            get
            {
                return _pLat;
            }
            set
            {
                _pLat = value;
            }
        }
        public float PLon
        {
            get
            {
                return _pLon;
            }
            set
            {
                _pLon = value;
            }
        }
        public float DLat
        {
            get
            {
                return _dLat;
            }
            set
            {
                _dLat = value;
            }
        }
        public float DLon
        {
            get
            {
                return _dLon;
            }
            set
            {
                _dLon = value;
            }
        }
        public int CustomYN
        {
            get
            {
                return _customYN;
            }
            set
            {
                if (GeneralUtility.IsInteger(value))
                {
                    _customYN = value;
                }
                else
                {
                    throw new Exception("Invalid Custom Id.");
                }

            }
        }
        public int NoOfPac
        {
            get
            {
                return _noOfPac;
            }
            set
            {
                _noOfPac = value;
            }
        }
        public string VisitedCities
        {
            get
            {
                return _visitedCities;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _visitedCities = null;
                }
                else
                {
                    _visitedCities = value;
                }

            }
        }
        public int NewPreAuthStatus
        {
            get
            {
                return _NewPreAuthStatus;
            }
            set
            {
                _NewPreAuthStatus = value;
            }
        }
        public int CustomPackageId
        {
            get
            {
                return _customPackageId;
            }
            set
            {
                if (GeneralUtility.IsNull(value))
                {
                    //_customPackageId = null;
                    throw new Exception("Invalid custom package id.");
                }
                else
                {
                    _customPackageId = value;
                }

            }
        }
        public double IndicatedPkgRate
        {
            get
            {
                return _indicatedPkgRate;
            }
            set
            {
                _indicatedPkgRate = value;
            }
        }
        public string ServiceUnitName
        {
            get
            {
                return _serviceUnitName;
            }
            set
            {
                _serviceUnitName = value;
            }
        }
        public string GuestName
        {
            get
            {
                return _guestName;
            }
            set
            {
                _guestName = value;
            }
        }
        public string FaclitatorPhoneNo
        {
            get
            {
                return _faclitatorPhoneNo;
            }
            set
            {
                _faclitatorPhoneNo = value;
            }
        }
        public string FacilitatorEmailId
        {
            get
            {
                return _facilitatorEmailId;
            }
            set
            {
                _facilitatorEmailId = value;
            }
        }
        public string FacilitaorName
        {
            get
            {
                return _facilitaorName;
            }
            set
            {
                _facilitaorName = value;
            }
        }

        public string GuestPhoneNo
        {
            get
            {
                return _guestPhoneNo;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _guestPhoneNo = null;
                }
                else
                {
                    _guestPhoneNo = value;
                }

            }
        }
        public string GuestEmailId
        {
            get
            {
                return _guestEmailId;
            }
            set
            {
                string errorMessage;
                if (GeneralUtility.ValidEmailAddress(value, out errorMessage))
                {
                    _guestEmailId = value;
                }
                else
                {
                    throw new Exception(errorMessage);
                }

            }
        }

        public double IndicatedDiscountPC
        {
            get
            {
                return _indicatedDiscountPC;
            }
            set
            {
                _indicatedDiscountPC = value;
            }
        }
        public double IndicatedDepositAmt
        {
            get
            {
                return _IndicatedDepositAmt;
            }
            set
            {
                _IndicatedDepositAmt = value;
            }
        }
        public string EXPYYMM
        {
            get
            {
                return _EXPYYMM;
            }
            set
            {
                _EXPYYMM = value;
            }
        }
        public string DAddress
        {
            get
            {
                return _dAddress;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _dAddress = null;
                }
                else
                {
                    _dAddress = value;
                }
            }
        }
        public int BookingApprovalYN
        {
            get
            {
                return _bookingApprovalYN;
            }
            set
            {
                _bookingApprovalYN = value;
            }
        }
        public string PickUpCityName
        {
            get
            {
                return _pickUpCityName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _pickUpCityName = null;
                }
                else
                {
                    _pickUpCityName = value;
                }

            }
        }
        public string CarModelName
        {
            get
            {
                return _carModelName;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _carModelName = null;
                }
                else
                {
                    _carModelName = value;
                }

            }
        }
        public bool MailStatus
        {
            get
            {
                return _mailStatus;
            }
            set
            {
                if (GeneralUtility.IsBoolean(value))
                {
                    _mailStatus = value;
                }
                else
                {
                    throw new Exception("Invalid mail content or mail id.");
                }


            }
        }
        public bool SmsStatus
        {
            get
            {
                return _smsStatus;
            }
            set
            {
                _smsStatus = value;
            }
        }
        public int SmsYN
        {
            get
            {
                return _smsYN;
            }
            set
            {
                _smsYN = value;
            }
        }
        public string Subsidiary
        {
            get
            {
                return _subsidiary;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _subsidiary = null;
                }
                else
                {
                    _subsidiary = value;
                }
            }
        }
        public bool IsPackageAvailable
        {
            get
            {
                return _isPackageAvailable;
            }
            set
            {
                if (GeneralUtility.IsBoolean(value))
                {
                    _isPackageAvailable = value;
                }
                else
                {
                    throw new Exception("Invalid Value.");
                }

            }
        }
        public bool IsCustomYN
        {
            get
            {
                return _isCustomYN;
            }
            set
            {
                if (GeneralUtility.IsBoolean(value))
                {
                    _isCustomYN = value;
                }
                else
                {
                    throw new Exception("Invalid Value.");
                }
            }
        }
        public int PackageType
        {
            get
            {
                return _packageType;
            }
            set
            {
                _packageType = value;
            }
        }
        public int ContinuousBooking
        {
            get
            {
                return _continuousBooking;
            }
            set
            {
                _continuousBooking = value;
            }
        }
        public string ContinuousBookingFromDate
        {
            get
            {
                return _continuousBookingFromDate;
            }
            set
            {
                _continuousBookingFromDate = value;
            }
        }
        public string ContinuousBookingToDate
        {
            get
            {
                return _continuousBookingToDate;
            }
            set
            {
                _continuousBookingToDate = value;
            }
        }
        public string ClientCoName
        {
            get
            {
                return _clientCoName;
            }
            set
            {
                _clientCoName = value;
            }
        }

        public int BookingScheme
        {
            get { return _bookingScheme; }
            set { _bookingScheme = value; }
        }
        public bool Escort
        {
            get { return _escort; }
            set { _escort = value; }
        }
        public string AirportType
        {
            get { return airportType; }
            set
            {

                if (string.IsNullOrEmpty(value.ToString()))
                {
                    airportType = null;
                }
                else
                { airportType = value; }

            }
        }
        public bool GuestRegisteredYN
        {
            get { return _guestRegisteredYN; }
            set { _guestRegisteredYN = value; }
        }
        public string AuthCode
        {
            get { return _authCode; }

            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _authCode = value;
                }
                else
                {
                    _authCode = null;
                }
            }
        }
        public string PGId
        {
            get { return _PGId; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _PGId = value;
                }
                else
                {
                    _PGId = null;
                }
            }
        }
        public int ModifiedStatus
        {
            get { return _modifiedStatus; }
            set { _modifiedStatus = value; }
        }

        public string CancelReason
        {
            get { return _cancelReason; }
            set
            {
                if (string.IsNullOrEmpty(value.ToString()))
                {
                    _cancelReason = null;
                    throw new Exception("Enter cancel reason");

                }
                else
                {
                    _cancelReason = value;
                }

            }
        }

        public string AdditionalMobileNo
        {
            get { return _additionalMobileNo; }
            set { _additionalMobileNo = value; }
        }


        public string AdditionalEmaiId
        {
            get { return _additionalEmaiId; }
            set { _additionalEmaiId = value; }
        }

        public int BookerID
        {
            get { return _bookerID; }
            set { _bookerID = value; }
        }
        public int? ImplantId
        {
            get { return _ImplantId; }
            set { _ImplantId = value; }
        }

        #endregion
    }
}