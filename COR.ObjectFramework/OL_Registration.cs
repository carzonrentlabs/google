﻿//===============================================================================
//Created By :	<Krishna Kumar>
//Create date:  <20-06-2015>
//Description:	<For define propertyof Registration >
//===============================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COR.SolutionFramework;

namespace COR.ObjectFramework
{
    public class OL_Registration
    {
        private string _firstName = String.Empty;
        private string _lastName = String.Empty;
        private string _emailId = String.Empty;
        private string _mobileNo = String.Empty;
        private string _employeeCode = String.Empty;
        private string _password = String.Empty;
        private string _action = string.Empty;
        private string _ccno = string.Empty;
        private int _clientIndivId = 0;
        private int _clientCoId = 0;
        private int _status = 0;
        private int _cctype = 0;
        public string Address { get; set; }
        public bool isPaymateCorporateModule { get; set; }
        public bool _autoredirect { get; set; }

        public string PaymentTerms { get; set; }
        public string Gender { get; set; }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string EmailId
        {
            get { return _emailId; }
            set { _emailId = value; }
        }
        public string MobileNo
        {
            get { return _mobileNo; }
            set { _mobileNo = value; }
        }
        public string EmployeeCode
        {
            get { return _employeeCode; }
            set { _employeeCode = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        public string Action
        {
            get { return _action; }
            set { _action = value; }
        }
        public string CCNo
        {
            get { return _ccno; }
            set { _ccno = value; }
        }
        public int ClientIndivId
        {
            get { return _clientIndivId; }
            set { _clientIndivId = value; }
        }
        public int ClientCoId
        {
            get { return _clientCoId; }
            set { _clientCoId = value; }
        }
        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public int CCType
        {
            get { return _cctype; }
            set { _cctype = value; }
        }

        public bool AutoRedirect
        {
            get { return _autoredirect; }
            set { _autoredirect = value; }
        }
    }
}
