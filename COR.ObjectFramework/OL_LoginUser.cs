﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COR.ObjectFramework
{
    public class OL_LoginUser :BaseClassObject
    {
        public int SysUserId { get; set; }
        public string LoginId { get; set; }
        public string Password { get; set; }
        public string LoginStatus { get; set; }
        public string UserName { get; set; }
        public string UserCityName { get; set; }
        public int ClientCoIndivID { get; set; }
        public int ClientCoId { get; set; }
        public string ReceiverId { get; set; }
        public string EmailContent { get; set; }
        public string Subject { get; set; }
        public string AddBCC { get; set; }
        public string AddCC { get; set; }
        public bool ClientRegisteredYN { get; set; }
        public bool CCRegisteredYN { get; set; }
        public int Implant { get; set; }       
        public int SearchOption { get; set; }
        public string GuestSearchKey { get; set; }
        public string GuestFirstName { get; set; }
        public string GuestLastName { get; set; }       
    }
}
