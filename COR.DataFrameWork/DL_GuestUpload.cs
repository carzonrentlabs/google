﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COR.ObjectFramework;
using COR.SolutionFramework;
using System.Data;
using System.Data.SqlClient;

namespace COR.DataFrameWork
{
    public class DL_GuestUpload
    {
        public OL_GuestUpload GetClientName(OL_GuestUpload objUploadguestOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[0];
                objUploadguestOL.ObjDataTable = SqlHelper.ExecuteDatatable("GetClientCoID", param);
                return objUploadguestOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objUploadguestOL;
        }



        public OL_GuestUpload GetClientCoAddID(OL_GuestUpload objUploadguestOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoID", objUploadguestOL.ClientCoId);
                objUploadguestOL.ObjDataTable = SqlHelper.ExecuteDatatable("GetClientCoAddID", param);
                return objUploadguestOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objUploadguestOL;
        }


        public OL_GuestUpload BulkGuest_Upload(OL_GuestUpload objUploadguestOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[17];
                param[0] = new SqlParameter("@ClientCoId", objUploadguestOL.ClientCoId);
                param[1] = new SqlParameter("@ClientCoAddId", objUploadguestOL.ClientCoAddId);
                param[2] = new SqlParameter("@Fname", objUploadguestOL.Fname);
                param[3] = new SqlParameter("@Mname", objUploadguestOL.Mname);
                param[4] = new SqlParameter("@Lname", objUploadguestOL.Lname);
                param[5] = new SqlParameter("@Phone1", objUploadguestOL.Phone1);
                param[6] = new SqlParameter("@EmailId", objUploadguestOL.EmailId);
                param[7] = new SqlParameter("@PaymentTerms", objUploadguestOL.PaymentTerms);
                param[8] = new SqlParameter("@Remarks", objUploadguestOL.Remarks);
                param[9] = new SqlParameter("@Active", objUploadguestOL.Active);
                param[10] = new SqlParameter("@IsVIP", objUploadguestOL.IsVIP);
                param[11] = new SqlParameter("@PreAuthNotRequire", objUploadguestOL.PreAuthNotRequire);
                param[12] = new SqlParameter("@Gender", objUploadguestOL.Gender);
                param[13] = new SqlParameter("@EmpCode", objUploadguestOL.EmpCode);
                param[14] = new SqlParameter("@UploadStatus", objUploadguestOL.UploadStatus);
                param[15] = new SqlParameter("@UploadType", objUploadguestOL.UploadType);
                param[16] = new SqlParameter("@OutStatus", objUploadguestOL.OutStatus);
               // param[17] = new SqlParameter("@Pass", objUploadguestOL.Pass);
                
                param[16].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Bl_SP_BulkGuest_Upload", param);
                objUploadguestOL.OutStatus = param[16].Value.ToString();
                return objUploadguestOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }


        //public string BulkGuest_Upload(int ClientCoId, int ClientCoAddId, string Fname, string Mname, string Lname, string Phone1, string EmailId, string PaymentTerms, string Remarks, Boolean Active, Boolean IsVIP, Boolean PreAuthNotRequire, string Gender, string EmpCode, int UploadStatus, string UploadType)
        //{
        //    SqlParameter[] sqlParam = new SqlParameter[16];

        //    sqlParam[0] = new SqlParameter("@ClientCoId", ClientCoId);
        //    sqlParam[1] = new SqlParameter("@ClientCoAddId", ClientCoAddId);
        //    sqlParam[2] = new SqlParameter("@Fname", Fname);
        //    sqlParam[3] = new SqlParameter("@Mname", Mname);
        //    sqlParam[4] = new SqlParameter("@Lname", Lname);
        //    sqlParam[5] = new SqlParameter("@Phone1", Phone1);
        //    sqlParam[6] = new SqlParameter("@EmailId", EmailId);
        //    sqlParam[7] = new SqlParameter("@PaymentTerms", PaymentTerms);
        //    sqlParam[8] = new SqlParameter("@Remarks", Remarks);
        //    sqlParam[9] = new SqlParameter("@Active", Active);
        //    sqlParam[10] = new SqlParameter("@IsVIP", IsVIP);
        //    sqlParam[11] = new SqlParameter("@PreAuthNotRequire", PreAuthNotRequire);
        //    //sqlParam[12] = new SqlParameter("@FacilitatorId", FacilitatorId);
        //    sqlParam[12] = new SqlParameter("@Gender", Gender);
        //    sqlParam[13] = new SqlParameter("@EmpCode", EmpCode);
        //    sqlParam[14] = new SqlParameter("@UploadStatus", UploadStatus);
        //    sqlParam[15] = new SqlParameter("@UploadType", UploadType);

        //    DataSet ds = DbConnect.GetDataSet("Bl_SP_BulkGuest_Upload", sqlParam);
        //    return ds.Tables[0].Rows[0]["Status"].ToString();
        //}

        public OL_GuestUpload GetTopStatus(OL_GuestUpload objUploadguestOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[0];
                objUploadguestOL.ObjDataTable = SqlHelper.ExecuteDatatable("Bl_sp_getBulkStatus", param);
                return objUploadguestOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objUploadguestOL;
        }

        public OL_GuestUpload GetDetailAftrSave(OL_GuestUpload objUploadguestOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[0];
                objUploadguestOL.ObjDataTable = SqlHelper.ExecuteDatatable("bl_sp_getGuestDetails", param);
                return objUploadguestOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objUploadguestOL;
        }


        public OL_GuestUpload SendBulkGuestUploadMailDetails(OL_GuestUpload objUploadguestOL)
        {
            try
            {
                DataTable dtGuestDetail;
                dtGuestDetail = new DataTable();
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@ClientCoAddId", objUploadguestOL.ClientCoAddId);
                param[1] = new SqlParameter("@Phone1", objUploadguestOL.Phone1);
                dtGuestDetail = SqlHelper.ExecuteDatatable("BL_SP_SendBulkGuestUploadMailDetails", param);
                if (dtGuestDetail.Rows.Count > 0)
                {
                    objUploadguestOL.Fname = Convert.ToString(dtGuestDetail.Rows[0]["FName"]);
                    objUploadguestOL.Lname = Convert.ToString(dtGuestDetail.Rows[0]["LName"]);
                    objUploadguestOL.Phone1 = Convert.ToString(dtGuestDetail.Rows[0]["Phone1"]);
                    objUploadguestOL.EmailId = Convert.ToString(dtGuestDetail.Rows[0]["EmailID"]);
                    objUploadguestOL.ClientCoAddId = Convert.ToInt32(dtGuestDetail.Rows[0]["ClientCoAddId"]);
                }
                return objUploadguestOL;
            }
            catch (Exception Ex)
            {

                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }

        }
    }
}
