﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COR.ObjectFramework;
using COR.SolutionFramework;
using System.Data;
using System.Data.SqlClient;

namespace COR.DataFrameWork
{

    public class DL_Booking
    {

        public OL_Booking GetCityName(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@SessionId", objBookingOL.ClientCoId);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Prc_GetCityContractByFunction", param);
                return objBookingOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }
       
        public OL_Booking GetFacilitatorName(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@ClientCoID", objBookingOL.ClientCoId);
                param[1] = new SqlParameter("@FacilitatorID", objBookingOL.FacilitatorId);
                param[2] = new SqlParameter("@flag", objBookingOL.Flag);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Proc_SelFacilitator_CORIntFacilitatorMaster", param);
                return objBookingOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }



        public OL_Booking GetCarModel(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@CityID", objBookingOL.PickUpCityId);
                param[1] = new SqlParameter("@ClientID", objBookingOL.ClientCoId);
                param[2] = new SqlParameter("@Flag", 2);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("ProcBook_Corweb_GetModelsAvailable", param);
            }
            catch (Exception Ex)
            {

                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }

        public OL_Booking GetPreAuthDetails()
        {
            OL_Booking objBookingOL = new OL_Booking();
            try
            {
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("GetPreAuthDetails");
            }
            catch (Exception Ex)
            {

                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }
        public OL_Booking GetServiceUnitId(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@CityID", objBookingOL.PickUpCityId);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Prc_GetServiceUnitID", param);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }

        public OL_Booking CalculateIndicatedPrice(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[9];
                param[0] = new SqlParameter("@ServiceType", objBookingOL.ServiceType);
                param[1] = new SqlParameter("@PickupCityId", objBookingOL.PickUpCityId);
                param[2] = new SqlParameter("@ClientID", objBookingOL.ClientCoId);
                param[3] = new SqlParameter("@ClientCoIndiv", objBookingOL.ClientCoIndivID);
                param[4] = new SqlParameter("@PickUpdate", objBookingOL.PickupDate);
                param[5] = new SqlParameter("@DropOffDate", objBookingOL.DropOffDate);
                param[6] = new SqlParameter("@PickupTime", objBookingOL.PickupTime);
                param[7] = new SqlParameter("@DropOfftime", objBookingOL.DropoffTime);
                param[8] = new SqlParameter("@CarModelId", objBookingOL.ModleId);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Prc_CORPackageCalCulation", param);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }
        public OL_Booking GetIndividualClientDetails(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@ClientCoIndivId", objBookingOL.ClientCoIndivID);
                param[1] = new SqlParameter("@ClientCoId", objBookingOL.ClientCoId);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Prc_CORGetIndividualClientDetails", param);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }

        public OL_Booking CreateBooking(OL_Booking objBookingOL)
        {
            try
            {

                SqlParameter[] param = new SqlParameter[52];
                param[0] = new SqlParameter("@BookIndivID", "C" + objBookingOL.ClientCoIndivID.ToString());
                param[1] = new SqlParameter("@Service", objBookingOL.ServiceType);
                param[2] = new SqlParameter("@PaymentMode", objBookingOL.PaymentMode);
                param[3] = new SqlParameter("@PickUpCityID", objBookingOL.PickUpCityId);
                param[4] = new SqlParameter("@ModelID", objBookingOL.ModleId);
                param[5] = new SqlParameter("@PickUpDate", objBookingOL.PickupDate);
                param[6] = new SqlParameter("@PickUpTime", objBookingOL.PickupTime);
                param[7] = new SqlParameter("@DropOffDate", objBookingOL.DropOffDate);
                param[8] = new SqlParameter("@DropOffTime", objBookingOL.DropoffTime);
                param[9] = new SqlParameter("@PickUpAdd", objBookingOL.PickUpAdd);
                param[10] = new SqlParameter("@OutstationYN", objBookingOL.OutstationYN);
                param[11] = new SqlParameter("@AirportType", objBookingOL.AirportType);
                param[12] = new SqlParameter("@FlgtNo", objBookingOL.FlightNo);
                param[13] = new SqlParameter("@IndicatedPkgID", objBookingOL.IndicatedPkgID);
                param[14] = new SqlParameter("@IndicatedPkgHrsTrue", objBookingOL.IndicatedPkgHrsTrue);
                param[15] = new SqlParameter("@IndicatedPkgHrs", objBookingOL.IndicatedPkgHrs);
                param[16] = new SqlParameter("@IndicatedPkgKMs", objBookingOL.IndicatedPkgKMs);
                param[17] = new SqlParameter("@IndicatedExtraHr", objBookingOL.IndicatedExtraHr);
                param[18] = new SqlParameter("@IndicatedExtraKM", objBookingOL.IndicatedExtraKM);
                param[19] = new SqlParameter("@IndicatedNightStayAmt", objBookingOL.IndicatedNightStayAmt);
                param[20] = new SqlParameter("@IndicatedOutStnAmt", objBookingOL.IndicatedOutStnAmt);
                param[21] = new SqlParameter("@IndicatedPrice", objBookingOL.IndicatedPrice);
                param[22] = new SqlParameter("@IndicatedDiscPC", objBookingOL.IndicatedDiscountPC);
                param[23] = new SqlParameter("@ServiceUnitID", objBookingOL.ServiceUnitId);
                param[24] = new SqlParameter("@NoNight", objBookingOL.NoNight);
                param[25] = new SqlParameter("@Status", "X");
                param[26] = new SqlParameter("@Remarks", objBookingOL.Remarks);
                param[27] = new SqlParameter("@CreatedBy", objBookingOL.CreatedBy);
                param[28] = new SqlParameter("@trackid", objBookingOL.Trackid);
                param[29] = new SqlParameter("@transactionid", objBookingOL.Transactionid);
                param[30] = new SqlParameter("@authorizationid", objBookingOL.Authorizationid);
                param[31] = new SqlParameter("@CCType", objBookingOL.CCType);
                param[32] = new SqlParameter("@PaymentStatus", objBookingOL.PaymentStatus);
                param[33] = new SqlParameter("@CCNo", objBookingOL.CCNo);
                param[34] = new SqlParameter("@bitPreAuthChk", objBookingOL.PreAuthNotRequire);
                param[35] = new SqlParameter("@CustomPkgYN", objBookingOL.CustomYN);
                param[36] = new SqlParameter("@ApprovalAmt", objBookingOL.ApprovalAmt);
                param[37] = new SqlParameter("@EscortRequestYN", objBookingOL.Escort);
                param[38] = new SqlParameter("@AdditionalEmailId", objBookingOL.AdditionalEmaiId);
                param[39] = new SqlParameter("@AdditionalMobileNo", objBookingOL.AdditionalMobileNo);
                param[40] = new SqlParameter("@PLat", objBookingOL.PLat);
                param[41] = new SqlParameter("@PLon", objBookingOL.PLon);
                param[42] = new SqlParameter("@FacilitatorID", objBookingOL.FacilitatorId);
                param[43] = new SqlParameter("@ImplantId", objBookingOL.ImplantId);

                param[44] = new SqlParameter("@CGSTTaxAmt", objBookingOL.CGSTTaxAmt);
                param[45] = new SqlParameter("@SGSTTaxAmt", objBookingOL.SGSTTaxAmt);
                param[46] = new SqlParameter("@IGSTTaxAmt", objBookingOL.IGSTTaxAmt);
                param[47] = new SqlParameter("@CGSTTaxPercent", objBookingOL.CGSTTaxPercent);
                param[48] = new SqlParameter("@SGSTTaxPercent", objBookingOL.SGSTTaxPercent);
                param[49] = new SqlParameter("@IGSTTaxPercent", objBookingOL.IGSTTaxPercent);
                param[50] = new SqlParameter("@ClientGSTId", objBookingOL.ClientGSTId);
                param[51] = new SqlParameter("@IndicatedGSTSurchargeAmount", objBookingOL.IndicatedGSTSurChargeAmount);
                object bookingId;
                bookingId = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Proc_CORBookingCreation_GST", param);
                //bookingId = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Proc_CORBookingCreation", param);
                
                //bookingId = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Proc_CORBookingCreation_New", param);
                objBookingOL.BookingId = Convert.ToInt32(bookingId);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }

        public OL_Booking CreateTackId(OL_Booking objBookingOL)
        {
            string trackId = string.Empty;
            try
            {
                object trackid;
                trackid = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "ProcUniqueTrackID");
                objBookingOL.Trackid = trackid.ToString();
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }
        public OL_Booking CheckGuestRegistration(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoIndivId", objBookingOL.ClientCoIndivID);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Prc_GetRegisteredStatus", param);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;

        }
        public OL_Booking UpdatePreAuthStatus(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[6];
                param[0] = new SqlParameter("@BookingID", objBookingOL.BookingId);
                param[1] = new SqlParameter("@TransactionID", objBookingOL.Transactionid);
                param[2] = new SqlParameter("@CCNo", objBookingOL.CCNo);
                param[3] = new SqlParameter("@AuthCode", objBookingOL.AuthCode);
                param[4] = new SqlParameter("@PGId", objBookingOL.PGId);
                param[5] = new SqlParameter("@CCType", objBookingOL.CCType);
                //objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("SP_UpdatePreAuth_NewProcess", param);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("SP_CORUpdatePreAuth_NewProcess", param);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }

        public OL_Booking UpdateNewPreAuthStatus(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[6];
                param[0] = new SqlParameter("@BookingID", objBookingOL.BookingId);
                param[1] = new SqlParameter("@TransactionID", objBookingOL.Transactionid);
                param[2] = new SqlParameter("@CCNo", objBookingOL.CCNo);
                param[3] = new SqlParameter("@AuthCode", objBookingOL.AuthCode);
                param[4] = new SqlParameter("@PGId", objBookingOL.PGId);
                param[5] = new SqlParameter("@CCType", objBookingOL.CCType);              
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("prc_RePreAuth", param);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }
        public OL_Booking GetBookingConfirmationDetails(OL_Booking objBookingOL)
        {

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@BookingID", objBookingOL.BookingId);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Prc_GetCORBookingDetails", param);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }
        public OL_Booking GetSoldOut(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[7];
                param[0] = new SqlParameter("@CityID", objBookingOL.PickUpCityId);
                param[1] = new SqlParameter("@Model", objBookingOL.ModleId);
                param[2] = new SqlParameter("@DateFrom", objBookingOL.PickupDate);
                param[3] = new SqlParameter("@TimeFrom", objBookingOL.PickupTime);
                param[4] = new SqlParameter("@DateTo", objBookingOL.DropOffDate);
                param[5] = new SqlParameter("@TimeTo", objBookingOL.DropoffTime);
                param[6] = new SqlParameter("@ClientIndivID", objBookingOL.ClientCoIndivID);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("prc_SoldOutCategory_Exception", param);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }
        public OL_Booking GetGuestBookingDetails(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@ClientCoIndivId", objBookingOL.ClientCoIndivID);
                param[1] = new SqlParameter("@PickupFromDate", objBookingOL.PickupDate);
                param[2] = new SqlParameter("@PickupToDate", objBookingOL.DropOffDate);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Proc_GetCorGuestBookingDetails", param);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }
        public OL_Booking UPdateSMSStatus(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@SMSStatus", objBookingOL.SmsStatus);
                param[1] = new SqlParameter("@BookingID", objBookingOL.BookingId);
                object status;
                status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "ProcBook_UpdateSMSStatus", param);
                objBookingOL.SmsStatus = Convert.ToBoolean(status);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;

            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }

        public void SaveLogError(OL_ErrorLogDetails objErrorLogDetailsOL)
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@ErrorSource", objErrorLogDetailsOL.errorSource);
            param[1] = new SqlParameter("@ErrorBlock", objErrorLogDetailsOL.errorBlock);
            param[2] = new SqlParameter("@ErrorMessage", objErrorLogDetailsOL.ErrorMessage);
            param[3] = new SqlParameter("@ClientCoInDivId", objErrorLogDetailsOL.errorCreatedBy);
            param[4] = new SqlParameter("@BookingID", objErrorLogDetailsOL.errorBookingId);
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_CORLogError", param);
        }

        public OL_Booking GetGuestDetailsOnMobileBasis(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@SearchKey", objBookingOL.GuestSearchKey);
                param[1] = new SqlParameter("@ClientCoId", objBookingOL.ClientCoId);
                param[2] = new SqlParameter("@SearchOption", objBookingOL.SearchOption);
                param[3] = new SqlParameter("@GuestLastName", objBookingOL.GuestLastName);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Prc_CorGetGuestDetailsOnMobile", param);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
                //if (objBookingOL.ObjDataTable.Rows.Count > 0)
                //{
                //    objBookingOL.GuestEmailId = Convert.ToString(objBookingOL.ObjDataTable.Rows[0]["EmailID"]);
                //    objBookingOL.GuestName = Convert.ToString(objBookingOL.ObjDataTable.Rows[0]["GuestName"]);
                //    objBookingOL.ClientCoIndivID = Convert.ToInt32(objBookingOL.ObjDataTable.Rows[0]["ClientCoIndivID"]);
                //    objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
                //}
                //else
                //{
                //    objBookingOL.GuestEmailId = "";
                //    objBookingOL.ClientCoIndivID = 0;
                //    objBookingOL.GuestName = "";
                //    objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                //}
               
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }

        public OL_Booking InsertBookingLog(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@BookingId", objBookingOL.BookingId);
                param[1] = new SqlParameter("@BookerType", objBookingOL.Implant);
                param[2] = new SqlParameter("@BookerID", objBookingOL.BookerID);
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CorIntCorBookingLogDetails", param);
            }
            catch (Exception Ex)
            {               
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        
        }
        public OL_Booking GetFacilitatorDetails(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@FacilitatorID", objBookingOL.FacilitatorId);              
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Proc_GetSelectedFacilitatorDetails", param);
                if (objBookingOL.ObjDataTable.Rows.Count > 0)
                {
                    objBookingOL.FaclitatorPhoneNo = objBookingOL.ObjDataTable.Rows[0]["Phone1"].ToString();
                    objBookingOL.FacilitatorEmailId = objBookingOL.ObjDataTable.Rows[0]["EmailID"].ToString();                   
                
                }
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;            

            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }

        public OL_Booking GetInplantName(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoID", objBookingOL.ClientCoId);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("GetImplantName", param);
                return objBookingOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }

      

    }
}
