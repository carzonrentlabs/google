﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient ;
using COR.SolutionFramework;
using COR.ObjectFramework;

namespace COR.DataFrameWork
{
    public class DL_EditBooking
    {
        public OL_Booking EditBookingSearch(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@BookingId", objBookingOL.BookingId);
                param[1] = new SqlParameter("@ClientCoIndivId", objBookingOL.ClientCoIndivID);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Proc_GetCorBookingDetails", param);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
                return objBookingOL;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }
        public OL_Booking GetEditBookingDetails(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@BookingId", objBookingOL.BookingId);
                objBookingOL.ObjDataTable = SqlHelper.ExecuteDatatable("Prc_GetCorEditBookingDetails", param);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
                return objBookingOL;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }

        public OL_Booking EditBooking(OL_Booking objBookingOL)
        {
            try
            {

                SqlParameter[] param = new SqlParameter[41];
                param[0] = new SqlParameter("@BookingID", objBookingOL.BookingId);
                param[1] = new SqlParameter("@PickUpCityID", objBookingOL.PickUpCityId);
                param[2] = new SqlParameter("@service", objBookingOL.ServiceType);
                param[3] = new SqlParameter("@ModelID", objBookingOL.ModleId);
                param[4] = new SqlParameter("@PickUpDate", objBookingOL.PickupDate);
                param[5] = new SqlParameter("@pickUpTime", objBookingOL.PickupTime);
                param[6] = new SqlParameter("@DropOffDate", objBookingOL.DropOffDate );
                param[7] = new SqlParameter("@DropOffTime", objBookingOL.DropoffTime);
                param[8] = new SqlParameter("@pickUpAdd", objBookingOL.PickUpAdd);
                param[9] = new SqlParameter("@OutstationYN", objBookingOL.OutstationYN);
                param[10] = new SqlParameter("@AirportType", objBookingOL.AirportType);
                param[11] = new SqlParameter("@FlgtNo", objBookingOL.FlightNo);

                param[12] = new SqlParameter("@IndicatedPkgID", objBookingOL.IndicatedPkgID);
                param[13] = new SqlParameter("@IndicatedPkgHrsTrue", objBookingOL.IndicatedPkgHrsTrue);
                param[14] = new SqlParameter("@IndicatedExtraHr", objBookingOL.IndicatedExtraHr);
                param[15] = new SqlParameter("@IndicatedPkgHrs", objBookingOL.IndicatedPkgHrs);
                param[16] = new SqlParameter("@IndicatedPkgKMs", objBookingOL.IndicatedPkgKMs);
                param[17] = new SqlParameter("@IndicatedExtraKM", objBookingOL.IndicatedExtraKM);

                param[18] = new SqlParameter("@IndicatedNightStayAmt", objBookingOL.IndicatedNightStayAmt);
                param[19] = new SqlParameter("@IndicatedOutStnAmt", objBookingOL.IndicatedOutStnAmt);
                param[20] = new SqlParameter("@IndicatedPrice", objBookingOL.IndicatedPrice);
                param[21] = new SqlParameter("@IndicatedDiscPC", objBookingOL.IndicatedDiscountPC);
                param[22] = new SqlParameter("@IndicatedDepositAmt", objBookingOL.IndicatedDepositAmt);
                param[23] = new SqlParameter("@ServiceUnitID", objBookingOL.ServiceUnitId);
                param[24] = new SqlParameter("@NoNight", objBookingOL.NoNight);
                param[25] = new SqlParameter("@Remarks", objBookingOL.Remarks);
                param[26] = new SqlParameter("@ModifiedBy", objBookingOL.ModifiedBy);
                param[27] = new SqlParameter("@intpkgCustom", objBookingOL.CustomYN);
                param[28] = new SqlParameter("@EscortRequestYN", objBookingOL.Escort);
                param[29] = new SqlParameter("@AdditionalEmailId", objBookingOL.AdditionalEmaiId);
                param[30] = new SqlParameter("@AdditionalMobileNo", objBookingOL.AdditionalMobileNo);
                param[31] = new SqlParameter("@BookerID", objBookingOL.BookerID);
                param[32] = new SqlParameter("@BookerType", objBookingOL.Implant);

                param[33] = new SqlParameter("@CGSTTaxAmt", objBookingOL.CGSTTaxAmt);
                param[34] = new SqlParameter("@SGSTTaxAmt", objBookingOL.SGSTTaxAmt);
                param[35] = new SqlParameter("@IGSTTaxAmt", objBookingOL.IGSTTaxAmt);
                param[36] = new SqlParameter("@CGSTTaxPercent", objBookingOL.CGSTTaxPercent);
                param[37] = new SqlParameter("@SGSTTaxPercent", objBookingOL.SGSTTaxPercent);
                param[38] = new SqlParameter("@IGSTTaxPercent", objBookingOL.IGSTTaxPercent);
                param[39] = new SqlParameter("@ClientGSTId", objBookingOL.ClientGSTId);
                param[40] = new SqlParameter("@IndicatedGSTSurchargeAmount", objBookingOL.IndicatedGSTSurChargeAmount);


                object status;
                //status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CorBookingUpdateDetails", param);
                status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CorBookingUpdateDetails_GST", param);
                objBookingOL.ModifiedStatus = Convert.ToInt32(status);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
                
            }
            return objBookingOL;
        }
        public OL_Booking CancelBooking(OL_Booking objBookingOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@BookingID", objBookingOL.BookingId);
                param[1] = new SqlParameter("@CancelReason", objBookingOL.CancelReason);
                param[2] = new SqlParameter("@ClientCoIndivId", objBookingOL.ClientCoIndivID);
                param[3] = new SqlParameter("@BookerID", objBookingOL.BookerID);
                param[4] = new SqlParameter("@BookerType", objBookingOL.Implant);
                object status;
                status = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Prc_CORCancelBooking", param);
                objBookingOL.ModifiedStatus = Convert.ToInt32(status);
                objBookingOL.DbOperationStatus = CommonConstant.SUCCEED;
            }
            catch (Exception Ex)
            {
                objBookingOL.DbOperationStatus = CommonConstant.FAIL;
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        
        }


    }
}
