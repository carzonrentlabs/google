﻿//===============================================================================
//Created By :	<Krishna Kumar>
//Create date:  <20-06-2015>
//Description:	<Guest Registration Data access logic >
//===============================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using COR.ObjectFramework;
using COR.SolutionFramework;

namespace COR.DataFrameWork
{
    public class DL_Registration
    {
        DataTable dtGuestDetail = null;

        public DataSet SearchClientByEmail(string EmailID)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@EmailId", EmailID);

                ds = SqlHelper.ExecuteDataset("PrcSearchClientByEmail", param);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
            return ds;
        }

        public OL_Registration BindRegisteredGuest(OL_Registration objRegistrationOL)
        {
            try
            {
                dtGuestDetail = new DataTable();
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@EmailId", objRegistrationOL.EmailId);
                param[1] = new SqlParameter("@ClientIndivId", objRegistrationOL.ClientIndivId);
                dtGuestDetail = SqlHelper.ExecuteDatatable("Proc_GuestDetails", param);
                if (dtGuestDetail.Rows.Count > 0)
                {
                    objRegistrationOL.FirstName = Convert.ToString(dtGuestDetail.Rows[0]["FName"]);
                    objRegistrationOL.LastName = Convert.ToString(dtGuestDetail.Rows[0]["LName"]);
                    objRegistrationOL.MobileNo = Convert.ToString(dtGuestDetail.Rows[0]["Phone1"]);
                    objRegistrationOL.EmailId = Convert.ToString(dtGuestDetail.Rows[0]["EmailID"]);
                    objRegistrationOL.EmployeeCode = Convert.ToString(dtGuestDetail.Rows[0]["EmpCode"]);
                    objRegistrationOL.ClientIndivId = Convert.ToInt32(dtGuestDetail.Rows[0]["ClientCoIndivID"]);
                    objRegistrationOL.PaymentTerms = Convert.ToString(dtGuestDetail.Rows[0]["paymentterms"]);
                    objRegistrationOL.Gender = Convert.ToString(dtGuestDetail.Rows[0]["Gender"]);
                    if (Convert.ToBoolean(dtGuestDetail.Rows[0]["ClientRegisteredYN"]) && Convert.ToBoolean(dtGuestDetail.Rows[0]["CCRegisteredYN"]))
                    {
                        objRegistrationOL.AutoRedirect = true;
                    }
                    else
                    {
                        objRegistrationOL.AutoRedirect = false;
                    }
                }                
                return objRegistrationOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }
        public OL_Registration RegisterGuest(OL_Registration objRegistrationOL)
        {
            try
            {                
                SqlParameter[] param = new SqlParameter[9];
                param[0] = new SqlParameter("@FName", objRegistrationOL.FirstName);
                param[1] = new SqlParameter("@LName", objRegistrationOL.LastName);
                param[2] = new SqlParameter("@EmailID", objRegistrationOL.EmailId);
                param[3] = new SqlParameter("@Mobile", objRegistrationOL.MobileNo);
                param[4] = new SqlParameter("@Empcode", objRegistrationOL.EmployeeCode);
                param[5] = new SqlParameter("@Password", objRegistrationOL.Password);
                param[6] = new SqlParameter("@ClientCoId", objRegistrationOL.ClientCoId);
                param[7] = new SqlParameter("@Gender", objRegistrationOL.Gender);
                param[8] = new SqlParameter("@ClientIndivId", objRegistrationOL.ClientIndivId);
                param[8].Direction = ParameterDirection.Output;
                //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_RegisterGuest", param);
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_RegisterGuest1", param);
                objRegistrationOL.ClientIndivId = (Int32)param[8].Value;
                return objRegistrationOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }
        public OL_Registration UpdateGuestStatus(OL_Registration objRegistrationOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@ClientCoIndivId", objRegistrationOL.ClientIndivId);
                param[1] = new SqlParameter("@Status", objRegistrationOL.Status);
                param[1].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_UpdateGuestStatus", param);
                objRegistrationOL.Status = (Int32)param[1].Value;
                return objRegistrationOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }
        public ClientDetails CheckPaymentCorporateModule(int ClientCoID)
        {
            ClientDetails CD = new ClientDetails();
            //Boolean ClientStatus = false;
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoID", ClientCoID);
                DataSet ds =  SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "Prc_GetPaymentCorporateModule", param);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    CD.isPaymateCorporateModule = Convert.ToBoolean(ds.Tables[0].Rows[0]["isPaymateCorporateModule"]);
                    CD.PaymentTerms = Convert.ToString(ds.Tables[0].Rows[0]["PaymentTerms"]);
                    CD.ClientCoName = Convert.ToString(ds.Tables[0].Rows[0]["ClientCoName"]);
                    //if (Convert.ToBoolean(ds.Tables[0].Rows[0]["isPaymateCorporateModule"]))
                    //{
                    //    ClientStatus = true;
                    //}
                }
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
            return CD;
        }
        public DataTable ExistingGuest(OL_Registration objRegistrationOL)
        {
            try
            {
                dtGuestDetail = new DataTable();
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@EmailId", objRegistrationOL.EmailId);
                param[1] = new SqlParameter("@MobileNo", objRegistrationOL.MobileNo);                
                param[2] = new SqlParameter("@EmployeeCode", objRegistrationOL.EmployeeCode);
                param[3] = new SqlParameter("@ClientCoId", objRegistrationOL.ClientCoId);
                param[4] = new SqlParameter("@ClientIndivId", objRegistrationOL.ClientIndivId);
                dtGuestDetail = SqlHelper.ExecuteDatatable("Proc_ExistingGuest", param);                
                return dtGuestDetail;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }
        public OL_Registration UpdateIndivInfo(OL_Registration objRegistrationOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[8];
                param[0] = new SqlParameter("@FName", objRegistrationOL.FirstName);
                param[1] = new SqlParameter("@LName", objRegistrationOL.LastName);
                param[2] = new SqlParameter("@EmailID", objRegistrationOL.EmailId);
                param[3] = new SqlParameter("@Mobile", objRegistrationOL.MobileNo);
                param[4] = new SqlParameter("@EmployeeCode", objRegistrationOL.EmployeeCode);
                param[5] = new SqlParameter("@ClientIndivId", objRegistrationOL.ClientIndivId);
                param[6] = new SqlParameter("@Gender", objRegistrationOL.Gender);
                param[7] = new SqlParameter("@Status", objRegistrationOL.Status);
                param[7].Direction = ParameterDirection.Output;
                //SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_UpdateExistingUser", param);
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_UpdateExistingUser1", param);
                objRegistrationOL.Status = (Int32)param[7].Value;
                return objRegistrationOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }
        public DataTable GetRegistrationStatus(OL_Registration objRegistrationOL)
        {
            try
            {
                dtGuestDetail = new DataTable();
                SqlParameter[] param = new SqlParameter[1];                
                param[0] = new SqlParameter("@ClientIndivId", objRegistrationOL.ClientIndivId);
                dtGuestDetail = SqlHelper.ExecuteDatatable("Proc_GetRegistrationStatus", param);
                return dtGuestDetail;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }
        public OL_Registration UpdateRegistrationStatus(OL_Registration objRegistrationOL)
        {
            try
            {                
                SqlParameter[] param = new SqlParameter[2];               
                param[0] = new SqlParameter("@ClientIndivId", objRegistrationOL.ClientIndivId);
                param[1] = new SqlParameter("@Status", objRegistrationOL.Status);
                param[1].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_ClientRegisteredStatus", param);
                objRegistrationOL.Status = (Int32)param[1].Value;
                return objRegistrationOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }
        public OL_Registration UpdateCCStatus(OL_Registration objRegistrationOL)
        {
            try
            {                
                SqlParameter[] param = new SqlParameter[2];               
                param[0] = new SqlParameter("@ClientIndivId", objRegistrationOL.ClientIndivId);
                param[1] = new SqlParameter("@Status", objRegistrationOL.Status);
                param[1].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_CCStatus", param);
                objRegistrationOL.Status = (Int32)param[1].Value;
                return objRegistrationOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }

        public OL_Registration UpdateCCStatus_NewProcess(OL_Registration objRegistrationOL)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@ClientIndivId", objRegistrationOL.ClientIndivId);
                param[1] = new SqlParameter("@CCType", objRegistrationOL.CCType);
                param[2] = new SqlParameter("@CCNo", objRegistrationOL.CCNo);
                param[3] = new SqlParameter("@Address", objRegistrationOL.Address);
                param[4] = new SqlParameter("@Status", objRegistrationOL.Status);
                param[4].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_CCStatus_NewProcess", param);
                objRegistrationOL.Status = (Int32)param[4].Value;
                return objRegistrationOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }

        public OL_Registration SendRegistrationMailDetails(OL_Registration objRegistrationOL)
        {
            try
            {
                dtGuestDetail = new DataTable();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoIndivId", objRegistrationOL.ClientIndivId);
                dtGuestDetail = SqlHelper.ExecuteDatatable("Prc_SendRegistrationMailDetails", param);
                if (dtGuestDetail.Rows.Count > 0)
                {
                    objRegistrationOL.FirstName = Convert.ToString(dtGuestDetail.Rows[0]["FName"]);
                    objRegistrationOL.LastName = Convert.ToString(dtGuestDetail.Rows[0]["LName"]);
                    objRegistrationOL.MobileNo = Convert.ToString(dtGuestDetail.Rows[0]["Phone1"]);
                    objRegistrationOL.EmailId = Convert.ToString(dtGuestDetail.Rows[0]["EmailID"]);
                    objRegistrationOL.Password = Convert.ToString(dtGuestDetail.Rows[0]["pass"]);
                    objRegistrationOL.ClientIndivId = Convert.ToInt32(dtGuestDetail.Rows[0]["ClientCoIndivID"]);
                    objRegistrationOL.ClientCoId = Convert.ToInt32(dtGuestDetail.Rows[0]["ClientCoID"]);
                }
                return objRegistrationOL;
            }
            catch (Exception Ex)
            {

                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
           
        }


        public string GetPaymateSysRegCodeForIndiv(int ClientCoIndivID)
        {
            try
            {
                DataTable dataTableSysRegCode = new DataTable();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoIndivID", ClientCoIndivID);
                dataTableSysRegCode = SqlHelper.ExecuteDatatable("Proc_GetPaymateSysRegCodeForIndiv", param);
                if (dataTableSysRegCode != null && dataTableSysRegCode.Rows.Count > 0)
                {
                    DataRow row = dataTableSysRegCode.Rows[0];
                    return Convert.ToString(row["PaymateSysRegCode"]);
                }
                return null;    
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
            
        }
    }
}
