﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data ;
using System.Data.SqlClient;
using COR.ObjectFramework ;
using COR.SolutionFramework;

namespace COR.DataFrameWork
{
    public class DL_LoginUser
    {
        DataTable dtLoginUser = null;
        public OL_LoginUser ValidateUser(OL_LoginUser objLoginUserOL)
        {
            try
            {
                dtLoginUser = new DataTable();
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@LoginID", objLoginUserOL.LoginId);
                param[1] = new SqlParameter("@Password", objLoginUserOL.Password);
                dtLoginUser = SqlHelper.ExecuteDatatable("Prc_CorAuthenticateUser", param);
                if (dtLoginUser.Rows.Count > 0)
                {
                    objLoginUserOL.ClientCoIndivID = Convert.ToInt32(dtLoginUser.Rows[0]["ClientCoIndivID"].ToString());
                    objLoginUserOL.ClientCoId = Convert.ToInt32(dtLoginUser.Rows[0]["ClientCoID"].ToString());
                    objLoginUserOL.LoginId = objLoginUserOL.LoginId.ToString();
                    objLoginUserOL.CCRegisteredYN = Convert.ToBoolean(dtLoginUser.Rows[0]["CCRegisteredYN"].ToString());
                    objLoginUserOL.ClientRegisteredYN = Convert.ToBoolean(dtLoginUser.Rows[0]["ClientRegisteredYN"].ToString());

                    objLoginUserOL.LoginStatus = "valid";
                }
                else
                {
                    objLoginUserOL.LoginId = "0";
                    objLoginUserOL.LoginStatus = "Invalid";
                }
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }

        }
        public OL_LoginUser GetRegistedLocation(OL_LoginUser objLoginUserOL)
        {
            try
            {
                dtLoginUser = new DataTable();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@sysUserId", objLoginUserOL.SysUserId);
                dtLoginUser = SqlHelper.ExecuteDatatable("Prc_CORGetLocationRegisterd", param);
                if (dtLoginUser.Rows.Count > 0 && dtLoginUser != null)
                {
                    objLoginUserOL.UserName = dtLoginUser.Rows[0]["UserName"].ToString();
                    objLoginUserOL.UserCityName = dtLoginUser.Rows[0]["CityName"].ToString();
                }
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }

        public OL_LoginUser GetImplantRegistedLocation(OL_LoginUser objLoginUserOL)
        {

            string clientConame = string.Empty;
            try
            {
                dtLoginUser = new DataTable();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoId", objLoginUserOL.ClientCoId);
                dtLoginUser = SqlHelper.ExecuteDatatable("Prc_CORGetImplantRegisterdLocation", param);
                if (dtLoginUser.Rows.Count > 0 && dtLoginUser != null)
                {
                    objLoginUserOL.UserName = dtLoginUser.Rows[0]["UserName"].ToString();
                    objLoginUserOL.UserCityName = dtLoginUser.Rows[0]["CityName"].ToString();
                }
               
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objLoginUserOL;
        }

        public OL_LoginUser GetValidateUserIdForPasswordRecovery(OL_LoginUser objLoginUserOL)
        {
            try
            {
                dtLoginUser = new DataTable();
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@EmailID", objLoginUserOL.LoginId);
                param[1] = new SqlParameter("@ClientCoId", objLoginUserOL.ClientCoId);
                dtLoginUser = SqlHelper.ExecuteDatatable("Prc_ValidateUserForPasswordRecovery_CorBooking", param);
                if (dtLoginUser.Rows.Count > 0)
                {  
                    objLoginUserOL.LoginStatus =dtLoginUser.Rows[0]["LoginStatus"].ToString();
                    objLoginUserOL.ReceiverId = objLoginUserOL.LoginId.ToString();
                }
                else
                {
                    objLoginUserOL.LoginStatus = "Invalid";
                }
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }

        public OL_LoginUser UpdatePasswword(OL_LoginUser objLoginUserOL)
        {
            try
            {
                dtLoginUser = new DataTable();
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@LoginId", objLoginUserOL.LoginId);
                param[1] = new SqlParameter("@ClientCoID", objLoginUserOL.ClientCoId);
                dtLoginUser = SqlHelper.ExecuteDatatable("Prc_GenerateRandomPassword_CorBooking", param);
                if (dtLoginUser.Rows.Count > 0 && dtLoginUser.Rows[0]["ConfirmPassword"].ToString() != "Invalid")
                {
                    objLoginUserOL.LoginStatus = "Valid";
                    objLoginUserOL.Password = dtLoginUser.Rows[0]["ConfirmPassword"].ToString();
                    objLoginUserOL.UserName = dtLoginUser.Rows[0]["UserName"].ToString();
                }
                else
                {
                    objLoginUserOL.LoginStatus = "Invalid";
                    objLoginUserOL.Password = null;
                    objLoginUserOL.UserName = null;
                    
                }
                return objLoginUserOL;

            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception (Ex.Message);
            }
        }
        public OL_LoginUser GetGuestProfile(OL_LoginUser objLoginUserOL)
        {
            try
            {
                dtLoginUser = new DataTable();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoIndivId", objLoginUserOL.ClientCoIndivID);
                objLoginUserOL.ObjDataTable = SqlHelper.ExecuteDatatable("Prc_GetGuestProfile", param);

                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }

        public OL_LoginUser ValidateImplant(OL_LoginUser objLoginUserOL)
        {

            DataTable dsImplantlogin = null;
            try
            {
                dsImplantlogin = new DataTable();
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@LoginID", objLoginUserOL.LoginId);
                param[1] = new SqlParameter("@Password", objLoginUserOL.Password);
                dsImplantlogin = SqlHelper.ExecuteDatatable("Prc_ImplantLoginValidation", param);
                if (dsImplantlogin.Rows.Count > 0)
                {
                    objLoginUserOL.ClientCoId = Convert.ToInt32(dsImplantlogin.Rows[0]["ClientCoID"].ToString());
                    objLoginUserOL.LoginId = objLoginUserOL.LoginId.ToString();
                    objLoginUserOL.LoginStatus = "valid";
                }
                else
                {
                    objLoginUserOL.ClientCoId = 0;
                    objLoginUserOL.LoginId = "";
                    objLoginUserOL.LoginStatus = "Invalid";
                }

            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
            return objLoginUserOL;
        }
    }
    
}
