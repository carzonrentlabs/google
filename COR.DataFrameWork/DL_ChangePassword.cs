﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COR.ObjectFramework;
using COR.SolutionFramework;
using System.Data;
using System.Data.SqlClient;

namespace COR.DataFrameWork
{
    public class DL_ChangePassword
    {
        DataTable dtLoginUser = null;
        public OL_LoginUser CheckCurrentPassword(OL_LoginUser objLoginUserOL)
        {
            try
            {
                dtLoginUser = new DataTable();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ClientCoIndivID", objLoginUserOL.ClientCoIndivID);
                dtLoginUser = SqlHelper.ExecuteDatatable("SP_CORGetOldPassword", param);
                if (dtLoginUser.Rows.Count > 0)
                {
                    objLoginUserOL.Password = dtLoginUser.Rows[0]["UserPwd"].ToString();
                    objLoginUserOL.LoginId = dtLoginUser.Rows[0]["LoginID"].ToString();
                }
                else
                {
                    objLoginUserOL.Password = null;
                    objLoginUserOL.LoginId = null;
                }
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }

        }
        public OL_LoginUser ChangePassword(OL_LoginUser objLoginUserOL)
        {
            try
            {

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@NewPassword", objLoginUserOL.Password);
                param[1] = new SqlParameter("@ClientCoIndivID", objLoginUserOL.ClientCoIndivID);
                int status = SqlHelper.ExecuteNonQuery("Prc_CORChangePassword", param);
                if (status > 0)
                {
                    objLoginUserOL.LoginStatus = "Update";
                }
                else
                {
                    objLoginUserOL.LoginStatus = "Invalid";
                }
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
                throw new Exception(Ex.Message);
            }

        }
    }
}
