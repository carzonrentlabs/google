﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COR.SolutionFramework;
using COR.DataFrameWork;
using COR.ObjectFramework;

namespace COR.BusinessFramework
{
    public class BL_LoginUser
    {
        DL_LoginUser objLoginUserDL = null;
        public OL_LoginUser AuthenticateLoginId(OL_LoginUser objLoginUserOL)
        {
            try
            {
                objLoginUserDL = new DL_LoginUser();
                objLoginUserOL = objLoginUserDL.ValidateUser(objLoginUserOL);
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        public OL_LoginUser GetRegistedLocation(OL_LoginUser objLoginUserOL)
        {
            try
            {
                objLoginUserDL = new DL_LoginUser();
                objLoginUserOL = objLoginUserDL.GetRegistedLocation(objLoginUserOL);
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }
        public OL_LoginUser GetImplantRegistedLocation(OL_LoginUser objLoginUserOL)
        {
            try
            {
                objLoginUserDL = new DL_LoginUser();
                objLoginUserOL = objLoginUserDL.GetImplantRegistedLocation(objLoginUserOL);
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        public OL_LoginUser GetValidateUserIdForPasswordRecovery(OL_LoginUser objLoginUserOL)
        {
            try
            {
                objLoginUserDL = new DL_LoginUser();
                objLoginUserOL = objLoginUserDL.GetValidateUserIdForPasswordRecovery(objLoginUserOL);
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        public OL_LoginUser UpdatePasswword(OL_LoginUser objLoginUserOL)
        {
            try
            {
                objLoginUserDL = new DL_LoginUser();
                objLoginUserOL = objLoginUserDL.UpdatePasswword(objLoginUserOL);
                if (objLoginUserOL.LoginStatus.ToString() != "Invalid" && objLoginUserOL.Password.ToString() != "Invalid" && !string.IsNullOrEmpty(objLoginUserOL.Password.ToString()) && objLoginUserOL.UserName.ToString() != "Invalid")
                {
                    objLoginUserOL.Subject = "Change Password";
                    objLoginUserOL.EmailContent = ChangePasswordMail.ChangePasswordMailContent(objLoginUserOL);
                    bool status = ChangePasswordMail.SendChagnePasswordMail(objLoginUserOL);
                    if (status == true)
                    {
                        objLoginUserOL.LoginStatus = "Valid";
                    }
                    else
                    {
                        objLoginUserOL.LoginStatus = "Invalid";
                    }
                }
                else
                {
                    objLoginUserOL.LoginStatus = "Invalid";
                }
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }
        public OL_LoginUser GetGuestProfile(OL_LoginUser objLoginUserOL)
        {
            try
            {
                objLoginUserDL = new DL_LoginUser();
                objLoginUserOL = objLoginUserDL.GetGuestProfile(objLoginUserOL);
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        public OL_LoginUser AuthenticateImplantLoginId(OL_LoginUser objLoginUserOL)
        {
            try
            {
                objLoginUserDL = new DL_LoginUser();
                objLoginUserOL = objLoginUserDL.ValidateImplant(objLoginUserOL);
                
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            return objLoginUserOL;
        }
    }
}
