﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COR.ObjectFramework;
using COR.SolutionFramework;


namespace COR.SolutionFramework
{
    public class SMSContent
    {
        public static String SendMessageForGuest(OL_Booking objBookingOL)
        {
            StringBuilder messageGuest = new StringBuilder();
            messageGuest = messageGuest.Append("Dear " + objBookingOL.GuestName + ", Your Carzonrent Booking ID for " + GeneralUtility.FormateDateSM(objBookingOL.PickupDate.ToString()) + " Pickup in " + objBookingOL.PickUpCityName.ToString() + " at " + objBookingOL.PickupTime + "Hrs. is " + objBookingOL.BookingId + ". Thank you for choosing COR. For assistance call  01141841212. To track your cab detail, billing information and share feedback download the COR RIDE mobile app at http://bit.ly/CORRide");
            return messageGuest.ToString();
        }
        public static String SendMessageForModification(OL_Booking objBookingOL)
        {
            StringBuilder messageGuest = new StringBuilder();
            messageGuest = messageGuest.Append("Dear Guest, your BookingID " + objBookingOL.BookingId + " for " + GeneralUtility.FormateDateSM(objBookingOL.PickupDate.ToString()) + " Pickup in " + objBookingOL.PickUpCityName.ToString() + " at " + objBookingOL.PickupTime.ToString() + "Hrs." + " has been modified.Thank you for choosing COR.For assistance call 01141841212");
            return messageGuest.ToString();
        }
        public static String SendMessageForCancel(OL_Booking objBookingOL)
        {
            StringBuilder messageGuest = new StringBuilder();
            messageGuest = messageGuest.Append("Dear Guest, your Carzonrent Booking ID " + objBookingOL.BookingId + " has been cancelled.Thank you for choosing COR.For assistance call 01141841212");
            return messageGuest.ToString();
        }
        public static String SendMessageForFacilitator(OL_Booking objBookingOL)
        {
            StringBuilder messageFacilitator = new StringBuilder();
            messageFacilitator = messageFacilitator.Append("Dear Receiver, Your Carzonrent Booking ID for " + GeneralUtility.FormateDateSM(objBookingOL.PickupDate.ToString()) + " Pickup in " + objBookingOL.PickUpCityName.ToString() + " at " + objBookingOL.PickupTime + "Hrs. is " + objBookingOL.BookingId + ". Thank you for choosing COR. For assistance call  01141841212. To track your cab detail, billing information and share feedback download the COR RIDE mobile app at http://bit.ly/CORRide");
            return messageFacilitator.ToString();
        }
    }
}
