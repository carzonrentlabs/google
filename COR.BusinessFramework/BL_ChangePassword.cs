﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COR.DataFrameWork;
using COR.ObjectFramework;
using COR.SolutionFramework;

namespace COR.BusinessFramework
{
    public class BL_ChangePassword
    {
        DL_ChangePassword objLoginUserDL = null;
        public OL_LoginUser CheckCurrentPassword(OL_LoginUser objLoginUserOL)
        {
            objLoginUserDL = new DL_ChangePassword();
            try
            {
                objLoginUserOL = objLoginUserDL.CheckCurrentPassword(objLoginUserOL);
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }

        }
        public OL_LoginUser ChangePassword(OL_LoginUser objLoginUserOL)
        {
            objLoginUserDL = new DL_ChangePassword();
            try
            {
                objLoginUserOL = objLoginUserDL.ChangePassword(objLoginUserOL);
                return objLoginUserOL;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }

        }
    }
}
