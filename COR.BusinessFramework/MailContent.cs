﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.IO;
using COR.ObjectFramework;
using COR.SolutionFramework;

namespace COR.BusinessFramework
{
   
    public class Mailhandling
    {
        public static bool SendMail(OL_Booking   objBookingOL)
        {
            try
            {
                string strMessageSubject = objBookingOL.Subject.ToString() + objBookingOL.GuestName.ToString();
                string senderID = "booking@carzonrent.com";
                string senderName = "Reserve Team";
                string guestEmailId = objBookingOL.GuestEmailId.ToString();
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(senderID, senderName);
                msg.To.Add(guestEmailId);

                if (!string.IsNullOrEmpty(objBookingOL.AddCC))
                {
                    msg.CC.Add(objBookingOL.AddCC.ToString());
                }
                if (!string.IsNullOrEmpty(objBookingOL.AddBCC))
                {
                    msg.CC.Add(objBookingOL.AddBCC.ToString());
                }


                msg.Bcc.Add("balister.sharma@carzonrent.com");
                msg.Subject = strMessageSubject;
                msg.SubjectEncoding = System.Text.Encoding.UTF8;
                msg.Body = objBookingOL.EmailContent.ToString();

                msg.BodyEncoding = System.Text.Encoding.UTF8;
                msg.IsBodyHtml = true;
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential("booking@carzonrent.com", "Carz@1212");
                client.Send(msg);
                return true;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an errored occured");
                return false;
                throw new Exception(Ex.Message);
            }

        }

    }
    

    public class ChangePasswordMail
    {
        public static bool SendChagnePasswordMail(OL_LoginUser objLoginUserOL)
        {
            try
            {

                string senderID = "booking@carzonrent.com";
                string senderName = "Reserve Team";
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(senderID, senderName);
                msg.To.Add(objLoginUserOL.ReceiverId);
                msg.Subject = objLoginUserOL.Subject;
                msg.SubjectEncoding = System.Text.Encoding.UTF8;
                msg.Body = objLoginUserOL.EmailContent.ToString();
                msg.BodyEncoding = System.Text.Encoding.UTF8;
                msg.IsBodyHtml = true;
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential("booking@carzonrent.com", "Carz@1212");
                client.Send(msg);
                return true;
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an errored occured");
                return false;
                throw new Exception(Ex.Message);
            }
        }

        public static string ChangePasswordMailContent(OL_LoginUser objLoginUserOL)
        {
            try
            {
                StringBuilder mailContent = new StringBuilder();
                mailContent = mailContent.Append("<html>");
                mailContent = mailContent.Append("<body>");
                mailContent = mailContent.Append("<Table>");

                mailContent = mailContent.Append("<Tr><Td align=Left> <b>");
                mailContent = mailContent.Append("Dear " + objLoginUserOL.UserName.ToString() + ",");
                mailContent = mailContent.Append("</b></Td></Tr>");
                mailContent = mailContent.Append("<Tr><Td align=Left></Td></Tr>");

                mailContent = mailContent.Append("<Tr><Td align=Left>");
                mailContent = mailContent.Append("We welcome you to carzonrent.");
                mailContent = mailContent.Append("</Td></Tr>");
                mailContent = mailContent.Append("<Tr><Td align=Left>");
                mailContent = mailContent.Append("Please find below your password to access your account at Carzonrent.");
                mailContent = mailContent.Append("</Td></Tr>");
                mailContent = mailContent.Append("<Tr><Td align=Left><b>");
                mailContent = mailContent.Append("Password: " + objLoginUserOL.Password.ToString());
                mailContent = mailContent.Append("</b></Td></Tr>");
                mailContent = mailContent.Append("<tr><Td>&nbsp;</td></tr>");
                mailContent = mailContent.Append("<Tr><Td align=Left>");
                mailContent = mailContent.Append("<Tr><Td align=Left>");
                mailContent = mailContent.Append("Thanks & Regards");
                mailContent = mailContent.Append("</Td></Tr>");
                mailContent = mailContent.Append("<Tr><Td align=Left>");
                mailContent = mailContent.Append("Team Carzonrent");
                mailContent = mailContent.Append("</Td></Tr>");

                mailContent = mailContent.Append("<Tr><Td align=Left></Td></Tr>");
                mailContent = mailContent.Append("<Tr><Td align=Left></Td></Tr>");
                mailContent = mailContent.Append("<Tr><Td align=Left></Td></Tr>");
                mailContent = mailContent.Append("<Tr><Td align=Left>This is system generated mail. Please do not reply.</Td></Tr>");
                mailContent = mailContent.Append("</Talbe>");
                mailContent = mailContent.Append("</body>");
                mailContent = mailContent.Append("</html >");
                return mailContent.ToString();
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
                throw new Exception(Ex.Message);
            }
        }
    }


    public class MailContent
    {
        public static string BillToClientEMail(OL_Booking objBookingOL)
        {
            try
            {

                StringBuilder billToclientEmail = new StringBuilder();
                billToclientEmail = billToclientEmail.Append("<html>");
                billToclientEmail = billToclientEmail.Append("<body>");
                billToclientEmail = billToclientEmail.Append("<Table>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Dear Esteemed Guest,");
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<tr><Td>&nbsp;</td></tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Thank you for choosing Carzonrent.");
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<tr><Td>&nbsp;</td></tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Please print this e-mail or record your confirmation number in order to pick up your rental.Your confirmation number is " + objBookingOL.BookingId + ". We look forward to seeing you on " + GeneralUtility.FormateDateSM(objBookingOL.PickupDate.ToString()));
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Guest Name : " + objBookingOL.GuestName);
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<tr><Td>&nbsp;</td></tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Reservation Confirmation:");
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<tr><Td>&nbsp;</td></tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Pick-up city		:" + objBookingOL.PickUpCityName);
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Pick-up date		: " + GeneralUtility.FormateDateSM(objBookingOL.PickupDate.ToString ()));
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Pick-up time		:" + objBookingOL.PickupTime);
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Pick-up location	:" + objBookingOL.PickUpAdd);
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Airport type		:" + objBookingOL.AirPort);
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Airline / flight number :" + objBookingOL.FlightNo);
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Type of Car		:" + objBookingOL.CarModelName);
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("To modify or cancel this reservation, call at 011-41841212 or send mail to :reserve@carzonrent.com");
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<tr><Td>&nbsp;</td></tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Please Note: Modification to the bookings may lead to change in charges.");
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<tr><Td>&nbsp;</td></tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("For additional information, please call us at 011-41841212,Email: reserve@carzonrent.com or visit us at www.carzonrent.com ");
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<tr><Td>&nbsp;</td></tr>");
                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("You can now avail our services across 15 cities i.e. Delhi, Gurgaon, Noida, Mumbai,");
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");

                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Bangalore,Hyderabad,Kolkata,Chennai,Ranchi, Bhubaneswar, Jamshedpur,Jaipur,Amritsar,Pune,");
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");

                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Chandigarh ,Udaipur and Ahmedabad.");
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");

                billToclientEmail = billToclientEmail.Append("<Tr><Td>");
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");
                billToclientEmail = billToclientEmail.Append("<tr><Td>&nbsp;</td></tr>");

                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Thanks & Regards,");
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");

                billToclientEmail = billToclientEmail.Append("<Tr><Td align=Left>");
                billToclientEmail = billToclientEmail.Append("Team Carzonrent");
                billToclientEmail = billToclientEmail.Append("</Td></Tr>");

                billToclientEmail = billToclientEmail.Append("</Talbe>");
                billToclientEmail = billToclientEmail.Append("</body>");
                billToclientEmail = billToclientEmail.Append("</html >");
                return billToclientEmail.ToString();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public static string DirectClientEmail(OL_Booking objBookingOL)
        {
            StringBuilder directtoClientEmail = new StringBuilder();
            directtoClientEmail = directtoClientEmail.Append("<html>");
            directtoClientEmail = directtoClientEmail.Append("<body>");
            directtoClientEmail = directtoClientEmail.Append("<Table>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Dear Esteemed Guest,");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Thank you for choosing Carzonrent.");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Please print this e-mail or record your confirmation number in order to pick up your rental.");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Your confirmation number is COR " + objBookingOL.BookingId + ". We look forward to seeing you on " + GeneralUtility.FormateDateSM(objBookingOL.PickupDate.ToString()));
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<tr><td>&nbsp;</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left><table celspacing='1' celpadding='2' border='1'>"); //Start table

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Guest Name </td><td>" + objBookingOL.GuestName);
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Company  </td><td>" + objBookingOL.ClientCoName.ToString());
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Mobile  </td><td>" + objBookingOL.GuestPhoneNo.ToString());
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Email ID   </td><td>" + objBookingOL.GuestEmailId.ToString());
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");


            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left colspan='2'>");
            directtoClientEmail = directtoClientEmail.Append("Reservation Request Confirmation  ");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Pick-up city	 </td><td>" + objBookingOL.PickUpCityName);
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Pick-up date	 </td><td>" + GeneralUtility.FormateDateSM(objBookingOL.PickupDate.ToString()));
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Pick-up time	(Hrs)  </td><td>" + objBookingOL.PickupTime);
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Pick-up location  </td><td>" + objBookingOL.PickUpAdd);
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Airport type	   </td><td>" + objBookingOL.AirPort);
            directtoClientEmail = directtoClientEmail.Append("&nbsp;</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Airline / flight number  </td><td>" + objBookingOL.FlightNo);
            directtoClientEmail = directtoClientEmail.Append("&nbsp;</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Type of Car   </td><td>" + objBookingOL.CarModelName);
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Pre-authorisation Amount  </td><td>" + objBookingOL.ApprovalAmt.ToString());
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");
            
            directtoClientEmail = directtoClientEmail.Append("</td></table>");  //Closed table
            directtoClientEmail = directtoClientEmail.Append("<tr><td>&nbsp;</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("*Pre-authorisation amount is only BLOCKED on your registered Credit Card ending : " + objBookingOL.CCNo);
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");


            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("To modify or cancel this reservation, call at 011-41841212 or send mail to :");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<tr><td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("reserve@carzonrent.com or Contact Carzonrent-Google Implants at :");
            directtoClientEmail = directtoClientEmail.Append("</td></tr>");

            directtoClientEmail = directtoClientEmail.Append("<tr><td>&nbsp;</td></tr>");

            directtoClientEmail = directtoClientEmail.Append("<tr><td align=left>");
            directtoClientEmail = directtoClientEmail.Append("<table celspacing='1' celpadding='1' border='1'>");

            directtoClientEmail = directtoClientEmail.Append("<tr>");
            directtoClientEmail = directtoClientEmail.Append("<Td align=center>City Name</td>");
            directtoClientEmail = directtoClientEmail.Append("<Td align=center>Contact person name</td>");
            directtoClientEmail = directtoClientEmail.Append("<Td align=center>Mobile No.</td>");
            directtoClientEmail = directtoClientEmail.Append("<Td align=center>Email Id</td>");
            directtoClientEmail = directtoClientEmail.Append("</tr>");

            directtoClientEmail = directtoClientEmail.Append("<tr>");
            directtoClientEmail = directtoClientEmail.Append("<td align=left> Gurgaon </td>");
            //directtoClientEmail = directtoClientEmail.Append("<td align=left> Keshav Kumar </td>");
            //directtoClientEmail = directtoClientEmail.Append("<td align=left> 9971399317 </td>"); 
            directtoClientEmail = directtoClientEmail.Append("<td align=left> Vishu/Ajay </td>");
            directtoClientEmail = directtoClientEmail.Append("<td align=left> 9717196377 </td>"); 
            directtoClientEmail = directtoClientEmail.Append("<td align=left> google.ggn@carzonrent.com </td>");
            directtoClientEmail = directtoClientEmail.Append("</tr>");
            
            //directtoClientEmail = directtoClientEmail.Append("<tr>");
            //directtoClientEmail = directtoClientEmail.Append("<td align=left> Hyderabad </td>");
            //directtoClientEmail = directtoClientEmail.Append("<td align=left> Pavan Kumar Mishra </td>");
            //directtoClientEmail = directtoClientEmail.Append("<td align=left> 9542738600 </td>");
            //directtoClientEmail = directtoClientEmail.Append("<td align=left> GoogleHyd@carzonrent.com </td>");
            //directtoClientEmail = directtoClientEmail.Append("</tr>");

            directtoClientEmail = directtoClientEmail.Append("</table>");                       
            directtoClientEmail = directtoClientEmail.Append("</td></tr>");

            directtoClientEmail = directtoClientEmail.Append("<tr><td>&nbsp;</td></tr>");


            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Note: Changing your pickup or return date, time or location may change your rates,");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("taxes or surcharges.");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("For additional information, please call us at 011-41841212, ");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Email: reserve@carzonrent.com or visit us at www.carzonrent.com");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Your chauffeur/car details will be intimated by emails/SMS 2 hrs prior to reporting time.");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<tr><td>&nbsp;</td></tr>");
           
            /*
            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("As part of our constant endeavor towards technological innovation, we are in the process of implementing COR DRIVE product across our geographical ");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("and fleet presence. COR DRIVE is an android based Chauffeur application which aims to deliver transparency, added security and improve comfort for our ");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("esteemed guests. In an effort to go paperless, COR DRIVE chauffeurs shall not be carrying print outs of duty slips and would request for your signatures ");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("on the android devices on completion of the service to automatically initiate invoicing process.");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");
            
            directtoClientEmail = directtoClientEmail.Append("<tr><td>&nbsp;</td></tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("We solicit your support during this implementation drive and would request to bear with us as our systems and processes are undergoing major ");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("technological change");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");
            directtoClientEmail = directtoClientEmail.Append("<tr><td>&nbsp;</td></tr>");
            */

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Thank you again for choosing Carzonrent.");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("</Talbe>");
            directtoClientEmail = directtoClientEmail.Append("</body>");
            directtoClientEmail = directtoClientEmail.Append("</html >");
            return directtoClientEmail.ToString();
        }

        public static string EditBookingMail(OL_Booking objBookingOL)
        {
            StringBuilder directtoClientEmail = new StringBuilder();
            directtoClientEmail = directtoClientEmail.Append("<html>");
            directtoClientEmail = directtoClientEmail.Append("<body>");
            directtoClientEmail = directtoClientEmail.Append("<Table>");
            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Dear Esteemed Guest,");
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");
            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Your car booking with Carzonrent has been updated. The details of the booking are as follows.");
            directtoClientEmail = directtoClientEmail.Append("</Td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<tr><td>&nbsp;</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left><table celspacing='1' celpadding='2' border='1'>"); //Start table

            directtoClientEmail = directtoClientEmail.Append("<tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Car booking ID  </td><td>" + objBookingOL.BookingId.ToString());
            directtoClientEmail = directtoClientEmail.Append("</td></tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Pick-up city  </td><td> " + objBookingOL.PickUpCityName);
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            if (objBookingOL.ServiceType == SeriveType.ChauffeurDrive)
            {
                directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
                directtoClientEmail = directtoClientEmail.Append("Service </td><td> " + "Chauffeur driven");
                directtoClientEmail = directtoClientEmail.Append("</Td></Tr>");
            }
            else
            {
                directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
                directtoClientEmail = directtoClientEmail.Append("Service </td><td> " + "Airport transfer");
                directtoClientEmail = directtoClientEmail.Append("</Td></Tr>");
            }


            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Car model	</td><td> " + objBookingOL.CarModelName);
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Pick-up date </td><td> " + GeneralUtility.FormateDateSM(objBookingOL.PickupDate.ToString()));
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Pick-up time (Hrs) </td><td> " + objBookingOL.PickupTime);
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Drop-off date </td><td> " + GeneralUtility.FormateDateSM(objBookingOL.DropOffDate.ToString()));
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Drop-off time (Hrs) </td><td> " + objBookingOL.DropoffTime);
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Pick-up location / address </td><td> " + objBookingOL.PickUpAdd.ToString());
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Outstation </td><td> " + objBookingOL.OutstationYN.ToString());
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Airport type </td><td> " + objBookingOL.AirportType);
            directtoClientEmail = directtoClientEmail.Append("&nbsp;</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Airline / flight number </td><td> " + objBookingOL.FlightNo);
            directtoClientEmail = directtoClientEmail.Append("&nbsp;</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Reporting contact </td><td> " + objBookingOL.ReportContact);
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Booking updated on </td><td> " + GeneralUtility.FormateDateSM(System.DateTime.Now.ToShortDateString()));
            directtoClientEmail = directtoClientEmail.Append("</td></Tr>");

            directtoClientEmail = directtoClientEmail.Append("</td></table>");  //Closed table
            directtoClientEmail = directtoClientEmail.Append("<tr><td>&nbsp;</td></Tr>");


            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("For additional information, please call us at 011-41841212, ");
            directtoClientEmail = directtoClientEmail.Append("</Tr></Td>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Email: reserve@carzonrent.com or visit us at www.carzonrent.com");
            directtoClientEmail = directtoClientEmail.Append("</Tr></Td>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Your chauffeur/car details will be intimated by emails/SMS 2 hrs prior to reporting time.");
            directtoClientEmail = directtoClientEmail.Append("</Tr></Td>");

            directtoClientEmail = directtoClientEmail.Append("<Tr><Td align=Left>");
            directtoClientEmail = directtoClientEmail.Append("Thank you again for choosing Carzonrent.");
            directtoClientEmail = directtoClientEmail.Append("</Tr></Td>");

            directtoClientEmail = directtoClientEmail.Append("</Talbe>");
            directtoClientEmail = directtoClientEmail.Append("</body>");
            directtoClientEmail = directtoClientEmail.Append("</html >");

            return directtoClientEmail.ToString();
        }

        public static string CancelBookingMail(OL_Booking objBookingOL)
        {

            StringBuilder cancelBookingMail = new StringBuilder();

            cancelBookingMail = cancelBookingMail.Append("<html>");
            cancelBookingMail = cancelBookingMail.Append("<body>");
            cancelBookingMail = cancelBookingMail.Append("<Table>");
            cancelBookingMail = cancelBookingMail.Append("<Tr><Td align=Left>");
            cancelBookingMail = cancelBookingMail.Append("Dear Esteemed Guest,");
            cancelBookingMail = cancelBookingMail.Append("</td></Tr>");

            cancelBookingMail = cancelBookingMail.Append("<Tr><Td align=Left>");
            cancelBookingMail = cancelBookingMail.Append("We wish to inform you that your booking against booking  ID: " + objBookingOL.BookingId.ToString() + " has been cancelled successfully as per your request.");
            cancelBookingMail = cancelBookingMail.Append("</Td></Tr>");


            cancelBookingMail = cancelBookingMail.Append("<tr><Td align=Left>");
            cancelBookingMail = cancelBookingMail.Append(" The details of the booking are as follows. ");
            cancelBookingMail = cancelBookingMail.Append("</td></tr>");

            cancelBookingMail = cancelBookingMail.Append("<tr><td>&nbsp;</td></Tr>");

            cancelBookingMail = cancelBookingMail.Append("<Tr><Td align=Left><table celspacing='1' celpadding='2' border='1'>"); //Start table

            cancelBookingMail = cancelBookingMail.Append("<Tr><Td align=Left>");
            cancelBookingMail = cancelBookingMail.Append("Car booking ID       </td><td> " + objBookingOL.BookingId.ToString());
            cancelBookingMail = cancelBookingMail.Append("</td></Tr>");


            cancelBookingMail = cancelBookingMail.Append("<Tr><Td align=Left>");
            cancelBookingMail = cancelBookingMail.Append("Cancellation reason  </td><td> " + objBookingOL.CancelReason.ToString());
            cancelBookingMail = cancelBookingMail.Append("</td></Tr>");


            cancelBookingMail = cancelBookingMail.Append("<Tr><Td align=Left>");
            cancelBookingMail = cancelBookingMail.Append("Booking cancelled on </td><td> " + GeneralUtility.FormateDateSM(System.DateTime.Now.ToShortDateString()));
            cancelBookingMail = cancelBookingMail.Append("</td></Tr>");

            cancelBookingMail = cancelBookingMail.Append("</td></table>");  //Closed table
            cancelBookingMail = cancelBookingMail.Append("<tr><td>&nbsp;</td></Tr>");

            cancelBookingMail = cancelBookingMail.Append("<Tr><Td align=Left>");
            cancelBookingMail = cancelBookingMail.Append("Thank you again for choosing Carzonrent.");
            cancelBookingMail = cancelBookingMail.Append("</Tr></Td>");

            cancelBookingMail = cancelBookingMail.Append("</Talbe>");
            cancelBookingMail = cancelBookingMail.Append("</body>");
            cancelBookingMail = cancelBookingMail.Append("</html >");
            return cancelBookingMail.ToString();
        
        }

    }
}
