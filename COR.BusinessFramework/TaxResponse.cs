﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COR.BusinessFramework
{
    public class TaxResponse
    {
        public TaxResponse()
        {

        }

        public double ServiceTaxPercent;
        public double EduCessPercent;
        public double HduCessPercent;
        public double DSTPercent;
        public bool KMWisePackage;
        public double SwachhBharatTaxPercent;
        public double KrishiKalyanTaxPercent;
        public bool GSTEnabledYN;
        public double CGSTPercent;
        public double SGSTPercent;
        public double IGSTPercent;
        public int ClientGSTId;
    }

}