﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.DataFrameWork;
using System.Web;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using COR.BusinessFramework.SendSMS;

namespace COR.BusinessFramework
{
    public class BL_Booking
    {

        private DL_Booking objBookingDL = null;
        
        public OL_Booking GetCityName(OL_Booking objBookingOL)
        {
            try
            {
                objBookingDL = new DL_Booking();
                objBookingOL = objBookingDL.GetCityName(objBookingOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }

        public OL_Booking GetFacilitatorName(OL_Booking objBookingOL)
        {
            try
            {
                objBookingDL = new DL_Booking();
                objBookingOL = objBookingDL.GetFacilitatorName(objBookingOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }
        public OL_Booking GetCarModel(OL_Booking objBookingOL)
        {

            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.GetCarModel(objBookingOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");

            }
            return objBookingOL;
        }

        public OL_Booking CheckGuestRegistration(OL_Booking objBookingOL)
        {
            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.CheckGuestRegistration(objBookingOL);
            }
            catch (Exception Ex)
            {

                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;

        }

        public OL_Booking GetPreAuthDetails()
        {
            OL_Booking objBookingOL = new OL_Booking();
            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.GetPreAuthDetails();
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");

            }
            return objBookingOL;
        }
        public OL_Booking BookingConfirmation(OL_Booking objBookingOL)
        {
            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.CalculateIndicatedPrice(objBookingOL);
                if (objBookingOL.DbOperationStatus == CommonConstant.SUCCEED)
                {
                    if (objBookingOL.ObjDataTable.Rows.Count > 0)
                    {
                        //add gst tax start on 23-June-2017 
                        OL_TaxHeads taxdetails = new OL_TaxHeads();
                        taxdetails = TaxCalculateMethod(objBookingOL.ClientCoId, objBookingOL.PickUpCityId, objBookingOL.PickupDate, 0, 0);

                        objBookingOL.CGSTTaxPercent = taxdetails.CGSTPercent;
                        objBookingOL.SGSTTaxPercent = taxdetails.SGSTPercent;
                        objBookingOL.IGSTTaxPercent = taxdetails.IGSTPercent;
                        objBookingOL.ClientGSTId = taxdetails.ClientGSTId;

                        objBookingOL.IndicatedPrice = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["IndicatedPrice"].ToString());

                        if (taxdetails.GSTEnabledYN == true)
                        {
                            objBookingOL.CGSTTaxAmt = ((objBookingOL.IndicatedPrice * taxdetails.CGSTPercent) / 100);
                            objBookingOL.SGSTTaxAmt = ((objBookingOL.IndicatedPrice * taxdetails.SGSTPercent) / 100);
                            objBookingOL.IGSTTaxAmt = ((objBookingOL.IndicatedPrice * taxdetails.IGSTPercent) / 100);
                        }
                        else {
                            objBookingOL.CGSTTaxAmt = 0;
                            objBookingOL.SGSTTaxAmt = 0;
                            objBookingOL.IGSTTaxAmt = 0;                        
                        }

                        objBookingOL.IndicatedPrice = objBookingOL.IndicatedPrice + objBookingOL.CGSTTaxAmt + objBookingOL.SGSTTaxAmt + objBookingOL.IGSTTaxAmt;

                        //add gst tax end   on 23-June-2017


                        objBookingOL.IndicatedPkgID = Convert.ToInt32(objBookingOL.ObjDataTable.Rows[0]["PkgID"].ToString());
                        objBookingOL.IndicatedPkgRate = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["PkgRate"].ToString());
                        objBookingOL.IndicatedPkgHrs = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["PkgHrs"].ToString());
                        objBookingOL.IndicatedPkgKMs = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["PkgKMs"].ToString());
                        objBookingOL.IndicatedExtraHr = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["ExtraHrRate"].ToString());
                        objBookingOL.IndicatedExtraKM = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["ExtraKMRate"].ToString());
                        objBookingOL.IndicatedNightStayAmt = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["NightStayAllowance"].ToString());
                        objBookingOL.IndicatedOutStnAmt = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["OutStationAllowance"].ToString());
                        
                        objBookingOL.IndicatedPkgHrsTrue = Convert.ToInt16(objBookingOL.ObjDataTable.Rows[0]["PackageHrsTrue"].ToString());
                        objBookingOL.IndicatedDiscountPC = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["DiscountPC"].ToString());
                        //objBookingOL.ApprovalAmt = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["IndicatedPrice"])  +  (Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["IndicatedPrice"])*25/100);
                        //objBookingOL.ApprovalAmt = 300;
                       // objBookingOL.ApprovalAmt = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["IndicatedPrice"]);
                        objBookingOL.ApprovalAmt = objBookingOL.IndicatedPrice;
                        objBookingOL.IndicatedGSTSurChargeAmount =  Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["IndicatedGSTSurchargeAmount"].ToString());
                        objBookingOL.IsPackageAvailable = true;
                    }
                    else
                    {
                        {
                            objBookingOL.IsPackageAvailable = false;
                            objBookingOL.CommonMessage = "No package available.";
                            return objBookingOL;
                        }
                    }

                    //Get Service Unit 
                    objBookingOL = objBookingDL.GetServiceUnitId(objBookingOL);
                    if (objBookingOL.DbOperationStatus == CommonConstant.SUCCEED)
                    {
                        if (objBookingOL.ObjDataTable.Rows.Count > 0)
                        {
                            objBookingOL.ServiceUnitId = Convert.ToInt16(objBookingOL.ObjDataTable.Rows[0]["UnitID"].ToString());
                            objBookingOL.ServiceUnitName = objBookingOL.ObjDataTable.Rows[0]["UnitName"].ToString();
                        }
                        else
                        {
                            objBookingOL.CommonMessage = "Unit name not getting.";
                            return objBookingOL;
                        }
                    }
                    //Get ClientDetails
                    objBookingOL = objBookingDL.GetIndividualClientDetails(objBookingOL);
                    if (objBookingOL.DbOperationStatus == CommonConstant.SUCCEED)
                    {
                        if (objBookingOL.ObjDataTable.Rows.Count > 0)
                        {
                            objBookingOL.GuestName = objBookingOL.ObjDataTable.Rows[0]["clientCoInDivName"].ToString();
                            objBookingOL.GuestEmailId = objBookingOL.ObjDataTable.Rows[0]["EmailId"].ToString();
                            objBookingOL.GuestPhoneNo = objBookingOL.ObjDataTable.Rows[0]["PhoneNo"].ToString();
                            objBookingOL.ClientCoName = objBookingOL.ObjDataTable.Rows[0]["ClientCoName"].ToString();
                            objBookingOL.PaymentMode = objBookingOL.ObjDataTable.Rows[0]["PaymentTerms"].ToString();
                            objBookingOL.PreAuthNotRequire = Convert.ToInt16(objBookingOL.ObjDataTable.Rows[0]["PreAuthNotRequire"]);
                            objBookingOL.CCType = 1; //Onlye for data manage (Master  & visa charges on the same same.)

                        }
                    }
                    else
                    {
                        objBookingOL.CommonMessage = "guest details not getting.";
                        return objBookingOL;
                    }

                    //Get Track Id
                    objBookingOL = objBookingDL.CreateTackId(objBookingOL);

                    if (objBookingOL.IsPackageAvailable == true && objBookingOL.CCType == 1 && objBookingOL.ServiceUnitId != 0 && !string.IsNullOrEmpty(objBookingOL.Trackid))
                    {
                        objBookingOL.CreatedBy = 1;
                        objBookingOL = objBookingDL.CreateBooking(objBookingOL);
                        if (objBookingOL.BookingId > 0)
                        {
                            objBookingOL = objBookingDL.InsertBookingLog(objBookingOL);
                        }
                    }
                    else
                    {
                        objBookingOL.CommonMessage = "Either package is not available or service unit or card type is not corret.";
                        return objBookingOL;
                    }
                }

            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
                objBookingOL.CommonMessage = "Package is not available.";

            }
            return objBookingOL;
        }

        private OL_TaxHeads TaxCalculateMethod(int ClientCoID, int PickupCityID, DateTime DateIn, int CarID, int SubsidiaryID)
        {
            OL_TaxHeads objTaxHeads = new OL_TaxHeads();
            SendSMSSoapClient sendSMSProxy=new SendSMSSoapClient ();

            //string responseJSON = HTTPUtil.doGet(HTTPUtil.GetConfigurationValue("TaxSendSMS") + "?ClientCoID="
            //    + ClientCoID + "&PickupCityID=" + PickupCityID + "&DateIn=" + DateIn + "&CarID=" + CarID + "&SubsidiaryID=" + SubsidiaryID);

            TaxDetails objTax = sendSMSProxy.GetTaxHead(ClientCoID,PickupCityID,Convert.ToString(DateIn),CarID, SubsidiaryID);

            if (objTax != null && Convert.ToBoolean(objTax.GSTEnabledYN) == true)
            {
                objTaxHeads.CGSTPercent = Convert.ToDouble(objTax.CGSTPercent);
                objTaxHeads.SGSTPercent = Convert.ToDouble(objTax.SGSTPercent);
                objTaxHeads.IGSTPercent = Convert.ToDouble(objTax.IGSTPercent);
                objTaxHeads.ClientGSTId = Convert.ToInt32(objTax.ClientGSTId);
                objTaxHeads.GSTEnabledYN = Convert.ToBoolean(objTax.GSTEnabledYN);
            }
            else
            {
                objTaxHeads.CGSTPercent = 0;
                objTaxHeads.SGSTPercent = 0;
                objTaxHeads.IGSTPercent = 0;
                objTaxHeads.ClientGSTId = 0;
                objTaxHeads.GSTEnabledYN = false;
            }
            return objTaxHeads;
        }

        public OL_Booking UpdatePreAuthStatus(OL_Booking objBookingOL)
        {
            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.UpdatePreAuthStatus(objBookingOL);
            }
            catch (Exception Ex)
            {

                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }

        public OL_Booking UpdateNewPreAuthStatus(OL_Booking objBookingOL)
        {
            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.UpdateNewPreAuthStatus(objBookingOL);
            }
            catch (Exception Ex)
            {

                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }

        public OL_Booking GetBookingConfirmationDetails(OL_Booking objBookingOL)
        {
            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.GetBookingConfirmationDetails(objBookingOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }
        public OL_Booking GetSoldOut(OL_Booking objBookingOL)
        {
            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.GetSoldOut(objBookingOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }
        public OL_Booking GetGuestBookingDetails(OL_Booking objBookingOL)
        {
            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.GetGuestBookingDetails(objBookingOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }
        public OL_Booking UPdateSMSStatus(OL_Booking objBookingOL)
        { 
            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.UPdateSMSStatus(objBookingOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        }

        public void SaveLogError(OL_ErrorLogDetails objErrorLogDetailsOL)
        {  
            objBookingDL = new DL_Booking();
            objBookingDL.SaveLogError(objErrorLogDetailsOL);
        }

        public OL_Booking GetGuestDetailsOnMobileBasis(OL_Booking objBookingOL)
        {
            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.GetGuestDetailsOnMobileBasis(objBookingOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;
        
        }

        public OL_Booking GetFacilitatorDetails(OL_Booking objBookingOL)
        {
            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.GetFacilitatorDetails(objBookingOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objBookingOL;

        }
        public OL_Booking GetInplantName(OL_Booking objBookingOL)
        {
            try
            {
                objBookingDL = new DL_Booking();
                objBookingOL = objBookingDL.GetInplantName(objBookingOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }

    }
}