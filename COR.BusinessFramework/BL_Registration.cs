﻿//===============================================================================
//Created By :	<Krishna Kumar>
//Create date:  <20-06-2015>
//Description:	<Guest Registration business logic >
//===============================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using COR.SolutionFramework;
using COR.DataFrameWork;
using COR.ObjectFramework;

namespace COR.BusinessFramework
{
    public class BL_Registration
    {
        DL_Registration objRegistrationDL = null;
        public OL_Registration BindRegisteredGuest(OL_Registration objRegistrationOL)
        {            
            try
            {
                objRegistrationDL = new DL_Registration();
                objRegistrationOL = objRegistrationDL.BindRegisteredGuest(objRegistrationOL);
                return objRegistrationOL;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        public DataSet SearchClientByEmail(string EmailID)
        {
            try
            {
                DL_Registration objRegistrationDL = new DL_Registration();
                DataSet ds = new DataSet();
                ds = objRegistrationDL.SearchClientByEmail(EmailID);
                return ds;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        public OL_Registration RegisterGuest(OL_Registration objRegistrationOL)
        {
            try
            {
                objRegistrationDL = new DL_Registration();
                objRegistrationOL = objRegistrationDL.RegisterGuest(objRegistrationOL);
                return objRegistrationOL;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }
        public OL_Registration UpdateGuestStatus(OL_Registration objRegistrationOL)
        {
            try
            {
                objRegistrationDL = new DL_Registration();
                objRegistrationOL = objRegistrationDL.UpdateGuestStatus(objRegistrationOL);
                return objRegistrationOL;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        //public bool CheckPaymentCorporateModule(int ClientCoID)
        public ClientDetails CheckPaymentCorporateModule(int ClientCoID)
        {
            try
            {
                objRegistrationDL = new DL_Registration();
                return objRegistrationDL.CheckPaymentCorporateModule(ClientCoID);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        public string GetPaymateSysRegCodeForIndiv(int ClientCoIndivID)
        {
            try
            {
                objRegistrationDL = new DL_Registration();
                return objRegistrationDL.GetPaymateSysRegCodeForIndiv(ClientCoIndivID);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        public DataTable ExistingGuest(OL_Registration objRegistrationOL)
        {
            try
            {
                objRegistrationDL = new DL_Registration();
                return objRegistrationDL.ExistingGuest(objRegistrationOL);                
            }
            catch (Exception Ex)
            {
                
                throw new Exception(Ex.Message);
            }
        }
        public OL_Registration UpdateIndivInfo(OL_Registration objRegistrationOL)
        {
            try
            {
                objRegistrationDL = new DL_Registration();
                return objRegistrationDL.UpdateIndivInfo(objRegistrationOL);                
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }
        public DataTable GetRegistrationStatus(OL_Registration objRegistrationOL)
        {
            try
            {
                objRegistrationDL = new DL_Registration();
                return objRegistrationDL.GetRegistrationStatus(objRegistrationOL);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }
        public OL_Registration UpdateRegistrationStatus(OL_Registration objRegistrationOL)
        {
            try
            {
                objRegistrationDL = new DL_Registration();
                return objRegistrationDL.UpdateRegistrationStatus(objRegistrationOL);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        public OL_Registration UpdateCCStatus(OL_Registration objRegistrationOL)
        {
            try
            {
                objRegistrationDL = new DL_Registration();
                return objRegistrationDL.UpdateCCStatus(objRegistrationOL);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        public OL_Registration UpdateCCStatus_NewProcess(OL_Registration objRegistrationOL)
        {
            try
            {
                objRegistrationDL = new DL_Registration();
                return objRegistrationDL.UpdateCCStatus_NewProcess(objRegistrationOL);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        public OL_Registration SendRegistrationMailDetails(OL_Registration objRegistrationOL)
        {
            try
            {
                objRegistrationDL = new DL_Registration();
                return objRegistrationDL.SendRegistrationMailDetails(objRegistrationOL);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }
    }
}
