﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.DataFrameWork;

namespace COR.BusinessFramework
{
    public class BL_GuestUpload
    {
        private DL_GuestUpload objUploadguestDL = null;
        public OL_GuestUpload GetClientName(OL_GuestUpload objUploadguestOL)
        {
            try
            {
                objUploadguestDL = new DL_GuestUpload();
                objUploadguestOL = objUploadguestDL.GetClientName(objUploadguestOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objUploadguestOL;
        }


        public OL_GuestUpload GetClientCoAddID(OL_GuestUpload objUploadguestOL)
        {
            try
            {
                objUploadguestDL = new DL_GuestUpload();
                objUploadguestOL = objUploadguestDL.GetClientCoAddID(objUploadguestOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objUploadguestOL;
        }


        public OL_GuestUpload BulkGuest_Upload(OL_GuestUpload objUploadguestOL)
        {
            try
            {
                objUploadguestDL = new DL_GuestUpload();
                objUploadguestOL = objUploadguestDL.BulkGuest_Upload(objUploadguestOL);
                return objUploadguestOL;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        public OL_GuestUpload GetTopStatus(OL_GuestUpload objUploadguestOL)
        {
            objUploadguestDL = new DL_GuestUpload();
            try
            {
                objUploadguestOL = objUploadguestDL.GetTopStatus(objUploadguestOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objUploadguestOL;
        }


        public OL_GuestUpload GetDetailAftrSave(OL_GuestUpload objUploadguestOL)
        {
            objUploadguestDL = new DL_GuestUpload();
            try
            {
                objUploadguestOL = objUploadguestDL.GetDetailAftrSave(objUploadguestOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            }
            return objUploadguestOL;
        }

        public OL_GuestUpload SendBulkGuestUploadMailDetails(OL_GuestUpload objUploadguestOL)
        {
            try
            {
                objUploadguestDL = new DL_GuestUpload();
                return objUploadguestDL.SendBulkGuestUploadMailDetails(objUploadguestOL);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }



    }
}
