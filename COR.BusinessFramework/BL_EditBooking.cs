﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COR.ObjectFramework;
using COR.SolutionFramework;
using COR.DataFrameWork;
using Newtonsoft.Json;
using COR.BusinessFramework.SendSMS;

namespace COR.BusinessFramework
{
    public class BL_EditBooking
    {
        private DL_EditBooking objEditBookingDL = null;
        private DL_Booking objBookingDL = null;

        public OL_Booking EditBookingSearch(OL_Booking objBookingOL)
        {
            objEditBookingDL = new DL_EditBooking();
            try
            {  
                objBookingOL = objEditBookingDL.EditBookingSearch(objBookingOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }
        public OL_Booking GetEditBookingDetails(OL_Booking objBookingOL)
        {
            objEditBookingDL = new DL_EditBooking();
            try
            {   
                objBookingOL = objEditBookingDL.GetEditBookingDetails(objBookingOL);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            }
            return objBookingOL;
        }

        public OL_Booking EditBookingConfirmation(OL_Booking objBookingOL)
        {
            objEditBookingDL = new DL_EditBooking();
            objBookingDL = new DL_Booking();
            try
            {
                objBookingOL = objBookingDL.CalculateIndicatedPrice(objBookingOL);
                if (objBookingOL.DbOperationStatus == CommonConstant.SUCCEED)
                {
                    if (objBookingOL.ObjDataTable.Rows.Count > 0)
                    {
                        objBookingOL.IndicatedPkgID = Convert.ToInt32(objBookingOL.ObjDataTable.Rows[0]["PkgID"].ToString());
                        objBookingOL.IndicatedPkgRate = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["PkgRate"].ToString());
                        objBookingOL.IndicatedPkgHrs = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["PkgHrs"].ToString());
                        objBookingOL.IndicatedPkgKMs = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["PkgKMs"].ToString());
                        objBookingOL.IndicatedExtraHr = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["ExtraHrRate"].ToString());
                        objBookingOL.IndicatedExtraKM = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["ExtraKMRate"].ToString());
                        objBookingOL.IndicatedNightStayAmt = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["NightStayAllowance"].ToString());
                        objBookingOL.IndicatedOutStnAmt = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["OutStationAllowance"].ToString());

                        OL_TaxHeads taxdetails = new OL_TaxHeads();
                        taxdetails = TaxCalculationMethod(objBookingOL.ClientCoId, objBookingOL.PickUpCityId, objBookingOL.PickupDate, 0, 0);
                        objBookingOL.CGSTTaxPercent = taxdetails.CGSTPercent;
                        objBookingOL.SGSTTaxPercent = taxdetails.SGSTPercent;
                        objBookingOL.IGSTTaxPercent = taxdetails.IGSTPercent;
                        objBookingOL.ClientGSTId = taxdetails.ClientGSTId;

                        objBookingOL.IndicatedPrice = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["IndicatedPrice"].ToString());

                        if (taxdetails.GSTEnabledYN == true)
                        {
                            objBookingOL.CGSTTaxAmt = ((objBookingOL.IndicatedPrice * taxdetails.CGSTPercent) / 100);
                            objBookingOL.SGSTTaxAmt = ((objBookingOL.IndicatedPrice * taxdetails.SGSTPercent) / 100);
                            objBookingOL.IGSTTaxAmt = ((objBookingOL.IndicatedPrice * taxdetails.IGSTPercent) / 100);
                        }
                        else
                        {
                            objBookingOL.CGSTTaxAmt =0;
                            objBookingOL.SGSTTaxAmt =0;
                            objBookingOL.IGSTTaxAmt =0;
                        }

                        objBookingOL.IndicatedPrice = objBookingOL.IndicatedPrice + objBookingOL.CGSTTaxAmt + objBookingOL.SGSTTaxAmt + objBookingOL.IGSTTaxAmt;

                        objBookingOL.IndicatedPkgHrsTrue = Convert.ToInt16(objBookingOL.ObjDataTable.Rows[0]["PackageHrsTrue"].ToString());
                        objBookingOL.IndicatedDiscountPC = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["DiscountPC"].ToString());
                        objBookingOL.IndicatedGSTSurChargeAmount = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["IndicatedGSTSurchargeAmount"].ToString());
                        //objBookingOL.ApprovalAmt = 300;
                        //objBookingOL.ApprovalAmt = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[0]["IndicatedPrice"]);
                        objBookingOL.IsPackageAvailable = true;
                    }
                    else
                    {
                        {
                            objBookingOL.IsPackageAvailable = false;
                            objBookingOL.CommonMessage = "No package available.";
                            return objBookingOL;
                        }
                    }

                    //Get Service Unit 
                    objBookingOL = objBookingDL.GetServiceUnitId(objBookingOL);
                    if (objBookingOL.DbOperationStatus == CommonConstant.SUCCEED)
                    {
                        if (objBookingOL.ObjDataTable.Rows.Count > 0)
                        {
                            objBookingOL.ServiceUnitId = Convert.ToInt16(objBookingOL.ObjDataTable.Rows[0]["UnitID"].ToString());
                            objBookingOL.ServiceUnitName = objBookingOL.ObjDataTable.Rows[0]["UnitName"].ToString();
                        }
                        else
                        {
                            objBookingOL.CommonMessage = "Unit name not getting.";
                            return objBookingOL;
                        }
                    }
                    //Get ClientDetails
                    objBookingOL = objBookingDL.GetIndividualClientDetails(objBookingOL);
                    if (objBookingOL.DbOperationStatus == CommonConstant.SUCCEED)
                    {
                        if (objBookingOL.ObjDataTable.Rows.Count > 0)
                        {
                            objBookingOL.GuestName = objBookingOL.ObjDataTable.Rows[0]["clientCoInDivName"].ToString();
                            objBookingOL.GuestEmailId = objBookingOL.ObjDataTable.Rows[0]["EmailId"].ToString();
                            objBookingOL.GuestPhoneNo = objBookingOL.ObjDataTable.Rows[0]["PhoneNo"].ToString();
                            objBookingOL.ClientCoName = objBookingOL.ObjDataTable.Rows[0]["ClientCoName"].ToString();
                            objBookingOL.PaymentMode = objBookingOL.ObjDataTable.Rows[0]["PaymentTerms"].ToString();
                            objBookingOL.PreAuthNotRequire = Convert.ToInt16(objBookingOL.ObjDataTable.Rows[0]["PreAuthNotRequire"]);

                        }
                    }
                    else
                    {
                        objBookingOL.CommonMessage = "Client details not getting";
                        return objBookingOL;
                    }

                   

                    if (objBookingOL.IsPackageAvailable == true  && objBookingOL.ServiceUnitId != 0 )
                    {
                        objBookingOL.ModifiedBy = 1;
                        objBookingOL = objEditBookingDL.EditBooking(objBookingOL);
                    }
                    else
                    {
                        objBookingOL.CommonMessage = "Either is package not available or service unit is not correct.";
                        return objBookingOL;
                    }
                }

            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
                objBookingOL.CommonMessage = "Package is not available.";

            }
            return objBookingOL;
        }

        public OL_Booking CancelBooking(OL_Booking objBookingOL)
        {
            objEditBookingDL = new DL_EditBooking();
            try
            {
                objBookingOL = objEditBookingDL.CancelBooking(objBookingOL);

            }
            catch (Exception Ex)
            {   
                 ErrorLogClass.LogErrorToLogFile(Ex, "an error occured"); 
            }
            return objBookingOL;
        
        }

        private OL_TaxHeads TaxCalculationMethod(int ClientCoID, int PickupCityID, DateTime DateIn, int CarID, int SubsidiaryID)
        {
            OL_TaxHeads objTaxHeads = new OL_TaxHeads();
            SendSMSSoapClient sendSMSProxy = new SendSMSSoapClient();

            //string responseJSON = HTTPUtil.doGet(HTTPUtil.GetConfigurationValue("TaxSendSMS") + "?ClientCoID="
            //    + ClientCoID + "&PickupCityID=" + PickupCityID + "&DateIn=" + DateIn + "&CarID=" + CarID + "&SubsidiaryID=" + SubsidiaryID);

            TaxDetails objTax = sendSMSProxy.GetTaxHead(ClientCoID, PickupCityID, Convert.ToString(DateIn), CarID, SubsidiaryID);

            if (objTax != null && Convert.ToBoolean(objTax.GSTEnabledYN) == true)
            {
                objTaxHeads.CGSTPercent = Convert.ToDouble(objTax.CGSTPercent);
                objTaxHeads.SGSTPercent = Convert.ToDouble(objTax.SGSTPercent);
                objTaxHeads.IGSTPercent = Convert.ToDouble(objTax.IGSTPercent);
                objTaxHeads.ClientGSTId = Convert.ToInt32(objTax.ClientGSTId);
                objTaxHeads.GSTEnabledYN = Convert.ToBoolean(objTax.GSTEnabledYN);
            }
            else
            {
                objTaxHeads.CGSTPercent = 0;
                objTaxHeads.SGSTPercent = 0;
                objTaxHeads.IGSTPercent = 0;
                objTaxHeads.ClientGSTId = 0;
                objTaxHeads.GSTEnabledYN = false;
            }
            return objTaxHeads;
        }
    }
}
