﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Carzonrent:Login page </title>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="ScriptsReveal/jquery.reveal.js" type="text/javascript"></script>
    <link href="Styles/login.css" rel="stylesheet" type="text/css" />
    <link href="CSSReveal/reveal.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 8]>
<link href="CSS/login_ie8.css" type="text/css" rel="stylesheet" />
<![endif]-->
    <style type="text/css">
        input
        {
            border-radius: 8px;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            -o-border-radius: 8px;
            behavior: url(border-radius.htc);
        }
        input[type="button"]
        {
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -o-border-radius: 5px;
            behavior: url(border-radius.htc);
        }
        .login_area
        {
            border-radius: 10px;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            -o-border-radius: 10px;
            behavior: url(border-radius.htc);
            border: none;
            outline: none;
        }
        .top_heading
        {
            border-top-left-radius: 7px;
            border-top-right-radius: 7px;
            -moz-border-top-left-radius: 7px;
            -moz-border-top-right-radius: 7px;
            -khtml-border-top-left-radius: 7px;
            -khtml-border-top-right-radius: 7px;
            -webkit-border-top-left-radius: 7px;
            -webkit-border-top-right-radius: 7px;
            -o-border-top-left-radius: 7px;
            -o-border-top-right-radius: 7px;
            behavior: url(PIE.htc);
        }
    </style>
    <script type="text/javascript">
        function WaterMark(txtName, event) {
            var defaultText = "Enter login id";
            // Condition to check textbox length and event type
            if (txtName.value.length == 0 & event.type == "blur") {
                //if condition true then setting text color and default text in textbox
                txtName.style.color = "Gray";
                txtName.value = defaultText;
            }
            // Condition to check textbox value and event type
            if (txtName.value == defaultText & event.type == "focus") {
                txtName.style.color = "black";
                txtName.value = "";
            }
        }
        function WaterMarkPassword(txtName, event) {
            var defaultText = "Enter password";
            // Condition to check textbox length and event type
            if (txtName.value.length == 0 & event.type == "blur") {
                //if condition true then setting text color and default text in textbox
                txtName.style.color = "Gray";
                txtName.value = defaultText;
            }
            // Condition to check textbox value and event type
            if (txtName.value == defaultText & event.type == "focus") {
                txtName.style.color = "black";
                txtName.value = "";
            }
        }
        $(document).ready(function () {
            $("#bntLogin").click(function () {
                if ($("#<%=chkImplant.ClientID%>").is(":checked") == true) {
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "HTTPHandler/ImplantLogin.ashx",
                        data: { username: $("#txtLoginId").val(), Password: $("#txtPassword").val() },
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        success: function (response) {
                            if (response == "valid") {
                                location.href = "GuestProfile/GuestSearch.aspx"
                            }
                            else {
                                alert(response)
                                $("#txtLoginId").val("");
                                $("#txtPassword").val("");
                                $("#txtLoginId").focus();
                            }
                        },
                        error: function (msg) {
                            alert(msg.responseText);
                        }
                    });
                }
                else {
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "HTTPHandler/LoginUser.ashx",
                        data: { username: $("#txtLoginId").val(), Password: $("#txtPassword").val() },
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        success: function (response) {
                            if (response == "valid") {
                                //location.href = "HomePage.aspx"
                                location.href = "Booking/BookingSystem.aspx"
                            }
                            else {
                                alert(response)
                                $("#txtLoginId").val("");
                                $("#txtPassword").val("");
                                $("#txtLoginId").focus();
                            }
                        },
                        error: function (msg) {
                            alert(msg.responseText);
                        }
                    });
                }
                return false;
            });

            $("#bntCancel").click(function () {
                $(".close-reveal-modal").trigger("click");
            });

            

            $("#txtLoginIdRecovery").focusout(function () {
                //if (!ValidateEmail($("#txtLoginIdRecovery").val())) {
                if ($("#txtLoginIdRecovery").val()=="") {
                    alert("Enter valid login ID.")
                    $("#txtLoginIdRecovery").focus();
                    return false;
                }
                else {
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "HTTPHandler/PasswordRecovery.ashx",
                        data: { userId: $("#txtLoginIdRecovery").val(), ClientCoId: $("#hdClientId").val() },
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        success: function (response) {
                            if (response == "Valid") {
                                //$("#ResetPasswordButton").attr("disabled",true);
                                //$("#ResetPasswordButton").removeAttr("disabled");
                            }
                            else {
                                alert("Enter valid login email ID.")
                                return true;
                            }
                        },
                        error: function (msg) {
                            alert(msg.responseText);
                        }
                    });
                    return false;
                }
            });

            $("#ResetPasswordButton").click(function () {
                if (!ValidateEmail($("#txtLoginIdRecovery").val())) {
                    alert("Enter valid login ID.")
                    $("#txtLoginIdRecovery").focus();
                    return false;
                }
                else {

                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: "HTTPHandler/ChangePassword.ashx",
                        data: { userId: $("#txtLoginIdRecovery").val(), ClientCoId: $("#hdClientId").val() },
                        contentType: "application/json; charset=utf-8",
                        dataType: "text",
                        success: function (response) {
                            if (response == "Valid") {
                                alert("Your password has been sent to your email address.")
                                $("#txtLoginIdRecovery").val("");
                                $(".close-reveal-modal").trigger("click");
                            }
                            else {
                                alert("Enter valid login ID.")
                                $("#txtLoginIdRecovery").focus();
                            }
                        },
                        error: function (msg) {
                            alert(msg.responseText);
                        }
                    });
                    return false;
                }
            });

        });

        function clickButton(e, buttonid) {
            var evt = e ? e : window.event;
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (evt.keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
        function ValidateEmail(email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td colspan="2" align="left">
                    <a href="">
                        <img src="Images/COR-logo.gif" border="0" style="height: 58px; width: 213px" alt="" />
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <div class="login_area">
        <div class="top_heading">
            Login</div>
        <center>
            <div class="padding5">
                <table cellpadding="3" cellspacing="3">
                    <tr>
                        <td align="left">
                            Email ID
                        </td>
                        <td align="left" class="pl5">
                            Password
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="txtLoginId" size="20" maxlength="50" value="Enter login id"
                                onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);" />
                        </td>
                        <td class="pl5">
                            <input type="password" id="txtPassword" size="20" maxlength="50" value="Enter password"
                                onblur="WaterMarkPassword(this, event);" onfocus="WaterMarkPassword(this, event);"
                                onkeypress="return clickButton(event,'bntLogin');" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="lh">
                           <%-- <a href="Registration/GuestRegistration.aspx?ClientCoId=LH6wBMRr2c4=" title="click to paymate registration.">
                                New Registration</a>--%>
                           <a href="Registration/GuestRegistration.aspx?ClientCoId=3eJ/Vkl8jYo=" title="click to paymate registration.">New Registration</a>
                           <input type ="hidden" id="hdClientId" name ="hdClientId" value="3eJ/Vkl8jYo=" />
                        </td>
                        <td align="left" class="pl5">
                            <a href="#" class="big-link" data-reveal-id="myModal" title="click to reset password.">Forgot password?</a>
                        </td>
                    </tr>
                    <tr>
                        <td style ="vertical-align:middle;">
                          Implant login<asp:CheckBox ID="chkImplant" runat="server" Width ="2px"/>
                        </td>
                        <td align="right" valign="middle" colspan="1">
                            <input type="button" id="bntLogin" value="Login" class="button_quote" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 400px" id="myModal" class="reveal-modal">
                <fieldset style="border-color: Green">
                    <legend><b>Password Recovery</b></legend>
                    <table>
                        <tr>
                            <td colspan="2">
                                <h2>
                                    Password Recovery
                                </h2>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Use the form below to Reset your password.
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 22px">
                                Login ID: &nbsp;
                                <input type="text" id="txtLoginIdRecovery" size="20" maxlength="50" 
                                value="Enter login id" onblur="WaterMark(this, event);" onfocus="WaterMark(this, event);"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" id="ResetPasswordButton" value="Reset Password" class="button_quote"
                                    runat="server" />
                            </td>
                            <td>
                               <input type="button" id="bntCancel" value="Cancel" class="button_quote"/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <a class="close-reveal-modal" title="click to close"><b>&#215;</b></a>
            </div>
        </center>
    </div>
    </form>
</body>
</html>
