﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/CORMaster.master" AutoEventWireup="true"
    CodeFile="BookingCancel.aspx.cs" Inherits="Booking_BookingCancel" %>

<asp:Content ID="ContentPage" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $(function () {
            $("#liBooking").removeClass('active')
            $("#liBookingEdit").removeClass('active')
            $("#liBookingCancelDetails").removeClass('active')
            $("#liBookingUsreProfile").removeClass('active')
            $("#liBookingCancelSearch").addClass('active')
        });
    
    </script>
    <asp:UpdatePanel ID="udpCancelBooking" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="udpCancelBooking">
                <ProgressTemplate>
                    <div class="modal">
                        <div class="center">
                            <img src="../Images/ajax-loader.gif" alt="" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <table cellpadding="3" cellspacing="3" border="1">
                <tr>
                    <td colspan="4" align="center">
                        <b>Cancel Booking</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:Label ID="lblMesaage" Visible="false" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>BookingId</b>
                    </td>
                    <td>
                        <asp:Label ID="lblBookingId" runat="server"></asp:Label>
                    </td>
                    <td>
                        <b>Guest Name</b>
                    </td>
                    <td>
                        <asp:Label ID="lblGustName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Client Name</b>
                    </td>
                    <td>
                        <asp:Label ID="lblClientName" runat="server"></asp:Label>
                    </td>
                    <td>
                        <b>Pickup City Name</b>
                    </td>
                    <td>
                        <asp:Label ID="lblPickupcityName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Pickup date</b>
                    </td>
                    <td>
                        <asp:Label ID="lblPickUpdate" runat="server"></asp:Label>
                    </td>
                    <td>
                        <b>Pickup Time</b>
                    </td>
                    <td>
                        <asp:Label ID="lblpickupTime" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Drop Off date</b>
                    </td>
                    <td>
                        <asp:Label ID="lblDropOffdate" runat="server"></asp:Label>
                    </td>
                    <td>
                        <b>Drop Off time</b>
                    </td>
                    <td>
                        <asp:Label ID="lblDropoffTime" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Booked Model Name</b>
                    </td>
                    <td>
                        <asp:Label ID="lblBookedModelName" runat="server"></asp:Label>
                    </td>
                    <td>
                        <b>Booking Status</b>
                    </td>
                    <td>
                        <asp:Label ID="lblBookingStatus" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Cancel Reason</b>
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="txtCancelReason" runat="server" TextMode="MultiLine" Rows="1" Columns="4"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfvCancelReason" runat="server" ControlToValidate="txtCancelReason"
                            ont-Bold="true" ErrorMessage="*" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:HiddenField ID="hdGuestEmailId" runat="server" />
                        <asp:HiddenField ID="hdGuestMobileNo" runat="server" />
                        <asp:HiddenField ID="hdAdditionalEmailId" runat="server" />
                        <asp:HiddenField ID="hdAddintionMobileNo" runat="server" />
                        <asp:Button ID="bntCancel" runat="server" Text="Cancel" OnClick="bntCancel_Click" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
