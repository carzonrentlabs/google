﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/CORMaster.master" AutoEventWireup="true"
    CodeFile="BookingEdit.aspx.cs" Inherits="Booking_BookingEdit" %>

<asp:Content ID="ContentPage" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Scripts>
            <asp:ScriptReference Path="~/Scripts/jquery-1.9.0.js" />
            <asp:ScriptReference Path="~/Scripts/zebra_datepicker.js" />
            <asp:ScriptReference Path="~/Scripts/functions.js" />
        </Scripts>
    </asp:ScriptManagerProxy>
    <script type="text/javascript">
        $(function () {
            $("#liBooking").removeClass('active')
            $("#liBookingEdit").addClass('active')
            $("#liBookingCancelDetails").removeClass('active')
            $("#liBookingUsreProfile").removeClass('active')
            $("#liBookingCancelSearch").removeClass('active')
        });

        function pageLoad(sender, args) {

            var implant = '<%=Session["Implant"].ToString()%>';
            if (implant == 0) {
                $('#<%=txtPickUpdate.ClientID%>').Zebra_DatePicker({
                    direction: true,
                    pair: $('#datepicker-example7-end')
                });
                $('#<%=txtDropOffdate.ClientID%>').Zebra_DatePicker({
                    direction: true,
                    pair: $('#datepicker-example7-end')
                });
            }
            else {
                $('#<%=txtPickUpdate.ClientID%>').Zebra_DatePicker({
                    //  direction: true,
                    //  pair: $('#datepicker-example7-end')
                });
                $('#<%=txtDropOffdate.ClientID%>').Zebra_DatePicker({
                    //  direction: true,
                    //  pair: $('#datepicker-example7-end')
                });
            }
            $("#<%=txtAdditionalMobileNo.ClientID%>").keyup(function () {

                var strPass = $("#<%=txtAdditionalMobileNo.ClientID%>").val();
                var strLength = strPass.length;
                var lchar = strPass.charAt((strLength) - 1);
                var cCode = CalcKeyCode(lchar);
                if (cCode < 46 || cCode > 57 || cCode == 47) {
                    var myNumber = strPass.substring(0, (strLength) - 1);
                    $("#<%=txtAdditionalMobileNo.ClientID%>").val(myNumber);
                    alert("Enter only numeric value.")
                }
            });

            $("#<%=ddlScheme.ClientID%>").change(function () {
                if ($("#<%=txtPickUpdate.ClientID%>").val() == "") {
                    alert("Select Pick Up date.")
                    $("#<%=txtPickUpdate.ClientID%>").focus();
                    return false;
                }
            });


            $("#<%=bntEditBooking.ClientID%>").click(function () {

                var strDatePickup = $("#<%=txtPickUpdate.ClientID%>").val();
                var strDateDropOff = $("#<%=txtDropOffdate.ClientID%>").val();

                var PickupDate = new Date(strDatePickup.split("-")[2], strDatePickup.split("-")[0], strDatePickup.split("-")[1]);
                var DropOffDate = new Date(strDateDropOff.split("-")[2], strDateDropOff.split("-")[0], strDateDropOff.split("-")[1]);

                var strPickuTime = $("#<%=ddlPickUpHr.ClientID%>").val() + $("#<%=ddlDropOffMin.ClientID%>").val();
                var strDroptime = $("#<%=ddlDropOffHr.ClientID%>").val() + $("#<%=ddlDropOffMin.ClientID%>").val();

                if ($("#<%=ddlPickUpCity.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select Pick Up City.")
                    $("#<%=ddlPickUpCity.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlCarModel.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select Car Model.")
                    $("#<%=ddlCarModel.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtPickUpdate.ClientID%>").val() == "") {
                    alert("Select Pick Up date.")
                    $("#<%=txtPickUpdate.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtDropOffdate.ClientID%>").val() == "") {
                    alert("Select Drop off date.")
                    $("#<%=txtDropOffdate.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlScheme.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select scheme.")
                    $("#<%=ddlScheme.ClientID%>").focus();
                    return false;
                }
                /*else if (Date.parse(PickupDate) > Date.parse(DropOffDate)) {
                alert("Pick up date should not be greater then drop off date.");
                return false;
                }*/
                else if (Date.parse(strDatePickup) > Date.parse(strDateDropOff)) {
                    alert("Pick up date should not be greater then drop off date.");
                    return false;
                }
                else if ((Date.parse(PickupDate) == Date.parse(DropOffDate)) && (strPickuTime == strDroptime)) {
                    alert("Pick up  time and drop off time can't same on the same date.")
                    $("#<%=ddlDropOffHr.ClientID%>").focus();
                    return false;
                }
                else if ((Date.parse(PickupDate) == Date.parse(DropOffDate)) && (parseInt(strPickuTime) > parseInt(strDroptime))) {
                    alert("Pick up time  can't  be greater thane drop off time on the same date.")
                    $("#<%=ddlDropOffHr.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtAdditionalMobileNo.ClientID%>").val() != "" && $("#<%=txtAdditionalMobileNo.ClientID%>").val().length < 10) {
                    alert("Enter 10 digit mobile number.");
                    $("#<%=txtAdditionalMobileNo.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtAdditionalMobileNo.ClientID%>").val() != "" && isNaN($("#<%=txtAdditionalMobileNo.ClientID%>").val())) {
                    alert("Mobile number should numeric value only.");
                    $("#<%=txtAdditionalMobileNo.ClientID%>").focus();
                    return false;
                }
                //                else if ($("#<%=chkEscort.ClientID%>").is(':checked') && (($("#<%=ddlPickUpHr.ClientID%>").val() > 6 && $("#<%=ddlPickUpHr.ClientID%>").val() < 20) || ($("#<%=ddlDropOffHr.ClientID%>").val() > 6 && $("#<%=ddlDropOffHr.ClientID%>").val() < 20))) {
                //                    alert("Escort request only available between 8 PM to 6 AM")
                //                    return false;
                //                }
            });

            $("#<%=chkEscort.ClientID%>").change(function () {
                if ($("#<%=ddlPickUpCity.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select Pick Up City.")
                    $("#<%=ddlPickUpCity.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlCarModel.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select Car Model.")
                    $("#<%=ddlCarModel.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtPickUpdate.ClientID%>").val() == "") {
                    alert("Select Pick Up date.")
                    $("#<%=txtPickUpdate.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtDropOffdate.ClientID%>").val() == "") {
                    alert("Select Drop off date.")
                    $("#<%=txtDropOffdate.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlScheme.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select scheme.")
                    $("#<%=ddlScheme.ClientID%>").focus();
                    return false;
                }
            });
        }
        function BookingProgress(strid) {

        }
        function CalcKeyCode(aChar) {
            var character = aChar.substring(0, 1);
            var code = aChar.charCodeAt(0);
            return code;
        }
    </script>
    <asp:UpdatePanel ID="udpCreateBooking" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="udpCreateBooking">
                <ProgressTemplate>
                    <div class="modal">
                        <div class="center">
                            <img src="../Images/ajax-loader.gif" alt="" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <table class="booking_cor" cellpadding="1" cellspacing="1" border="0" width="60%">
                <tr>
                    <td class="bookings_h" colspan="3" align="center">
                        <b>Edit Booking</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <asp:Label ID="lblMesaage" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Pick-up/source city</b>
                    </td>
                    <td class="left">
                        <asp:DropDownList ID="ddlPickUpCity" runat="server" OnSelectedIndexChanged="ddlPickUpCity_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rvf_PickUpcity" runat="server" ControlToValidate="ddlPickUpCity"
                            InitialValue="0" ErrorMessage="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Car Model</b>
                    </td>
                    <td class="left">
                        <asp:DropDownList ID="ddlCarModel" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rvf_CarModel" runat="server" ControlToValidate="ddlCarModel"
                            InitialValue="0" ErrorMessage="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Pick-up date</b>
                    </td>
                    <td class="left">
                        <asp:TextBox ID="txtPickUpdate" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rvf_PickpUpdate" runat="server" Font-Bold="True"
                            ControlToValidate="txtPickUpdate" ErrorMessage="*" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Pick-up time</b>
                    </td>
                    <td class="left">
                        <div class="w_49 f_l mr_2">
                            <asp:DropDownList ID="ddlPickUpHr" runat="server" OnSelectedIndexChanged="ddlPickUpHr_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                        <div class="w_49 f_l">
                            <asp:DropDownList ID="ddlPickUpMin" runat="server" OnSelectedIndexChanged="ddlPickUpMin_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Rental Type</b>
                    </td>
                    <td class="left">
                        <asp:DropDownList ID="ddlScheme" runat="server" OnSelectedIndexChanged="ddlScheme_SelectedIndexChanged"
                            AutoPostBack="true">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Half Day" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Full Day" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Airport Transfer" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfv_ddlScheme" runat="server" ControlToValidate="ddlScheme"
                            InitialValue="0" ErrorMessage="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Drop-off date</b>
                    </td>
                    <td class="left">
                        <asp:TextBox ID="txtDropOffdate" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfv_DropoffDate" runat="server" Font-Bold="True"
                            ControlToValidate="txtDropOffdate" ErrorMessage="*" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Drop-off time</b>
                    </td>
                    <td class="left">
                        <div class="w_49 f_l mr_2">
                            <asp:DropDownList ID="ddlDropOffHr" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="w_49 f_l">
                            <asp:DropDownList ID="ddlDropOffMin" runat="server">
                            </asp:DropDownList>
                        </div>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Pick-up location / Addres</b>
                    </td>
                    <td class="left">
                        <asp:TextBox ID="txtPickUPAddress" runat="server" TextMode="MultiLine" Rows="2" Columns="17"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfvPickupAddres" ControlToValidate="txtPickUPAddress"
                            runat="server" Font-Bold="true" ErrorMessage="*" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Remarks</b>
                    </td>
                    <td class="left">
                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="2" Columns="17"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Escort Request </b><font color="red">(8 PM to 6 AM) </font>
                    </td>
                    <td class="left">
                        <asp:CheckBox ID="chkEscort" runat="server" OnCheckedChanged="chkEscort_CheckedChanged"
                            AutoPostBack="true" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Send Email</b>
                    </td>
                    <td class="left">
                        <asp:CheckBox ID="chkEmail" runat="server" Checked="true" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Additional EmaiId</b>
                    </td>
                    <td class="left">
                        <asp:TextBox runat="server" ID="txtadditionalEmaiId" MaxLength="100"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="revAdditionalEmailId" runat="server" ErrorMessage="*"
                            ControlToValidate="txtadditionalEmaiId" Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Additional Mobile No</b>
                    </td>
                    <td class="left">
                        <asp:TextBox runat="server" ID="txtAdditionalMobileNo" MaxLength="10"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <asp:Button ID="bntEditBooking" runat="server" Text="Edit" CausesValidation="true"
                            OnClick="bntEditBooking_Click" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
