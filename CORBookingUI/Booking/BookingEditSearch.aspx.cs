﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.ObjectFramework;
using COR.SolutionFramework;
using COR.BusinessFramework;
using System.Drawing;

public partial class Booking_BookingEditSearch : System.Web.UI.Page
{
    private OL_Booking objBookingOL;
    private BL_EditBooking objEditBookingBL;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblMesaage.Text = "";
        lblMesaage.Visible = false;
    }

    protected void bntSearch_Click(object sender, EventArgs e)
    {
        objBookingOL = new OL_Booking();
        objEditBookingBL = new BL_EditBooking();
        try
        {
            if (!string.IsNullOrEmpty(txtBookingId.Text.ToString()))
            {
                objBookingOL.BookingId = Convert.ToInt32(txtBookingId.Text.ToString());
                Session["bookingId"] = objBookingOL.BookingId;
                objBookingOL.ClientCoIndivID = Convert.ToInt32(Session["ClientCoIndivID"]);
                objBookingOL = objEditBookingBL.EditBookingSearch(objBookingOL);
                if (objBookingOL.ObjDataTable.Rows.Count > 0)
                {
                    tblDetailsShows.Visible = true;
                    lblBookingid.Text = objBookingOL.BookingId.ToString();
                    lblGuestName.Text = objBookingOL.ObjDataTable.Rows[0]["GuestName"].ToString ();
                    lblClientName.Text = objBookingOL.ObjDataTable.Rows[0]["ClientCoName"].ToString();
                    lblPickupCityName.Text = objBookingOL.ObjDataTable.Rows[0]["CityName"].ToString();
                    lblPickupdate.Text = objBookingOL.ObjDataTable.Rows[0]["PickUpDate"].ToString();
                    lblPickupTime.Text = objBookingOL.ObjDataTable.Rows[0]["PickUpTime"].ToString();
                    lblDropOffDate.Text = objBookingOL.ObjDataTable.Rows[0]["DropOffDate"].ToString();
                    lblDropOffTime.Text = objBookingOL.ObjDataTable.Rows[0]["DropOffTime"].ToString();
                    hdlLink.NavigateUrl = "BookingEdit.aspx";
                   

                }
                else
                {
                    tblDetailsShows.Visible = false;
                    lblMesaage.Visible = true;
                    lblMesaage.Text = "No booking found";
                    lblMesaage.ForeColor = Color.Red;
                }

            }
            else
            {
                lblMesaage.Visible = true;
                lblMesaage.Text = "Enter booking id";
                lblMesaage.ForeColor = Color.Red;
            }


        }
        catch (Exception Ex)
        {
            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            throw new Exception(Ex.Message);

        }

    }
}