﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookingSystem.aspx.cs" Inherits="Booking_BookingSystem"
    MasterPageFile="~/MasterPage/CORMaster.master" %>

<asp:Content ID="ContentPage" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Scripts>
            <asp:ScriptReference Path="http://maps.googleapis.com/maps/api/js?sensor=true&client=gme-carzonrentindiapvt&v=3.20&libraries=places" />
            <asp:ScriptReference Path="~/Scripts/GoogleMapAPIWrapper.js" />
            <asp:ScriptReference Path="~/Scripts/jquery-1.11.3.min.js" />
            <asp:ScriptReference Path="~/Scripts/bootstrap.js" />
            <asp:ScriptReference Path="~/Scripts/jquery-1.9.0.js" />
            <asp:ScriptReference Path="~/Scripts/zebra_datepicker.js" />
            <asp:ScriptReference Path="~/Scripts/functions.js" />
        </Scripts>
    </asp:ScriptManagerProxy>
    <script type="text/javascript">


        function pageLoad(sender, args) {

            var implant = '<%=Session["Implant"].ToString()%>';
            if (implant == 0) {
                $('#<%=txtPickUpdate.ClientID%>').Zebra_DatePicker({
                    direction: true,
                    pair: $('#datepicker-example7-end')
                });
                $('#<%=txtDropOffdate.ClientID%>').Zebra_DatePicker({
                    direction: true,
                    pair: $('#datepicker-example7-end')
                });
            }
            else {
                $('#<%=txtPickUpdate.ClientID%>').Zebra_DatePicker({
                    //direction: false,
                    //pair: $('#datepicker-example7-end')
                });
                $('#<%=txtDropOffdate.ClientID%>').Zebra_DatePicker({
                    // direction: false,
                    // pair: $('#datepicker-example7-end')
                });
            }
           

            $('#city_location_text').keydown(function (e) {
                if (e.which == 13 && $('.input-large:visible').length) return false;
            });



            $("#<%=txtPickUPAddress.ClientID%>,#<%=txtRemarks.ClientID%>").keyup(function () {
                controlId = "#" + this.id;
                data = $(controlId).val();
                var iChars = "!@#$%^&*()+=[]\';{}|:<>?\\";
                for (var i = 0; i < data.length; i++) {
                    if (iChars.indexOf(data.charAt(i)) != -1) {
                        alert("Your string has special characters.These are not allowed.");
                        var myString = $(controlId).val().substring(0, (data.length) - 1);
                        $(controlId).val(myString);
                        return false;
                    }
                }
            });

            $("#<%=txtAdditionalMobileNo.ClientID%>").keyup(function () {

                var strPass = $("#<%=txtAdditionalMobileNo.ClientID%>").val();
                var strLength = strPass.length;
                var lchar = strPass.charAt((strLength) - 1);
                var cCode = CalcKeyCode(lchar);
                if (cCode < 46 || cCode > 57 || cCode == 47) {
                    var myNumber = strPass.substring(0, (strLength) - 1);
                    $("#<%=txtAdditionalMobileNo.ClientID%>").val(myNumber);
                    alert("Enter only numeric value.")
                }
            });

            $("#<%=ddlScheme.ClientID%>").change(function () {
                if ($("#<%=txtPickUpdate.ClientID%>").val() == "") {
                    alert("Select Pick Up date.")
                    $("#<%=txtPickUpdate.ClientID%>").focus();
                    return false;
                }
            });

            $("#<%=bntBooking.ClientID%>").click(function () {

                var strDatePickup = $("#<%=txtPickUpdate.ClientID%>").val();
                var strDateDropOff = $("#<%=txtDropOffdate.ClientID%>").val();

                var PickupDate = new Date(strDatePickup.split("-")[2], strDatePickup.split("-")[0], strDatePickup.split("-")[1]);
                var DropOffDate = new Date(strDateDropOff.split("-")[2], strDateDropOff.split("-")[0], strDateDropOff.split("-")[1]);

                var strPickuTime = $("#<%=ddlPickUpHr.ClientID%>").val() + $("#<%=ddlDropOffMin.ClientID%>").val();
                var strDroptime = $("#<%=ddlDropOffHr.ClientID%>").val() + $("#<%=ddlDropOffMin.ClientID%>").val();

                if ($("#<%=ddlPickUpCity.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select Pick Up City.")
                    $("#<%=ddlPickUpCity.ClientID%>").focus();
                    return false;
                }
               <%-- else if ($("#<%=ddlfacilitator.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select Facilitator.")
                    $("#<%=ddlfacilitator.ClientID%>").focus();
                    return false;
                }--%>
                else if ($("#<%=ddlCarModel.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select Car Model.")
                    $("#<%=ddlCarModel.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtPickUpdate.ClientID%>").val() == "") {
                    alert("Select Pick Up date.")
                    $("#<%=txtPickUpdate.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtDropOffdate.ClientID%>").val() == "") {
                    alert("Select Drop off date.")
                    $("#<%=txtDropOffdate.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlScheme.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select scheme.")
                    $("#<%=ddlScheme.ClientID%>").focus();
                    return false;
                }
                /*else if (Date.parse(PickupDate) > Date.parse(DropOffDate)) {
                alert("Pick up date should not be greater then drop off date.");
                return false;
                }*/
                else if (Date.parse(strDatePickup) > Date.parse(strDateDropOff)) {
                    alert("Pick up date should not be greater then drop off date.");
                    return false;
                }
                else if ((Date.parse(PickupDate) == Date.parse(DropOffDate)) && (strPickuTime == strDroptime)) {
                    alert("Pick up  time and drop off time can't same on the same date.")
                    $("#<%=ddlDropOffHr.ClientID%>").focus();
                    return false;
                }
                else if ((Date.parse(PickupDate) == Date.parse(DropOffDate)) && (parseInt(strPickuTime) > parseInt(strDroptime))) {
                    alert("Pick up time  can't  be greater thane drop off time on the same date.")
                    $("#<%=ddlDropOffHr.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtAdditionalMobileNo.ClientID%>").val() != "" && $("#<%=txtAdditionalMobileNo.ClientID%>").val().length < 10) {
                    alert("Enter 10 digit mobile number.");
                    $("#<%=txtAdditionalMobileNo.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtAdditionalMobileNo.ClientID%>").val() != "" && isNaN($("#<%=txtAdditionalMobileNo.ClientID%>").val())) {
                    alert("Mobile number should numeric value only.");
                    $("#<%=txtAdditionalMobileNo.ClientID%>").focus();
                    return false;
                }
                else if ($("#city_location_text").val() != "" && ($("#<%=city_location_lat.ClientID%>").val() == "" || $("#<%=city_location_long.ClientID%>").val() == "")) {
                    alert("Select guest pickup location.");
                    $("#city_location_text").focus();
                    return false;
                }
                //                else if ($("#<%=chkEscort.ClientID%>").is(':checked') && (($("#<%=ddlPickUpHr.ClientID%>").val() > 6 && $("#<%=ddlPickUpHr.ClientID%>").val() < 20) || ($("#<%=ddlDropOffHr.ClientID%>").val() > 6 && $("#<%=ddlDropOffHr.ClientID%>").val() < 20))) {
                //                    alert("Escort request only available between 8 PM to 6 AM")
                //                    return false;
                //                }
            });

            $("#<%=chkEscort.ClientID%>").change(function () {
                if ($("#<%=ddlPickUpCity.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select Pick Up City.")
                    $("#<%=ddlPickUpCity.ClientID%>").focus();
                    return false;
                }
               <%-- else if ($("#<%=ddlfacilitator.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select Facilitator.")
                    $("#<%=ddlfacilitator.ClientID%>").focus();
                    return false;
                }--%>
                else if ($("#<%=ddlCarModel.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select Car Model.")
                    $("#<%=ddlCarModel.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtPickUpdate.ClientID%>").val() == "") {
                    alert("Select Pick Up date.")
                    $("#<%=txtPickUpdate.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtDropOffdate.ClientID%>").val() == "") {
                    alert("Select Drop off date.")
                    $("#<%=txtDropOffdate.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=ddlScheme.ClientID%>")[0].selectedIndex == 0) {
                    alert("Select scheme.")
                    $("#<%=ddlScheme.ClientID%>").focus();
                    return false;
                }
            });

            // start boot strap 
            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                                            sURLVariables = sPageURL.split('&'),
                                            sParameterName,
                                            i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };


            var office_location_prediction_autocomplete = null;
            function officeLocationAutoCompleteHandler() {
                if (office_location_prediction_autocomplete != null) {
                    var place = office_location_prediction_autocomplete.getPlace();
                    $("#<%=city_location_lat.ClientID%>").val(place.geometry.location.lat());
                    $("#<%=city_location_long.ClientID%> ").val(place.geometry.location.lng());

                }
            }

            function initializePlaceAutoComplete(element_id, callback_function, element_obj) {
                var input = document.getElementById(element_id);
                if (element_obj != null) {
                    input = element_obj;
                }
                if (input != null) {
                    var options = {};
                }
                options['componentRestrictions'] = { country: 'in' };
                if (google == null || google.maps == null) {
                    loading();
                    alert("OOPS. There was a network error in loading this page. Please reload");
                    return;
                }
                office_location_prediction_autocomplete = new google.maps.places.Autocomplete(input, options);
                google.maps.event.addListener(office_location_prediction_autocomplete, "place_changed", callback_function, false);

            }

            $(function () {
                initializePlaceAutoComplete("city_location_text", officeLocationAutoCompleteHandler);
                // getCities();
            });

            function parseJSON(obj) {
                if (obj.constructor === "test".constructor) {
                    return JSON.parse(obj);
                }
                return obj;
            }

            //End of bootstrap
        }
        function BookingProgress(strid) {

        }

        function CalcKeyCode(aChar) {
            var character = aChar.substring(0, 1);
            var code = aChar.charCodeAt(0);
            return code;
        }
            

    </script>
    <asp:UpdatePanel ID="udpCreateBooking" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="udpCreateBooking">
                <ProgressTemplate>
                    <div class="modal">
                        <div class="center">
                            <img src="../Images/ajax-loader.gif" alt="" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <table class="booking_cor" cellpadding="1" cellspacing="1" border="0" width="60%">
                <tr>
                    <td class="bookings_h" colspan="3" align="center">
                        <b>Booking System</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <asp:Label ID="lblMesaage" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Pick-up/source city</b>
                    </td>
                    <td class="left">
                        <asp:DropDownList ID="ddlPickUpCity" runat="server" OnSelectedIndexChanged="ddlPickUpCity_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rvf_PickUpcity" runat="server" ControlToValidate="ddlPickUpCity"
                            InitialValue="0" ErrorMessage="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                 <tr>
                    <td class="left">
                        <b>Facilitator</b>
                    </td>
                    <td class="left">
                        <asp:DropDownList ID="ddlfacilitator" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                       <%-- <asp:RequiredFieldValidator ID="rvf_facilitator" runat="server" ControlToValidate="ddlfacilitator"
                            InitialValue="0" ErrorMessage="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Car Model</b>
                    </td>
                    <td class="left">
                        <asp:DropDownList ID="ddlCarModel" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rvf_CarModel" runat="server" ControlToValidate="ddlCarModel"
                            InitialValue="0" ErrorMessage="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Pick-up date</b>
                    </td>
                    <td class="left">
                        <asp:TextBox ID="txtPickUpdate" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rvf_PickpUpdate" runat="server" Font-Bold="True"
                            ControlToValidate="txtPickUpdate" ErrorMessage="*" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Pick-up time</b>
                    </td>
                    <td class="left">
                        <div class="w_49 f_l mr_2">
                            <asp:DropDownList ID="ddlPickUpHr" runat="server" OnSelectedIndexChanged="ddlPickUpHr_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                        <div class="w_49 f_l">
                            <asp:DropDownList ID="ddlPickUpMin" runat="server" OnSelectedIndexChanged="ddlPickUpMin_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Rental Type</b>
                    </td>
                    <td class="left">
                        <asp:DropDownList ID="ddlScheme" runat="server" OnSelectedIndexChanged="ddlScheme_SelectedIndexChanged"
                            AutoPostBack="true">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Half Day" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Full Day" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Airport Transfer" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfv_ddlScheme" runat="server" ControlToValidate="ddlScheme"
                            InitialValue="0" ErrorMessage="*" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Drop-off date</b>
                    </td>
                    <td class="left">
                        <asp:TextBox ID="txtDropOffdate" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfv_DropoffDate" runat="server" Font-Bold="True"
                            ControlToValidate="txtDropOffdate" ErrorMessage="*" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Drop-off time</b>
                    </td>
                    <td class="left">
                        <div class="w_49 f_l mr_2">
                            <asp:DropDownList ID="ddlDropOffHr" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="w_49 f_l">
                            <asp:DropDownList ID="ddlDropOffMin" runat="server">
                            </asp:DropDownList>
                        </div>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Pick-up location / Addres</b>
                    </td>
                    <td class="left">
                        <asp:TextBox ID="txtPickUPAddress" runat="server" TextMode="MultiLine" Rows="2" Columns="17"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfvPickupAddres" ControlToValidate="txtPickUPAddress"
                            runat="server" Font-Bold="true" ErrorMessage="*" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Guest Pickup Location</b>
                    </td>
                    <td class="left">
                        <input class="input-large" type="text" id="city_location_text">
                    </td>
                    <td>
                        <input id="city_location_lat" runat="server" type="hidden" />
                        <input id="city_location_long" runat="server" type="hidden" />
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Remarks</b>
                    </td>
                    <td class="left">
                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="2" Columns="17"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="left" style="white-space:nowrap;">
                        <b>Escort Request </b><font color="red">(8 PM to 6 AM)</font>
                    </td>
                    <td class="left">
                        <asp:CheckBox ID="chkEscort" runat="server" OnCheckedChanged="chkEscort_CheckedChanged"
                            AutoPostBack="true" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Send Email</b>
                    </td>
                    <td class="left">
                        <asp:CheckBox ID="chkEmail" runat="server" Checked="true" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Additional EmaiId</b>
                    </td>
                    <td class="left">
                        <asp:TextBox runat="server" ID="txtadditionalEmaiId" MaxLength="100"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="revAdditionalEmailId" runat="server" ErrorMessage="*"
                            ControlToValidate="txtadditionalEmaiId" Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td class="left">
                        <b>Additional Mobile No</b>
                    </td>
                    <td class="left">
                        <asp:TextBox runat="server" ID="txtAdditionalMobileNo" MaxLength="10"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <asp:Button ID="bntBooking" runat="server" Text="Book" OnClick="bntBooking_Click"
                            CausesValidation="true"/>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <%--<Triggers>
            <asp:PostBackTrigger ControlID="bntBooking" />
        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>
