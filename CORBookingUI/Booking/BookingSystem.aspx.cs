﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.BusinessFramework;
using COR.BusinessFramework.SendSMS;
using System.Drawing;
using System.Text;
using System.Net;
using System.IO;


public partial class Booking_BookingSystem : System.Web.UI.Page
{

    private OL_Booking objBookingOL = null;
    private BL_Booking objBookingBL = null;
    SendSMSSoapClient sendSMSPoxy = new SendSMSSoapClient();
    private OL_ErrorLogDetails objErrorLogDetailsOL = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        chkEmail.Checked = true;
        if (!Page.IsPostBack)
        {
            BindCity();
            BindPickUpTimeHours();          // For Selection of Pickup Hours
            BindPickUPTimeMinute();         // For Selection of Pickup Minute
            BindDropOffHours();             // For Selection of Drop off Hours
            BindDropOffMintue();            // For Selection of Drop off Minute      
            BindFacilitator();              // Get Facilitaor candidate
        }
    }



    protected void ddlPickUpCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        objBookingOL = new OL_Booking();
        objBookingBL = new BL_Booking();

        try
        {
            if (ddlPickUpCity.SelectedIndex > 0)
            {
                objBookingOL.PickUpCityId = Convert.ToInt32(ddlPickUpCity.SelectedValue);
                objBookingOL.ClientCoId = Convert.ToInt32(Session["ClientCoId"]);
                objBookingOL = objBookingBL.GetCarModel(objBookingOL);
                if (objBookingOL.ObjDataTable.Rows.Count > 0)
                {
                    ddlCarModel.DataSource = objBookingOL.ObjDataTable;
                    ddlCarModel.DataTextField = "CarModel";
                    ddlCarModel.DataValueField = "CarModelID";
                    ddlCarModel.DataBind();
                    ddlCarModel.Items.Insert(0, new ListItem("--Select--", "0"));
                }
                else
                {
                    ddlCarModel.DataSource = null;
                    ddlCarModel.DataBind();
                }

            }
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            throw new Exception(Ex.Message);
        }
    }

    protected void chkEscort_CheckedChanged(object sender, EventArgs e)
    {
        //if ((Convert.ToInt16(ddlPickUpHr.Text) >= 20 || Convert.ToInt16(ddlPickUpHr.Text) <= 6) && (Convert.ToInt16(ddlDropOffHr.Text) >= 20 || Convert.ToInt16(ddlDropOffHr.Text) <= 6))
        // {
        lblMesaage.Visible = false;
        if (Convert.ToInt16(ddlScheme.SelectedValue) == Convert.ToInt16(RentalType.Airport))
        {
            if (chkEscort.Checked == true)
            {
                DateTime newDropOffdate = DateTime.Parse(txtPickUpdate.Text.ToString());
                newDropOffdate = newDropOffdate.AddHours(Convert.ToInt16(ddlPickUpHr.SelectedValue.ToString()));
                newDropOffdate = newDropOffdate.AddMinutes(Convert.ToInt16(ddlPickUpMin.SelectedValue.ToString()));
                newDropOffdate = newDropOffdate.AddHours(4);
                txtDropOffdate.Text = newDropOffdate.ToString("yyyy-MM-dd");
                int newDropOffHr = newDropOffdate.Hour;
                int newDropOffMin = newDropOffdate.Minute;
                if (newDropOffHr.ToString().Length == 1)
                {
                    ddlDropOffHr.SelectedValue = "0" + newDropOffHr.ToString();
                }
                else
                {
                    ddlDropOffHr.SelectedValue = newDropOffHr.ToString();
                }
                if (newDropOffMin.ToString().Length == 1)
                {
                    ddlDropOffMin.SelectedValue = "0" + newDropOffMin.ToString();
                }
                else
                {
                    ddlDropOffMin.SelectedValue = newDropOffMin.ToString();
                }
            }
        }
        //}
        //else
        //{
        //    if (chkEscort.Checked == true)
        //    {
        //        lblMesaage.Visible = true;
        //        lblMesaage.Text = "Escort Request only available form 8 PM to 6 AM";
        //        lblMesaage.ForeColor = Color.Red;
        //    }
        //    else
        //    {
        //        lblMesaage.Visible = false;
        //    }

        //}

    }

    public string[] SplitFunction(string ChargingStatus)
    {
        string[] Chargingstat = ChargingStatus.Split('|');

        return Chargingstat;
    }

    protected void bntBooking_Click(object sender, EventArgs e)
    {
        objBookingOL = new OL_Booking();
        objBookingBL = new BL_Booking();
        objErrorLogDetailsOL = new OL_ErrorLogDetails();
        //bntBooking.Text = "Please wait...";
        //bntBooking.Attributes.Add("onclick", "this.disabled=true;");

        try
        {
            objBookingOL.PickUpCityId = Convert.ToInt32(ddlPickUpCity.SelectedValue);
            objBookingOL.FacilitatorId = Convert.ToInt32(ddlfacilitator.SelectedValue);
            objBookingOL.ModleId = Convert.ToInt32(ddlCarModel.SelectedValue);
            objBookingOL.PickupDate = Convert.ToDateTime(txtPickUpdate.Text.ToString());
            objBookingOL.PickupTime = ddlPickUpHr.SelectedValue.ToString() + ddlPickUpMin.SelectedValue.ToString();
            objBookingOL.BookingScheme = Convert.ToInt32(ddlScheme.SelectedValue);
            objBookingOL.PickUpAdd = txtPickUPAddress.Text.ToString();
            objBookingOL.Remarks = txtRemarks.Text.ToString();
            objBookingOL.ClientCoId = Convert.ToInt32(Session["ClientCoId"]);
            objBookingOL.ClientCoIndivID = Convert.ToInt32(Session["ClientCoIndivID"]);
            objBookingOL.DropOffDate = Convert.ToDateTime(txtDropOffdate.Text.ToString());
            objBookingOL.DropoffTime = ddlDropOffHr.SelectedValue.ToString() + ddlDropOffMin.SelectedValue.ToString();

            objBookingOL.PickUpCityName = ddlPickUpCity.SelectedItem.Text.ToString().Split('-')[0];
            objBookingOL.CarModelName = ddlCarModel.SelectedItem.Text.ToString();
            objBookingOL.Implant = Convert.ToInt16(Session["Implant"]);
            objBookingOL.ImplantId = Convert.ToInt32(Session["ImplantId"]);

            if (!string.IsNullOrEmpty(city_location_lat.Value.ToString()))
            {
                objBookingOL.PLat = float.Parse(city_location_lat.Value.ToString());
            }
            else
            {
                objBookingOL.PLat = 0;
            }
            if (!string.IsNullOrEmpty(city_location_long.Value.ToString()))
            {
                objBookingOL.PLon = float.Parse(city_location_long.Value.ToString());
            }
            else
            {
                objBookingOL.PLon = 0;
            }


            //Days Calculation
            DateTime fromdate = Convert.ToDateTime(txtPickUpdate.Text.ToString());
            DateTime todate = Convert.ToDateTime(txtDropOffdate.Text.ToString());

            // Difference in days, hours, and minutes.
            TimeSpan ts = todate - fromdate;
            int noOfNight = ts.Days;
            objBookingOL.NoNight = noOfNight;
            //end of Days calculation

            if (chkEscort.Checked == true)
            {
                objBookingOL.Escort = true;
            }
            else
            {
                objBookingOL.Escort = false;
            }

            if (Convert.ToInt16(ddlScheme.SelectedValue) == Convert.ToInt16(RentalType.HalfDay))
            {
                objBookingOL.ServiceType = SeriveType.ChauffeurDrive;
            }
            else if (Convert.ToInt16(ddlScheme.SelectedValue) == Convert.ToInt16(RentalType.FullDay))
            {
                objBookingOL.ServiceType = SeriveType.ChauffeurDrive;
            }
            else if (Convert.ToInt16(ddlScheme.SelectedValue) == Convert.ToInt16(RentalType.Airport))
            {
                objBookingOL.ServiceType = SeriveType.Airport;
                if (chkEscort.Checked == true)
                {
                    objBookingOL.ServiceType = SeriveType.ChauffeurDrive;
                }
            }
            if (!string.IsNullOrEmpty(txtadditionalEmaiId.Text.ToString()))
            {
                objBookingOL.AdditionalEmaiId = txtadditionalEmaiId.Text.ToString();
            }
            else
            {
                objBookingOL.AdditionalEmaiId = null;
            }

            if (!string.IsNullOrEmpty(txtAdditionalMobileNo.Text.ToString()))
            {
                objBookingOL.AdditionalMobileNo = txtAdditionalMobileNo.Text.ToString();
            }
            else
            {
                objBookingOL.AdditionalMobileNo = null;
            }

            if (objBookingOL.Implant == 1)
            {
                objBookingOL.BookerID = objBookingOL.ClientCoId;
            }
            else
            {
                objBookingOL.BookerID = objBookingOL.ClientCoIndivID;
            }

            objBookingOL = objBookingBL.CheckGuestRegistration(objBookingOL); //check Client Registration

            DateTime currentDate = DateTime.Now.Date;
            DateTime guestPickupDate = Convert.ToDateTime(objBookingOL.PickupDate).Date;


            if (objBookingOL.DbOperationStatus == CommonConstant.SUCCEED && objBookingOL.ObjDataTable.Rows.Count > 0)
            {

                if (Convert.ToBoolean(objBookingOL.ObjDataTable.Rows[0]["ClientRegisteredYN"]) == true && Convert.ToBoolean(objBookingOL.ObjDataTable.Rows[0]["CCRegisteredYN"]) == true)
                {
                    objBookingOL = objBookingBL.GetSoldOut(objBookingOL);  //Sold out condition check

                    if (objBookingOL.ObjDataTable.Rows.Count > 0 && objBookingOL.DbOperationStatus == CommonConstant.SUCCEED && guestPickupDate >= currentDate)
                    {
                        lblMesaage.Visible = true;
                        GeneralUtility.InitializeControls(Form);
                        lblMesaage.Text = "Category : " + objBookingOL.ObjDataTable.Rows[0]["CarCatName"] + " is sold out for period between " + objBookingOL.ObjDataTable.Rows[0]["FromDate"] + " and " + objBookingOL.ObjDataTable.Rows[0]["ToDate"];
                        lblMesaage.ForeColor = Color.Red;
                        return;
                    }
                    else
                    {
                        objBookingOL = objBookingBL.BookingConfirmation(objBookingOL); // Calling for Booking

                        if (objBookingOL.BookingId > 0 && objBookingOL.DbOperationStatus == CommonConstant.SUCCEED)
                        {

                            /*
                            //Remove start 
                             objBookingOL = objBookingBL.GetFacilitatorDetails(objBookingOL); // Facilitator details

                             string mailConent = MailContent.DirectClientEmail(objBookingOL);
                             objBookingOL.EmailContent = mailConent;
                             if (!string.IsNullOrEmpty(mailConent) && chkEmail.Checked == true)
                             {
                                 if (!string.IsNullOrEmpty(objBookingOL.AdditionalEmaiId))
                                 {
                                     objBookingOL.AddCC = objBookingOL.AdditionalEmaiId.ToString();
                                     if (!string.IsNullOrEmpty(objBookingOL.FacilitatorEmailId))
                                     {
                                         objBookingOL.AddBCC = objBookingOL.FacilitatorEmailId.ToString();
                                     }

                                 }
                                 objBookingOL.Subject = "Carzonrent Reservation confirmation e-mail For ";
                                 objBookingOL.MailStatus = Mailhandling.SendMail(objBookingOL);
                             }

                             string smsContent = SMSContent.SendMessageForGuest(objBookingOL);
                             string guestMobile = "91" + objBookingOL.GuestPhoneNo.ToString();
                             if (guestMobile.Length == 12)
                             {
                                 string status = sendSMSPoxy.SendSMSDetails(smsContent.ToString(), guestMobile, objBookingOL.BookingId.ToString());
                                 objBookingOL.SmsStatus = status.Equals("1") ? true : false;
                                 objBookingOL = objBookingBL.UPdateSMSStatus(objBookingOL);
                                 if (!string.IsNullOrEmpty(objBookingOL.AdditionalMobileNo))
                                 {
                                     string additionalMobile = "91" + objBookingOL.AdditionalMobileNo.ToString();
                                     if (additionalMobile.Length == 12)
                                     {
                                         string additionalstatus = sendSMSPoxy.SendSMSDetails(smsContent.ToString(), additionalMobile, objBookingOL.BookingId.ToString());
                                     }
                                 }
                             }
                             if (objBookingOL.FacilitatorId > 0)
                             {
                                 string smsContentFacilitator = SMSContent.SendMessageForFacilitator(objBookingOL);
                                 string FacilitatortMobile = "91" + objBookingOL.FaclitatorPhoneNo.ToString();
                                 if (FacilitatortMobile.Length == 12)
                                 {
                                     string status = sendSMSPoxy.SendSMSDetails(smsContentFacilitator.ToString(), FacilitatortMobile, objBookingOL.BookingId.ToString());
                                 }

                             }

                             lblMesaage.Visible = true;
                             GeneralUtility.InitializeControls(Form);
                             lblMesaage.Text = "Your booking id is " + objBookingOL.BookingId.ToString();
                             lblMesaage.ForeColor = Color.Red;
                             Session["bookingId"] = objBookingOL.BookingId;
                             Response.Redirect("BookingConfirmation.aspx", false);
                            
                             // Remomve End */

                            // start main logic 

                            int CustOrgRegId = objBookingOL.ClientCoIndivID;

                            TestChargingWebService.ServiceSoapClient ta = new TestChargingWebService.ServiceSoapClient();

                            string Preauth = ta.PaymateCorporateGooglePreauth(objBookingOL.BookingId, objBookingOL.ApprovalAmt);

                            string[] SPreturnresult = SplitFunction(Preauth);

                            string ERRORCODE, ERRORMESSAGE, PayMateAuthTransactionID, InvoiceNo, Amount;

                            ERRORCODE = SPreturnresult[0].ToString();
                            ERRORMESSAGE = SPreturnresult[1].ToString();
                            PayMateAuthTransactionID = SPreturnresult[2].ToString();
                            InvoiceNo = SPreturnresult[3].ToString();
                            Amount = SPreturnresult[4].ToString();

                            //objBookingOL.CCNo = resultResponse[3]; //Credit Card No
                            objBookingOL.AuthCode = PayMateAuthTransactionID;
                            objBookingOL.PGId = PayMateAuthTransactionID;

                            //string Mer_UserName = "00011500";
                            //string Mer_Refno = "WNOUD";

                            //StringBuilder urlPost = new StringBuilder("https://secure.paymate.co.in/CCAPI/APIAuthTran.aspx?UserName=" + Mer_UserName);
                            //urlPost.Append("&RefNo=" + Mer_Refno.ToString());
                            //urlPost.Append("&EmployeeName=" + objBookingOL.GuestName.ToString());
                            //urlPost.Append("&AgentName=" + objBookingOL.GuestName.ToString());
                            //urlPost.Append("&AgentCode=" + objBookingOL.ClientCoIndivID);
                            //urlPost.Append("&AgentLocation=" + objBookingOL.ServiceUnitName.ToString());
                            //urlPost.Append("&RANumber=" + objBookingOL.BookingId);
                            //urlPost.Append("&RentalDate=" + objBookingOL.DropOffDate.ToShortDateString());
                            //urlPost.Append("&PickupCity=" + objBookingOL.PickUpCityName.ToString());
                            //urlPost.Append("&ReturnCity=" + objBookingOL.PickUpCityName.ToString());
                            //urlPost.Append("&CustOrgRegId=" + CustOrgRegId.ToString());
                            //urlPost.Append("&Amount=" + objBookingOL.ApprovalAmt);
                            //urlPost.Append("&OrderId=" + objBookingOL.BookingId);


                            //byte[] buffer1 = System.Text.Encoding.GetEncoding(1252).GetBytes(urlPost.ToString());
                            //WebRequest request1 = WebRequest.Create(urlPost.ToString());
                            //request1.Method = "POST";
                            //request1.ContentLength = buffer1.Length;

                            //request1.ContentType = "application/x-www-form-urlencoded";
                            //Stream dataStream1 = request1.GetRequestStream();
                            //dataStream1.Write(buffer1, 0, buffer1.Length);
                            //dataStream1.Close();

                            //HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
                            //Stream stm1 = response1.GetResponseStream();
                            //StreamReader sr1 = new StreamReader(stm1);
                            //string payMateRespose = sr1.ReadToEnd();

                            //string[] resultResponse = payMateRespose.Split('|');

                            //string eRRORCODE = resultResponse[0]; //Error Code '400 if Sucess

                            //objBookingOL.Transactionid = resultResponse[1]; ///Transaction ID


                            ////string transactionId = splitStatus[1]; //Transaction ID
                            //string transactionNo = resultResponse[1]; //Transaction ID
                            //string errorMessage = resultResponse[2]; //Error Message


                            //objErrorLogDetailsOL.errorSource = "BookingSystem.aspx.cs";
                            //objErrorLogDetailsOL.errorBlock = eRRORCODE;
                            //objErrorLogDetailsOL.ErrorMessage = errorMessage;
                            //objErrorLogDetailsOL.errorBookingId = objBookingOL.BookingId;
                            //objErrorLogDetailsOL.errorCreatedBy = objBookingOL.ClientCoIndivID;


                            //if (ERRORCODE == "400")
                            //{
                            //    //objBookingOL.CCNo = resultResponse[3]; //Credit Card No
                            //    objBookingOL.AuthCode = resultResponse[4]; //Auth Code
                            //    objBookingOL.PGId = resultResponse[5];
                            //    if (resultResponse[6].ToString() == "VISA")
                            //    {
                            //        objBookingOL.CCType = 1;
                            //    }
                            //    else if (resultResponse[6].ToString() == "MASTER")
                            //    {
                            //        objBookingOL.CCType = 3;
                            //    }
                            //    else if (resultResponse[6].ToString() == "AMEX")
                            //    {
                            //        objBookingOL.CCType = 2;
                            //    }
                            //}


                            if (ERRORCODE == "000")
                            {
                                objBookingOL = objBookingBL.UpdatePreAuthStatus(objBookingOL);
                                if (objBookingOL.DbOperationStatus == CommonConstant.SUCCEED)
                                {

                                    objBookingOL = objBookingBL.GetFacilitatorDetails(objBookingOL); // Facilitator details

                                    string mailConent = MailContent.DirectClientEmail(objBookingOL);
                                    objBookingOL.EmailContent = mailConent;
                                    if (!string.IsNullOrEmpty(mailConent) && chkEmail.Checked == true)
                                    {
                                        if (!string.IsNullOrEmpty(objBookingOL.AdditionalEmaiId))
                                        {
                                            objBookingOL.AddCC = objBookingOL.AdditionalEmaiId.ToString();
                                        }
                                        if (!string.IsNullOrEmpty(objBookingOL.FacilitatorEmailId))
                                        {
                                            objBookingOL.AddBCC = objBookingOL.FacilitatorEmailId.ToString();
                                        }
                                        objBookingOL.Subject = "Carzonrent Reservation confirmation e-mail For ";
                                        objBookingOL.MailStatus = Mailhandling.SendMail(objBookingOL);
                                    }


                                    string smsContent = SMSContent.SendMessageForGuest(objBookingOL);
                                    string guestMobile = "91" + objBookingOL.GuestPhoneNo.ToString();
                                    if (guestMobile.Length == 12)
                                    {
                                        //string status = sendSMSPoxy.SendSMSDetails(smsContent.ToString(), guestMobile, objBookingOL.BookingId.ToString());
                                        string status = sendSMSPoxy.SendSMSDetails_smsdetails_GetDetails("Guest", guestMobile, objBookingOL.BookingId.ToString(), 0);
                                        objBookingOL.SmsStatus = status.Equals("1") ? true : false;
                                        objBookingOL = objBookingBL.UPdateSMSStatus(objBookingOL);
                                    }

                                    if (!string.IsNullOrEmpty(objBookingOL.AdditionalMobileNo))
                                    {
                                        string additionalMobile = "91" + objBookingOL.AdditionalMobileNo.ToString();
                                        if (additionalMobile.Length == 12)
                                        {
                                            //string additionalstatus = sendSMSPoxy.SendSMSDetails(smsContent.ToString(), additionalMobile, objBookingOL.BookingId.ToString());
                                            string additionalstatus = sendSMSPoxy.SendSMSDetails_smsdetails_GetDetails("Facilitator", additionalMobile, objBookingOL.BookingId.ToString(), 0);
                                        }
                                    }

                                    //if (objBookingOL.FacilitatorId > 0)
                                    //{
                                    //    string smsContentFacilitator = SMSContent.SendMessageForFacilitator(objBookingOL);
                                    //    string FacilitatortMobile = "91" + objBookingOL.FaclitatorPhoneNo.ToString();
                                    //    if (FacilitatortMobile.Length == 12)
                                    //    {
                                    //        string status = sendSMSPoxy.SendSMSDetails(smsContentFacilitator.ToString(), FacilitatortMobile, objBookingOL.BookingId.ToString());
                                    //    }
                                    //}

                                    lblMesaage.Visible = true;
                                    GeneralUtility.InitializeControls(Form);
                                    lblMesaage.Text = "Your booking id is " + objBookingOL.BookingId.ToString();
                                    lblMesaage.ForeColor = Color.Red;
                                    Session["bookingId"] = objBookingOL.BookingId;
                                    Response.Redirect("BookingConfirmation.aspx", false);
                                }
                            }
                            //else if (ERRORCODE == "401")
                            //{
                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Invalid user name or reference number";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);
                            //}
                            //else if (ERRORCODE == "406")
                            //{

                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Empty value/ Invalid Parameters";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);
                            //}
                            //else if (ERRORCODE == "410")
                            //{


                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Invalid Transaction ID";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);
                            //}
                            //else if (ERRORCODE == "500")
                            //{

                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Timeout/Error";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);
                            //}
                            //else if (ERRORCODE == "115")
                            //{

                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Transaction Declined";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);
                            //}
                            //else if (ERRORCODE == "021")
                            //{

                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Invalid login details";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);

                            //}
                            //else if (ERRORCODE == "022")
                            //{

                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Invalid source";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);

                            //}
                            //else if (ERRORCODE == "023")
                            //{

                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Denied by risk";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);

                            //}
                            //else if (ERRORCODE == "035")
                            //{

                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Incorrect Expiry";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);

                            //}
                            //else if (ERRORCODE == "036")
                            //{
                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Incorrect Credit card number";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDet    ailsOL);
                            //}
                            //else if (ERRORCODE == "059")
                            //{
                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Transaction Declined";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);
                            //}
                            //else if (ERRORCODE == "062")
                            //{
                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Invalid details";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);
                            //}
                            //else if (ERRORCODE == "API01")
                            //{
                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "Invalid Request.";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);
                            //}
                            //else if (ERRORCODE == "API02")
                            //{
                            //    lblMesaage.Visible = true;
                            //    GeneralUtility.InitializeControls(Form);
                            //    lblMesaage.Text = "invalid Merchant";
                            //    lblMesaage.ForeColor = Color.Red;
                            //    objBookingBL.SaveLogError(objErrorLogDetailsOL);
                            //}
                            else
                            {

                                lblMesaage.Visible = true;
                                lblMesaage.Text = ERRORMESSAGE;
                                GeneralUtility.InitializeControls(Form);
                                lblMesaage.ForeColor = Color.Red;
                                objBookingBL.SaveLogError(objErrorLogDetailsOL);
                            }
                            /* End mail logic */
                        }
                        else
                        {
                            lblMesaage.Visible = true;
                            lblMesaage.Text = objBookingOL.CommonMessage.ToString();
                            GeneralUtility.InitializeControls(Form);
                            lblMesaage.ForeColor = Color.Red;
                        }
                    }

                }
                else
                {
                    lblMesaage.Visible = true;
                    GeneralUtility.InitializeControls(Form);
                    lblMesaage.Text = "Guest not registered.";
                    lblMesaage.ForeColor = Color.Red;
                }

            }
            else
            {
                lblMesaage.Visible = true;
                GeneralUtility.InitializeControls(Form);
                lblMesaage.Text = "Guest not registered.";
                lblMesaage.ForeColor = Color.Red;
            }

        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            throw new Exception(Ex.Message);
        }
        finally
        {

            // bntBooking.Attributes.Add("onclick", "this.disabled=false;");
            bntBooking.Enabled = true;
            //bntBooking.Text = "Book";
        }


    }

    protected void ddlScheme_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMesaage.Visible = false;
        if (!string.IsNullOrEmpty(txtPickUpdate.Text.ToString()))
        {
            BindDynamicTimeDropOffTime();
        }
        else
        {
            ddlScheme.SelectedIndex = 0;
            lblMesaage.Visible = true;
            lblMesaage.Text = "Select pick-up date";
            lblMesaage.ForeColor = Color.Red;
        }

    }


    protected void ddlPickUpHr_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMesaage.Visible = false;
        if (!string.IsNullOrEmpty(txtDropOffdate.Text.ToString()))
        {
            BindDynamicTimeDropOffTime();
        }
    }

    protected void ddlPickUpMin_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMesaage.Visible = false;
        if (!string.IsNullOrEmpty(txtDropOffdate.Text.ToString()))
        {
            BindDynamicTimeDropOffTime();
        }
    }
    protected void txtPickUpdate_TextChanged(object sender, EventArgs e)
    {
        lblMesaage.Visible = false;
        if (!string.IsNullOrEmpty(txtDropOffdate.Text.ToString()))
        {
            BindDynamicTimeDropOffTime();
        }
    }



    private void BindCity()
    {
        objBookingOL = new OL_Booking();
        objBookingBL = new BL_Booking();
        objBookingOL.ClientCoId = Convert.ToInt32(Session["ClientCoId"]);
        try
        {
            objBookingOL = objBookingBL.GetCityName(objBookingOL);

            if (objBookingOL.ObjDataTable.Rows.Count > 0)
            {

                ddlPickUpCity.DataSource = objBookingOL.ObjDataTable;
                ddlPickUpCity.DataTextField = "CityName";
                ddlPickUpCity.DataValueField = "CityId";
                ddlPickUpCity.DataBind();
                ddlPickUpCity.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlPickUpCity.DataSource = null;
                ddlPickUpCity.DataBind();
            }

        }
        catch (Exception Ex)
        {
            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            throw new Exception(Ex.Message);
        }
    }
    private void BindFacilitator()
    {
        objBookingOL = new OL_Booking();
        objBookingBL = new BL_Booking();
        objBookingOL.ClientCoId = Convert.ToInt32(Session["ClientCoId"]);
        objBookingOL.FacilitatorId = 0;
        objBookingOL.Flag = 1;
        try
        {
            objBookingOL = objBookingBL.GetFacilitatorName(objBookingOL);

            if (objBookingOL.ObjDataTable.Rows.Count > 0)
            {

                ddlfacilitator.DataSource = objBookingOL.ObjDataTable;
                ddlfacilitator.DataTextField = "FacilitatorName";
                ddlfacilitator.DataValueField = "FacilitatorID";
                ddlfacilitator.DataBind();
                ddlfacilitator.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlfacilitator.DataSource = null;
                ddlfacilitator.DataBind();
            }

        }
        catch (Exception Ex)
        {
            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            throw new Exception(Ex.Message);
        }
    }


    #region Helper Function
    private void BindPickUpTimeHours()
    {
        try
        {
            BindHours(ddlPickUpHr);
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            throw new Exception(Ex.Message);
        }

    }

    private void BindPickUPTimeMinute()
    {
        try
        {
            BindMinute(ddlPickUpMin);
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            throw new Exception(Ex.Message);
        }
    }

    private void BindDropOffHours()
    {
        try
        {
            BindHours(ddlDropOffHr);
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            throw new Exception(Ex.Message);
        }
    }

    private void BindDropOffMintue()
    {
        try
        {
            BindMinute(ddlDropOffMin);
        }
        catch (Exception Ex)
        {
            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            throw new Exception(Ex.Message);
        }
    }

    private void BindHours(DropDownList ddlHoursDrowpDown)
    {

        try
        {
            for (int i = 0; i <= 23; i++)
            {
                string timeHours = string.Empty;
                if (i <= 9)
                {
                    timeHours = "0" + i.ToString();
                }
                else
                {
                    timeHours = i.ToString();
                }
                ddlHoursDrowpDown.Items.Add(new ListItem(timeHours.ToString(), timeHours.ToString()));
            }
        }
        catch (Exception Ex)
        {

            throw new Exception(Ex.Message);

        }

    }
    private void BindMinute(DropDownList ddlMintueDropDown)
    {
        try
        {

            for (int i = 0; i <= 45; i = i + 15)
            {
                string timeMinute = string.Empty;
                if (i <= 9)
                {
                    timeMinute = "0" + i.ToString();
                }
                else
                {
                    timeMinute = i.ToString();
                }
                ddlMintueDropDown.Items.Add(new ListItem(timeMinute.ToString(), timeMinute.ToString()));
            }
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error accured.");
            throw new Exception(Ex.Message);
        }
    }
    private void BindDynamicTimeDropOffTime()
    {
        DateTime newDropOffdate = DateTime.Parse(txtPickUpdate.Text.ToString());
        newDropOffdate = newDropOffdate.AddHours(Convert.ToInt16(ddlPickUpHr.SelectedValue.ToString()));
        newDropOffdate = newDropOffdate.AddMinutes(Convert.ToInt16(ddlPickUpMin.SelectedValue.ToString()));

        if (Convert.ToInt16(ddlScheme.SelectedValue.ToString()) == Convert.ToInt16(RentalType.HalfDay))
        {
            newDropOffdate = newDropOffdate.AddHours(4);
        }
        else if (Convert.ToInt16(ddlScheme.SelectedValue.ToString()) == Convert.ToInt16(RentalType.FullDay))
        {
            newDropOffdate = newDropOffdate.AddHours(8);
        }
        else if (Convert.ToInt16(ddlScheme.SelectedValue.ToString()) == Convert.ToInt16(RentalType.Airport))
        {
            newDropOffdate = newDropOffdate.AddHours(2);
            if (chkEscort.Checked == true)
            {
                newDropOffdate = newDropOffdate.AddHours(4);
            }
        }

        int newDropOffHr = newDropOffdate.Hour;
        int newDropOffMin = newDropOffdate.Minute;
        // txtDropOffdate.Text = Convert.ToDateTime(txtPickUpdate.Text.ToString()).AddHours(4).ToString ("yyyy-MM-dd");
        txtDropOffdate.Text = newDropOffdate.ToString("yyyy-MM-dd");

        if (newDropOffHr.ToString().Length == 1)
        {
            ddlDropOffHr.SelectedValue = "0" + newDropOffHr.ToString();
        }
        else
        {
            ddlDropOffHr.SelectedValue = newDropOffHr.ToString();
        }
        if (newDropOffMin.ToString().Length == 1)
        {
            ddlDropOffMin.SelectedValue = "0" + newDropOffMin.ToString();
        }
        else
        {
            ddlDropOffMin.SelectedValue = newDropOffMin.ToString();
        }
    }
    #endregion

}