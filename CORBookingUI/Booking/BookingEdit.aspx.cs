﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.BusinessFramework;
using System.Drawing;
using System.Text;
using System.Net;
using System.IO;

public partial class Booking_BookingEdit : System.Web.UI.Page
{
    private OL_Booking objBookingOL = null;
    private BL_EditBooking objEditBookingBL = null;
    private BL_Booking objBookingBL = null;
    COR.BusinessFramework.SendSMS.SendSMSSoapClient sendSMSPoxy = new COR.BusinessFramework.SendSMS.SendSMSSoapClient();

    protected void Page_Load(object sender, EventArgs e)
    {
        objBookingOL = new OL_Booking();

        if (!Page.IsPostBack)
        {
            BindCity();
            BindPickUpTimeHours();          // For Selection of Pickup Hours
            BindPickUPTimeMinute();         // For Selection of Pickup Minute
            BindDropOffHours();             // For Selection of Drop off Hours
            BindDropOffMintue();            // For Selection of Drop off Minute   
            //if (Request.QueryString.HasKeys())
            //{
                if (!string.IsNullOrEmpty(Convert.ToString(Session["bookingId"])))
                {
                    objBookingOL.BookingId = Convert.ToInt32(Session["bookingId"]);
                    FillEditBookingDetails(objBookingOL);

                }
            //}
        }
    }

    private void FillEditBookingDetails(OL_Booking objBookingOL)
    {
        objEditBookingBL = new BL_EditBooking();
        try
        {
            objBookingOL = objEditBookingBL.GetEditBookingDetails(objBookingOL);
            if (objBookingOL.ObjDataTable.Rows.Count > 0)
            {
                objBookingOL.ModleId = Convert.ToInt32(objBookingOL.ObjDataTable.Rows[0]["ModelID"].ToString());
                objBookingOL.PickUpCityId = Convert.ToInt32(objBookingOL.ObjDataTable.Rows[0]["PickUpCityID"].ToString());
                ddlPickUpCity.SelectedValue = objBookingOL.ObjDataTable.Rows[0]["PickUpCityID"].ToString();
                ddlCarModel.SelectedValue = objBookingOL.ObjDataTable.Rows[0]["ModelID"].ToString();
                //txtPickUpdate.Text = //objBookingOL.ObjDataTable.Rows[0]["PickUpDate"].ToString();
                txtPickUpdate.Text = DateTime.Parse(objBookingOL.ObjDataTable.Rows[0]["PickUpDate"].ToString()).ToString("yyyy-MM-dd");
                ddlPickUpHr.SelectedValue = objBookingOL.ObjDataTable.Rows[0]["PickUpHr"].ToString();
                ddlPickUpMin.SelectedValue = objBookingOL.ObjDataTable.Rows[0]["PickupMi"].ToString();
                //txtDropOffdate.Text = objBookingOL.ObjDataTable.Rows[0]["DropOffDate"].ToString();
                txtDropOffdate.Text = DateTime.Parse(objBookingOL.ObjDataTable.Rows[0]["DropOffDate"].ToString()).ToString("yyyy-MM-dd");
                ddlDropOffHr.SelectedValue = objBookingOL.ObjDataTable.Rows[0]["DropOffHr"].ToString();
                ddlDropOffMin.SelectedValue = objBookingOL.ObjDataTable.Rows[0]["DropOffMin"].ToString();
                txtPickUPAddress.Text = objBookingOL.ObjDataTable.Rows[0]["PickUpAdd"].ToString();
                txtRemarks.Text = objBookingOL.ObjDataTable.Rows[0]["Remarks"].ToString();
                ddlScheme.SelectedValue = objBookingOL.ObjDataTable.Rows[0]["ServiceType"].ToString();
                txtadditionalEmaiId.Text = objBookingOL.ObjDataTable.Rows[0]["Miscellaneous1"].ToString();
                txtAdditionalMobileNo.Text = objBookingOL.ObjDataTable.Rows[0]["Miscellaneous2"].ToString();
                if (Convert.ToBoolean(objBookingOL.ObjDataTable.Rows[0]["EscortRequestYN"]) == true)
                {
                    chkEscort.Checked = true;
                }
                else
                {
                    chkEscort.Checked = false;
                }
                objBookingOL = BindCarModel(objBookingOL);
            }

        }
        catch (Exception Ex)
        {
            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            throw new Exception(Ex.Message);
        }
    }

    private OL_Booking BindCarModel(OL_Booking objBookingOL)
    {
        try
        {
            objBookingOL.ClientCoId = Convert.ToInt32(Session["ClientCoId"]);
            objBookingOL = objBookingBL.GetCarModel(objBookingOL);
            if (objBookingOL.ObjDataTable.Rows.Count > 0)
            {
                ddlCarModel.DataSource = objBookingOL.ObjDataTable;
                ddlCarModel.DataTextField = "CarModel";
                ddlCarModel.DataValueField = "CarModelID";
                ddlCarModel.DataBind();
                ddlCarModel.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlCarModel.DataSource = null;
                ddlCarModel.DataBind();

            }
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            throw new Exception(Ex.Message);
        }
        return objBookingOL;
    }

    protected void ddlPickUpCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        objBookingOL = new OL_Booking();
        objBookingBL = new BL_Booking();

        try
        {
            if (ddlPickUpCity.SelectedIndex > 0)
            {
                objBookingOL.PickUpCityId = Convert.ToInt32(ddlPickUpCity.SelectedValue);
                objBookingOL.ClientCoId = Convert.ToInt32(Session["ClientCoId"]);
                objBookingOL = objBookingBL.GetCarModel(objBookingOL);
                if (objBookingOL.ObjDataTable.Rows.Count > 0)
                {
                    ddlCarModel.DataSource = objBookingOL.ObjDataTable;
                    ddlCarModel.DataTextField = "CarModel";
                    ddlCarModel.DataValueField = "CarModelID";
                    ddlCarModel.DataBind();
                    ddlCarModel.Items.Insert(0, new ListItem("--Select--", "0"));
                }
                else
                {
                    ddlCarModel.DataSource = null;
                    ddlCarModel.DataBind();

                }

            }
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            throw new Exception(Ex.Message);
        }
    }

    protected void chkEscort_CheckedChanged(object sender, EventArgs e)
    {
        //if ((Convert.ToInt16(ddlPickUpHr.Text) >= 20 || Convert.ToInt16(ddlPickUpHr.Text) <= 6) && (Convert.ToInt16(ddlDropOffHr.Text) >= 20 || Convert.ToInt16(ddlDropOffHr.Text) <= 6))
        //{
            lblMesaage.Visible = false;

            if (Convert.ToInt16(ddlScheme.SelectedValue) == Convert.ToInt16(RentalType.Airport))
            {
                if (chkEscort.Checked == true)
                {
                    DateTime newDropOffdate = DateTime.Parse(txtPickUpdate.Text.ToString());
                    newDropOffdate = newDropOffdate.AddHours(Convert.ToInt16(ddlPickUpHr.SelectedValue.ToString()));
                    newDropOffdate = newDropOffdate.AddMinutes(Convert.ToInt16(ddlPickUpMin.SelectedValue.ToString()));
                    newDropOffdate = newDropOffdate.AddHours(4);
                    txtDropOffdate.Text = newDropOffdate.ToString("yyyy-MM-dd");
                    int newDropOffHr = newDropOffdate.Hour;
                    int newDropOffMin = newDropOffdate.Minute;
                    if (newDropOffHr.ToString().Length == 1)
                    {
                        ddlDropOffHr.SelectedValue = "0" + newDropOffHr.ToString();
                    }
                    else
                    {
                        ddlDropOffHr.SelectedValue = newDropOffHr.ToString();
                    }
                    if (newDropOffMin.ToString().Length == 1)
                    {
                        ddlDropOffMin.SelectedValue = "0" + newDropOffMin.ToString();
                    }
                    else
                    {
                        ddlDropOffMin.SelectedValue = newDropOffMin.ToString();
                    }
                }
            }
        //}
        //else
        //{
        //    if (chkEscort.Checked == true)
        //    {
        //        lblMesaage.Visible = true;
        //        lblMesaage.Text = "Escort Request only available form 8 PM to 6 AM";
        //        lblMesaage.ForeColor = Color.Red;
        //    }
        //    else
        //    {
        //        lblMesaage.Visible = false;
        //    }
        //}

    }


    protected void bntEditBooking_Click(object sender, EventArgs e)
    {
        objBookingOL = new OL_Booking();
        objBookingBL = new BL_Booking();
        objEditBookingBL = new BL_EditBooking();
       

        try
        {
            objBookingOL.BookingId = Convert.ToInt32(Session["bookingId"]);
            objBookingOL.PickUpCityId = Convert.ToInt32(ddlPickUpCity.SelectedValue);
            objBookingOL.ModleId = Convert.ToInt32(ddlCarModel.SelectedValue);
            objBookingOL.PickupDate = Convert.ToDateTime(txtPickUpdate.Text.ToString());
            objBookingOL.PickupTime = ddlPickUpHr.SelectedValue.ToString() + ddlPickUpMin.SelectedValue.ToString();
            objBookingOL.BookingScheme = Convert.ToInt32(ddlScheme.SelectedValue);
            objBookingOL.PickUpAdd = txtPickUPAddress.Text.ToString();
            objBookingOL.Remarks = txtRemarks.Text.ToString();
            objBookingOL.ClientCoId = Convert.ToInt32(Session["ClientCoId"]);
            objBookingOL.ClientCoIndivID = Convert.ToInt32(Session["ClientCoIndivID"]);
            objBookingOL.DropOffDate = Convert.ToDateTime(txtDropOffdate.Text.ToString());
            objBookingOL.DropoffTime = ddlDropOffHr.SelectedValue.ToString() + ddlDropOffMin.SelectedValue.ToString();
            objBookingOL.PickUpCityName = ddlPickUpCity.SelectedItem.Text.ToString().Split('-')[0];
            objBookingOL.CarModelName = ddlCarModel.SelectedItem.Text.ToString();
            objBookingOL.Implant = Convert.ToInt16(Session["Implant"]);

            //Days Calculation
            DateTime fromdate = Convert.ToDateTime(txtPickUpdate.Text.ToString()); ;
            DateTime todate = Convert.ToDateTime(txtDropOffdate.Text.ToString()); ;

            // Difference in days, hours, and minutes.
            TimeSpan ts = todate - fromdate;
            int noOfNight = ts.Days;
            objBookingOL.NoNight = noOfNight;
            //end of Days calculation

            if (chkEscort.Checked == true)
            {
                objBookingOL.Escort = true;
            }
            else
            {
                objBookingOL.Escort = false;
            }

            if (Convert.ToInt16(ddlScheme.SelectedValue) == Convert.ToInt16(RentalType.HalfDay))
            {
                objBookingOL.ServiceType = SeriveType.ChauffeurDrive;
            }
            else if (Convert.ToInt16(ddlScheme.SelectedValue) == Convert.ToInt16(RentalType.FullDay))
            {
                objBookingOL.ServiceType = SeriveType.ChauffeurDrive;
            }
            else if (Convert.ToInt16(ddlScheme.SelectedValue) == Convert.ToInt16(RentalType.Airport))
            {
                objBookingOL.ServiceType = SeriveType.Airport;
                if (chkEscort.Checked == true)
                {
                    objBookingOL.ServiceType = SeriveType.ChauffeurDrive;
                }
            }

            if (!string.IsNullOrEmpty(txtadditionalEmaiId.Text.ToString()))
            {
                objBookingOL.AdditionalEmaiId = txtadditionalEmaiId.Text.ToString();
            }
            else
            {
                objBookingOL.AdditionalEmaiId = null;
            }

            if (!string.IsNullOrEmpty(txtAdditionalMobileNo.Text.ToString()))
            {
                objBookingOL.AdditionalMobileNo = txtAdditionalMobileNo.Text.ToString();
            }
            else
            {
                objBookingOL.AdditionalMobileNo = null;
            }

            if (objBookingOL.Implant == 1)
            {
                objBookingOL.BookerID = objBookingOL.ClientCoId;
            }
            else
            {
                objBookingOL.BookerID = objBookingOL.ClientCoIndivID;
            }


            objBookingOL = objBookingBL.CheckGuestRegistration(objBookingOL);

            if (objBookingOL.DbOperationStatus == CommonConstant.SUCCEED && objBookingOL.ObjDataTable.Rows.Count > 0)
            {

                if (Convert.ToBoolean(objBookingOL.ObjDataTable.Rows[0]["ClientRegisteredYN"]) == true && Convert.ToBoolean(objBookingOL.ObjDataTable.Rows[0]["CCRegisteredYN"]) == true)
                {
                    objBookingOL = objBookingBL.GetSoldOut(objBookingOL);  //Sold out condition check

                    if (objBookingOL.ObjDataTable.Rows.Count > 0 && objBookingOL.DbOperationStatus == CommonConstant.SUCCEED)
                    {
                        lblMesaage.Visible = true;
                        GeneralUtility.InitializeControls(Form);
                        lblMesaage.Text = "Category : " + objBookingOL.ObjDataTable.Rows[0]["CarCatName"] + " is sold out for period between " + objBookingOL.ObjDataTable.Rows[0]["FromDate"] + " and " + objBookingOL.ObjDataTable.Rows[0]["ToDate"];
                        lblMesaage.ForeColor = Color.Red;
                        return;
                    }
                    else
                    {
                        objBookingOL = objEditBookingBL.EditBookingConfirmation(objBookingOL);

                        if (objBookingOL.ModifiedStatus > 0 && objBookingOL.DbOperationStatus == CommonConstant.SUCCEED)
                        {

                            string mailConent = MailContent.EditBookingMail(objBookingOL);
                            objBookingOL.EmailContent = mailConent;
                            
                            if (!string.IsNullOrEmpty(mailConent) && chkEmail.Checked == true)
                            {
                                if (!string.IsNullOrEmpty(objBookingOL.AdditionalEmaiId))
                                {
                                    objBookingOL.AddCC = objBookingOL.AdditionalEmaiId.ToString();
                                }
                                objBookingOL.Subject = "Carzonrent Reservation modification e-mail For ";
                                objBookingOL.MailStatus = Mailhandling.SendMail(objBookingOL);
                            }

                            string smsContent = SMSContent.SendMessageForModification(objBookingOL);
                            string guestMobile = "91" + objBookingOL.GuestPhoneNo.ToString();
                           
                            if (guestMobile.Length == 12)
                            {
                                //string status = sendSMSPoxy.SendSMSDetails(smsContent.ToString(), guestMobile, objBookingOL.BookingId.ToString());
                                string status = sendSMSPoxy.SendSMSDetails_smsdetails_GetDetails("EditBooking", guestMobile, objBookingOL.BookingId.ToString(), 0);
                                objBookingOL.SmsStatus = status.Equals("1") ? true : false;
                            }

                            if (!string.IsNullOrEmpty(objBookingOL.AdditionalMobileNo))
                            {
                                string additionalMobile = "91" + objBookingOL.AdditionalMobileNo.ToString();
                                if (additionalMobile.Length == 12)
                                {
                                    //string additionalstatus = sendSMSPoxy.SendSMSDetails(smsContent.ToString(), additionalMobile, objBookingOL.BookingId.ToString());
                                    string additionalstatus = sendSMSPoxy.SendSMSDetails_smsdetails_GetDetails("EditBooking", additionalMobile, objBookingOL.BookingId.ToString(), 0);
                                }
                            }
                            
                            lblMesaage.Visible = true;
                            lblMesaage.Text = "You have successfully updated the booking.";
                            GeneralUtility.InitializeControls(Form);
                            lblMesaage.ForeColor = Color.Red;
                            Response.Redirect("BookingConfirmation.aspx",false);

                        }
                        else
                        {
                            lblMesaage.Visible = true;
                            lblMesaage.Text = objBookingOL.CommonMessage.ToString();
                            GeneralUtility.InitializeControls(Form);
                            lblMesaage.ForeColor = Color.Red;
                        }
                    }
                }
                else
                {
                    lblMesaage.Visible = true;
                    GeneralUtility.InitializeControls(Form);
                    lblMesaage.Text = "Guest not registred.";
                    lblMesaage.ForeColor = Color.Red;
                }

            }



        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            throw new Exception(Ex.Message);
        }
        finally
        {
            bntEditBooking.Enabled = true;

        }


    }

    protected void ddlScheme_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtPickUpdate.Text.ToString()))
        {
            bindDynamicTimeDropOffTime();
        }

    }


    protected void ddlPickUpHr_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtDropOffdate.Text.ToString()))
        {
            bindDynamicTimeDropOffTime();
        }
    }

    protected void ddlPickUpMin_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtDropOffdate.Text.ToString()))
        {
            bindDynamicTimeDropOffTime();
        }
    }
    protected void txtPickUpdate_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtDropOffdate.Text.ToString()))
        {
            bindDynamicTimeDropOffTime();
        }
    }





    #region Helper Function
    private void BindCity()
    {
        objBookingOL = new OL_Booking();
        objBookingBL = new BL_Booking();
        objBookingOL.ClientCoId = Convert.ToInt32(Session["ClientCoId"]);
        try
        {
            objBookingOL = objBookingBL.GetCityName(objBookingOL);

            if (objBookingOL.ObjDataTable.Rows.Count > 0)
            {

                ddlPickUpCity.DataSource = objBookingOL.ObjDataTable;
                ddlPickUpCity.DataTextField = "CityName";
                ddlPickUpCity.DataValueField = "CityId";
                ddlPickUpCity.DataBind();
                ddlPickUpCity.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlPickUpCity.DataSource = null;
                ddlPickUpCity.DataBind();
            }

        }
        catch (Exception Ex)
        {
            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            throw new Exception(Ex.Message);
        }
    }

    private void BindPickUpTimeHours()
    {
        try
        {
            BindHours(ddlPickUpHr);
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            throw new Exception(Ex.Message);
        }

    }

    private void BindPickUPTimeMinute()
    {
        try
        {
            BindMinute(ddlPickUpMin);
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            throw new Exception(Ex.Message);
        }
    }

    private void BindDropOffHours()
    {
        try
        {
            BindHours(ddlDropOffHr);
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            throw new Exception(Ex.Message);
        }
    }

    private void BindDropOffMintue()
    {
        try
        {
            BindMinute(ddlDropOffMin);
        }
        catch (Exception Ex)
        {
            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured.");
            throw new Exception(Ex.Message);
        }
    }

    private void BindHours(DropDownList ddlHoursDrowpDown)
    {

        try
        {
            for (int i = 0; i <= 23; i++)
            {
                string timeHours = string.Empty;
                if (i <= 9)
                {
                    timeHours = "0" + i.ToString();
                }
                else
                {
                    timeHours = i.ToString();
                }
                ddlHoursDrowpDown.Items.Add(new ListItem(timeHours.ToString(), timeHours.ToString()));
            }
        }
        catch (Exception Ex)
        {

            throw new Exception(Ex.Message);

        }

    }
    private void BindMinute(DropDownList ddlMintueDropDown)
    {
        try
        {

            for (int i = 0; i <= 45; i = i + 15)
            {
                string timeMinute = string.Empty;
                if (i <= 9)
                {
                    timeMinute = "0" + i.ToString();
                }
                else
                {
                    timeMinute = i.ToString();
                }
                ddlMintueDropDown.Items.Add(new ListItem(timeMinute.ToString(), timeMinute.ToString()));
            }
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error accured.");
            throw new Exception(Ex.Message);
        }
    }
    private void bindDynamicTimeDropOffTime()
    {
        DateTime newDropOffdate = DateTime.Parse(txtPickUpdate.Text.ToString());
        newDropOffdate = newDropOffdate.AddHours(Convert.ToInt16(ddlPickUpHr.SelectedValue.ToString()));
        newDropOffdate = newDropOffdate.AddMinutes(Convert.ToInt16(ddlPickUpMin.SelectedValue.ToString()));

        if (Convert.ToInt16(ddlScheme.SelectedValue.ToString()) == Convert.ToInt16(RentalType.HalfDay))
        {
            newDropOffdate = newDropOffdate.AddHours(4);
        }
        else if (Convert.ToInt16(ddlScheme.SelectedValue.ToString()) == Convert.ToInt16(RentalType.FullDay))
        {
            newDropOffdate = newDropOffdate.AddHours(8);
        }
        else if (Convert.ToInt16(ddlScheme.SelectedValue.ToString()) == Convert.ToInt16(RentalType.Airport))
        {
            newDropOffdate = newDropOffdate.AddHours(2);
            if (chkEscort.Checked == true)
            {
                newDropOffdate = newDropOffdate.AddHours(4);
            }
        }

        int newDropOffHr = newDropOffdate.Hour;
        int newDropOffMin = newDropOffdate.Minute;

        txtDropOffdate.Text = newDropOffdate.ToString("yyyy-MM-dd");

        if (newDropOffHr.ToString().Length == 1)
        {
            ddlDropOffHr.SelectedValue = "0" + newDropOffHr.ToString();
        }
        else
        {
            ddlDropOffHr.SelectedValue = newDropOffHr.ToString();
        }
        if (newDropOffMin.ToString().Length == 1)
        {
            ddlDropOffMin.SelectedValue = "0" + newDropOffMin.ToString();
        }
        else
        {
            ddlDropOffMin.SelectedValue = newDropOffMin.ToString();
        }
    }
    #endregion



}