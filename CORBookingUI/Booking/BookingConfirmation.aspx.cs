﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.ObjectFramework;
using COR.SolutionFramework;
using COR.BusinessFramework;

public partial class Booking_BookingConfirmation : System.Web.UI.Page
{
    private OL_Booking objBookingOL = null;
    private BL_Booking objBookingBL = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        objBookingOL = new OL_Booking();
        objBookingBL = new BL_Booking();

        if (!Page.IsPostBack)
        {
            //if (Request.QueryString.HasKeys())
            //{
                //if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["BookingId"].ToString())))
            if (!string.IsNullOrEmpty(Convert.ToString(Session["bookingId"])))
                {
                    objBookingOL.BookingId = Convert.ToInt32(Session["bookingId"]);
                    objBookingOL = objBookingBL.GetBookingConfirmationDetails(objBookingOL);
                    if (objBookingOL.DbOperationStatus == CommonConstant.SUCCEED)
                    {
                        if (objBookingOL.ObjDataTable.Rows.Count > 0)
                        {
                            lblBookingId.Text = objBookingOL.BookingId.ToString();
                            lblPickupCity.Text = objBookingOL.ObjDataTable.Rows[0]["CityName"].ToString();
                            lblPickupdate.Text = objBookingOL.ObjDataTable.Rows[0]["PickUpDate"].ToString();
                            lblPickupTime.Text = objBookingOL.ObjDataTable.Rows[0]["PickupTime"].ToString();
                            lblDropOfDate.Text = objBookingOL.ObjDataTable.Rows[0]["DropOffDate"].ToString();
                            lblDropOffTime.Text = objBookingOL.ObjDataTable.Rows[0]["DropOffTime"].ToString();
                            lblAuthAmount.Text = objBookingOL.ObjDataTable.Rows[0]["ApprovalAmt"].ToString();
                            lblTransactionId.Text = objBookingOL.ObjDataTable.Rows[0]["transactionid"].ToString();
                            lblModel.Text = objBookingOL.ObjDataTable.Rows[0]["CarModelName"].ToString();
                        }
                    }

                }
           // }

        }

    }
}