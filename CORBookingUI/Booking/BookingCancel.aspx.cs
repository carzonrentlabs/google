﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.BusinessFramework;
using COR.ObjectFramework;
using COR.SolutionFramework;
using System.Drawing;

public partial class Booking_BookingCancel : System.Web.UI.Page
{
    private OL_Booking objBookingOL;
    private BL_EditBooking objEditBookingBL;
    COR.BusinessFramework.SendSMS.SendSMSSoapClient sendSMSPoxy = new COR.BusinessFramework.SendSMS.SendSMSSoapClient();

    protected void Page_Load(object sender, EventArgs e)
    {
        objBookingOL = new OL_Booking();
        objEditBookingBL = new BL_EditBooking();
        lblMesaage.Visible = false;

        if (!Page.IsPostBack)
        {
            objBookingOL.BookingId = Convert.ToInt32(Session["bookingId"]);
            objBookingOL.ClientCoIndivID = Convert.ToInt32(Session["ClientCoIndivID"]);
            objBookingOL = objEditBookingBL.EditBookingSearch(objBookingOL);
            if (objBookingOL.ObjDataTable.Rows.Count > 0)
            {

                lblBookingId.Text = objBookingOL.BookingId.ToString();
                lblGustName.Text = objBookingOL.ObjDataTable.Rows[0]["GuestName"].ToString();
                lblClientName.Text = objBookingOL.ObjDataTable.Rows[0]["ClientCoName"].ToString();
                lblPickupcityName.Text = objBookingOL.ObjDataTable.Rows[0]["CityName"].ToString();
                lblPickUpdate.Text = objBookingOL.ObjDataTable.Rows[0]["PickUpDate"].ToString();
                lblpickupTime.Text = objBookingOL.ObjDataTable.Rows[0]["PickUpTime"].ToString();
                lblDropOffdate.Text = objBookingOL.ObjDataTable.Rows[0]["DropOffDate"].ToString();
                lblDropoffTime.Text = objBookingOL.ObjDataTable.Rows[0]["DropOffTime"].ToString();
                lblBookedModelName.Text = objBookingOL.ObjDataTable.Rows[0]["CarModelName"].ToString();
                lblBookingStatus.Text = objBookingOL.ObjDataTable.Rows[0]["BookingStatus"].ToString();
                hdGuestEmailId.Value = objBookingOL.ObjDataTable.Rows[0]["GuestEmailId"].ToString();
                hdGuestMobileNo.Value = objBookingOL.ObjDataTable.Rows[0]["GuestMobileNo"].ToString();
                hdAdditionalEmailId.Value = objBookingOL.ObjDataTable.Rows[0]["AdditionalEmailId"].ToString();
                hdAddintionMobileNo.Value = objBookingOL.ObjDataTable.Rows[0]["AdditionalMobileNo"].ToString();
            }
            else
            {

                lblMesaage.Visible = true;
                lblMesaage.Text = "No booking found";
                lblMesaage.ForeColor = Color.Red;
            }
        }
    }
    protected void bntCancel_Click(object sender, EventArgs e)
    {
        objBookingOL = new OL_Booking();
        objEditBookingBL = new BL_EditBooking();

        if (string.IsNullOrEmpty(txtCancelReason.Text))
        {
            lblMesaage.Visible = true;
            lblMesaage.Text = "Cancel should not be blank.";
            lblMesaage.ForeColor = Color.Red;
        }
        else
        {
            objBookingOL.BookingId = Convert.ToInt32(Session["bookingId"]);
            objBookingOL.ClientCoIndivID = Convert.ToInt32(Session["ClientCoIndivID"]);
            objBookingOL.CancelReason = txtCancelReason.Text.ToString();
            objBookingOL.GuestName = lblGustName.Text.ToString();
            objBookingOL.GuestEmailId = hdGuestEmailId.Value.ToString();
            objBookingOL.GuestPhoneNo = hdGuestMobileNo.Value.ToString();
            objBookingOL.AdditionalEmaiId = hdAdditionalEmailId.Value.ToString();
            objBookingOL.AdditionalMobileNo = hdAddintionMobileNo.Value.ToString();
            objBookingOL.Implant = Convert.ToInt16(Session["Implant"]);
            if (objBookingOL.Implant == 1)
            {
                objBookingOL.BookerID = Convert.ToInt32(Session["ClientCoId"]); 
            }
            else
            {
                objBookingOL.BookerID = Convert.ToInt32(Session["ClientCoIndivID"]);
            }

            objBookingOL = objEditBookingBL.CancelBooking(objBookingOL);

            if (objBookingOL.DbOperationStatus == CommonConstant.SUCCEED && objBookingOL.ModifiedStatus > 0)
            {
                string mailConent = MailContent.CancelBookingMail(objBookingOL);
                objBookingOL.EmailContent = mailConent;

                if (!string.IsNullOrEmpty(mailConent))
                {
                    if (!string.IsNullOrEmpty(objBookingOL.AdditionalEmaiId))
                    {
                        objBookingOL.AddCC = objBookingOL.AdditionalEmaiId.ToString();
                    }
                    objBookingOL.Subject = "Carzonrent Reservation cancellation e-mail For ";
                    objBookingOL.MailStatus = Mailhandling.SendMail(objBookingOL);
                }


                string smsContent = SMSContent.SendMessageForCancel(objBookingOL);
                string guestMobile = "91" + objBookingOL.GuestPhoneNo.ToString();
                if (guestMobile.Length == 12)
                {
                    //string status = sendSMSPoxy.SendSMSDetails(smsContent.ToString(), guestMobile, objBookingOL.BookingId.ToString());
                    string status = sendSMSPoxy.SendSMSDetails_smsdetails_GetDetails("CancelGuest", guestMobile, objBookingOL.BookingId.ToString(), 0);
                    objBookingOL.SmsStatus = status.Equals("1") ? true : false;
                    
                }
                if (!string.IsNullOrEmpty(objBookingOL.AdditionalMobileNo))
                {
                    string additionalMobile = "91" + objBookingOL.AdditionalMobileNo.ToString();
                    if (additionalMobile.Length == 12)
                    {
                        //string additionalstatus = sendSMSPoxy.SendSMSDetails(smsContent.ToString(), additionalMobile, objBookingOL.BookingId.ToString());
                        string additionalstatus = sendSMSPoxy.SendSMSDetails_smsdetails_GetDetails("CancelGuest", additionalMobile, objBookingOL.BookingId.ToString(), 0);
                    }
                }

                lblMesaage.Visible = true;
                lblMesaage.Text = "You have successfully cancelled the booking.";
                lblBookingStatus.Text = "Cancel";
                lblMesaage.ForeColor = Color.Red;
            }
            else
            {
                lblMesaage.Visible = true;
                lblMesaage.Text = "Booking already cancelled.";
                lblBookingStatus.Text = "Cancel";
                lblMesaage.ForeColor = Color.Red;
            }

        }
    }
}