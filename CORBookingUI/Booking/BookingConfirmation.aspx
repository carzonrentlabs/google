﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookingConfirmation.aspx.cs"
    Inherits="Booking_BookingConfirmation" MasterPageFile="~/MasterPage/CORMaster.master" %>

<asp:Content ID="ContentPage" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        function PrintDiv() {
            var contents = document.getElementById("dvContents").innerHTML;
            var frame1 = document.createElement('iframe');
            frame1.name = "frame1";
            frame1.style.position = "absolute";
            frame1.style.top = "-1000000px";
            document.body.appendChild(frame1);
            var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
            frameDoc.document.open();
            frameDoc.document.write('<html><head><title></title>');
            frameDoc.document.write('</head><body><center>');
            frameDoc.document.write(contents);
            frameDoc.document.write('</center></body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                document.body.removeChild(frame1);
            }, 500);
            return false;
        }
    </script>
    <br />
    <div id="dvContents" style="text-align: center;">
        <table cellpadding="1" cellspacing="1" border="1">
            <tr>
                <td class="left">
                    <b>Booking Id</b>
                </td>
                <td class="left">
                    <asp:Label ID="lblBookingId" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="left">
                    <b>Pick-up/source city</b>
                </td>
                <td class="left">
                    <asp:Label ID="lblPickupCity" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="left">
                    <b>Pick-up date</b>
                </td>
                <td class="left">
                    <asp:Label ID="lblPickupdate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="left">
                    <b>Pick-up time</b>
                </td>
                <td class="left">
                    <asp:Label ID="lblPickupTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="left">
                    <b>Drop-off date</b>
                </td>
                <td class="left">
                    <asp:Label ID="lblDropOfDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="left">
                    <b>Drop-off time</b>
                </td>
                <td class="left">
                    <asp:Label ID="lblDropOffTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="left">
                    <b>Model Name</b>
                </td>
                <td class="left">
                    <asp:Label ID="lblModel" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="left">
                    <b>Pre-authorisation Amount:</b>
                </td>
                <td class="left">
                    <asp:Label ID="lblAuthAmount" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="left">
                    <b>TransactionId</b>
                </td>
                <td class="left">
                    <asp:Label ID="lblTransactionId" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center">
                <input type="button" id="bntPrint" value="Print" onclick="javascript:PrintDiv()" />
            </td>
        </tr>
    </table>
</asp:Content>
