﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/CORMaster.master" AutoEventWireup="true"
    CodeFile="BookingEditSearch.aspx.cs" Inherits="Booking_BookingEditSearch" %>

<asp:Content ID="ContentPage" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        $(function () {
            $("#liBooking").removeClass('active')
            $("#liBookingEdit").addClass('active')
            $("#liBookingCancelDetails").removeClass('active')
            $("#liBookingUsreProfile").removeClass('active')
            $("#liBookingCancelSearch").removeClass('active')

            $("#<%=txtBookingId.ClientID%>").keyup(function () {

                var strPass = $("#<%=txtBookingId.ClientID%>").val();
                var strLength = strPass.length;
                var lchar = strPass.charAt((strLength) - 1);
                var cCode = CalcKeyCode(lchar);
                if (cCode < 46 || cCode > 57 || cCode == 47) {
                    var myNumber = strPass.substring(0, (strLength) - 1);
                    $("#<%=txtBookingId.ClientID%>").val(myNumber);
                    alert("Enter only numeric value.")
                }
            });

            $("#<%=bntSearch.ClientID%>").click(function () {

                if ($("#<%=txtBookingId.ClientID%>").val() == "") {
                    alert("Enter booking id");
                    $("#<%=txtBookingId.ClientID%>").focus();
                    return false;
                }

                else if (isNaN($("#<%=txtBookingId.ClientID%>").val())) {
                    alert("Enter only numeric value");
                    $("#<%=txtBookingId.ClientID%>").focus();
                    return false;

                }
            });

        });
        function CalcKeyCode(aChar) {
            var character = aChar.substring(0, 1);
            var code = aChar.charCodeAt(0);
            return code;
        }
    </script>
    <table class="booking_cor" cellpadding="2" cellspacing="5" border="0" width="40%">
        <tr>
            <td class="bookings_h" colspan="4" align="center">
                <b>Edit Booking</b>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Label ID="lblMesaage" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <b>Booking Id</b>
            </td>
            <td style="white-space: nowrap">
                <asp:TextBox ID="txtBookingId" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="rfvBooking" runat="server" ControlToValidate="txtBookingId"
                    ErrorMessage="*" ForeColor="Red" Display="Dynamic">
                </asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Button ID="bntSearch" runat="server" Text="Search" OnClick="bntSearch_Click" />
            </td>
        </tr>
    </table>
    <br />
    <table cellpadding="2" cellspacing="2" border="1" id="tblDetailsShows" runat="server"
        visible="false">
        <thead>
            <tr style="background-color: #990000;">
                <th style="color: White;">
                    BookingId
                </th>
                <th style="color: White;">
                    Guest Name
                </th>
                <th style="color: White;">
                    Client Name
                </th>
                <th style="color: White;">
                    Pickup City Name
                </th>
                <th style="color: White;">
                    Pickup date
                </th>
                <th style="color: White;">
                    Pickup Time
                </th>
                <th style="color: White;">
                    Drop Off date
                </th>
                <th style="color: White;">
                    Drop Off time
                </th>
                <th style="color: White;">
                    Edit
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <asp:Label ID="lblBookingid" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblGuestName" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblClientName" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblPickupCityName" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblPickupdate" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblPickupTime" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblDropOffDate" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblDropOffTime" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:HyperLink ID="hdlLink" runat="server" Text="Edit"></asp:HyperLink>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
