﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.BusinessFramework;
using System.Drawing;
using System.Text;
using System.Net;
using System.IO;

public partial class PreAuth : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {
        OL_Booking objBookingOL = new OL_Booking();
        BL_Booking objBookingBL = new BL_Booking();

        objBookingOL = objBookingBL.GetPreAuthDetails();

        int CustOrgRegId = objBookingOL.ClientCoIndivID;
        string Mer_UserName = "00011500";
        string Mer_Refno = "WNOUD";

        if (objBookingOL.ObjDataTable.Rows.Count > 0)
        {
            for (int i = 0; i <= objBookingOL.ObjDataTable.Rows.Count - 1; i++)
            {
                objBookingOL.GuestName = objBookingOL.ObjDataTable.Rows[i]["GuestName"].ToString();
                objBookingOL.ClientCoIndivID = Convert.ToInt32(objBookingOL.ObjDataTable.Rows[i]["ClientCoIndivID"]);
                objBookingOL.ServiceUnitName = objBookingOL.ObjDataTable.Rows[i]["ServiceUnitName"].ToString();
                objBookingOL.BookingId = Convert.ToInt32(objBookingOL.ObjDataTable.Rows[i]["BookingId"]);
                objBookingOL.DropOffDate = Convert.ToDateTime(objBookingOL.ObjDataTable.Rows[i]["DropOffDate"]);
                objBookingOL.PickUpCityName = objBookingOL.ObjDataTable.Rows[i]["CityName"].ToString();
                objBookingOL.ApprovalAmt = Convert.ToDouble(objBookingOL.ObjDataTable.Rows[i]["ApprovalAmt"]);

                StringBuilder urlPost = new StringBuilder("https://secure.paymate.co.in/CCAPI/APIAuthTran.aspx?UserName=" + Mer_UserName);
                urlPost.Append("&RefNo=" + Mer_Refno.ToString());
                urlPost.Append("&EmployeeName=" + objBookingOL.GuestName.ToString());
                urlPost.Append("&AgentName=" + objBookingOL.GuestName.ToString());
                urlPost.Append("&AgentCode=" + objBookingOL.ClientCoIndivID);
                urlPost.Append("&AgentLocation=" + objBookingOL.ServiceUnitName.ToString());
                urlPost.Append("&RANumber=" + objBookingOL.BookingId);
                urlPost.Append("&RentalDate=" + objBookingOL.DropOffDate.ToShortDateString());
                urlPost.Append("&PickupCity=" + objBookingOL.PickUpCityName.ToString());
                urlPost.Append("&ReturnCity=" + objBookingOL.PickUpCityName.ToString());
                urlPost.Append("&CustOrgRegId=" + objBookingOL.ClientCoIndivID.ToString());
                urlPost.Append("&Amount=" + objBookingOL.ApprovalAmt);
                urlPost.Append("&OrderId=" + objBookingOL.BookingId);

                byte[] buffer1 = System.Text.Encoding.GetEncoding(1252).GetBytes(urlPost.ToString());
                WebRequest request1 = WebRequest.Create(urlPost.ToString());
                request1.Method = "POST";
                request1.ContentLength = buffer1.Length;

                request1.ContentType = "application/x-www-form-urlencoded";
                Stream dataStream1 = request1.GetRequestStream();
                dataStream1.Write(buffer1, 0, buffer1.Length);
                dataStream1.Close();

                HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
                Stream stm1 = response1.GetResponseStream();
                StreamReader sr1 = new StreamReader(stm1);
                string payMateRespose = sr1.ReadToEnd();

                string[] resultResponse = payMateRespose.Split('|');

                string eRRORCODE = resultResponse[0]; //Error Code '400 if Sucess

                objBookingOL.Transactionid = resultResponse[1]; ///Transaction ID


                //string transactionId = splitStatus[1]; //Transaction ID
                string transactionNo = resultResponse[1]; //Transaction ID
                string errorMessage = resultResponse[2]; //Error Message

                if (eRRORCODE == "400")
                {
                    objBookingOL.CCNo = resultResponse[3]; //Credit Card No
                    objBookingOL.AuthCode = resultResponse[4]; //Auth Code
                    objBookingOL.PGId = resultResponse[5];
                    if (resultResponse[6].ToString() == "VISA")
                    {
                        objBookingOL.CCType = 1;
                    }
                    else if (resultResponse[6].ToString() == "MASTER")
                    {
                        objBookingOL.CCType = 3;
                    }
                    else if (resultResponse[6].ToString() == "AMEX")
                    {
                        objBookingOL.CCType = 2;
                    }
                    objBookingOL = objBookingBL.UpdateNewPreAuthStatus(objBookingOL);
                }


            }
        }
    }
}