﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.BusinessFramework;
using System.Data.OleDb;
using System.Text;
using System.Net;
using System.IO;
using ReportingTool;


public partial class GuestProfile_UploadGuestDetails : System.Web.UI.Page
{
    private OL_GuestUpload objguestuploadOL = null;
    private BL_GuestUpload objguestuploadBL = null;
    clsAdmin objAdmin = new clsAdmin();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindGuestUpload();
            btnSend.Visible = false;

            if (ChkSendmail.Checked == true)
            {
                btnSend.Visible = true;
            }
            else
            {
                btnSend.Visible = false;
            }
        }    
    }

    private void BindGuestUpload()
    {
        objguestuploadOL = new OL_GuestUpload();
        objguestuploadBL = new BL_GuestUpload();
        objguestuploadOL.ClientCoId = Convert.ToInt32(Session["ClientCoId"]);
        try
        {
            objguestuploadOL = objguestuploadBL.GetClientName(objguestuploadOL);

            if (objguestuploadOL.ObjDataTable.Rows.Count > 0)
            {

                ddlClientCoId.DataSource = objguestuploadOL.ObjDataTable;
                ddlClientCoId.DataTextField = "CLIENTCONAME";
                ddlClientCoId.DataValueField = "CLIENTCOID";
                ddlClientCoId.DataBind();
                ddlClientCoId.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlClientCoId.DataSource = null;
                ddlClientCoId.DataBind();
            }

        }
        catch (Exception Ex)
        {
            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            throw new Exception(Ex.Message);
        }
    }
    private void clear()
    {
        Gvguest.DataSource = null;
        Gvguest.DataBind();
        GvSavedGuests.DataSource = null;
        GvSavedGuests.DataBind();
    }

    private void clearSelected()
    {
       
        upldFile.Visible = true;
        btnUpload.Visible = true;
        Gvguest.DataSource = null;
        Gvguest.DataBind();
        GvSavedGuests.DataSource = null;
        GvSavedGuests.DataBind();
        lblMsg.Text = "";
    }


    protected void ddlClientCoId_SelectedIndexChanged(object sender, EventArgs e)
    {

        clearSelected(); 

        objguestuploadOL = new OL_GuestUpload();
        objguestuploadBL = new BL_GuestUpload();

        try
        {
            if (ddlClientCoId.SelectedIndex > 0)
            {
                objguestuploadOL.ClientCoId = Convert.ToInt32(ddlClientCoId.SelectedValue);
                objguestuploadOL = objguestuploadBL.GetClientCoAddID(objguestuploadOL);
                if (objguestuploadOL.ObjDataTable.Rows.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt = objguestuploadOL.ObjDataTable;
                    lblClientCoAddID.Text = dt.Rows[0]["ClientCoAddID"].ToString();  
                }
                else
                {
                    ddlClientCoId.DataSource = null;
                    ddlClientCoId.DataBind();

                }

            }
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            throw new Exception(Ex.Message);
        }
    }

    private void clearAll()
    {

        ddlClientCoId.SelectedIndex = 0;
        upldFile.Visible = true;
        lblMsg.Text = "";
        lblMsg.Visible = false;
        Gvguest.DataSource = null;
        Gvguest.DataBind();
        GvSavedGuests.DataSource = null;
        GvSavedGuests.DataBind();
        btnSend.Visible = false;
        btnUpload.Visible = true;
        RequiredFieldValidator2.Visible = false;
        ChkSendmail.Checked = false;
        lblSelectFile.Visible = true;
    }
    private void clearFileUpload()
    {
        upldFile.Dispose();
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {

            if (ddlClientCoId.SelectedIndex == 0)
            {
                lblMsg.Text = "Please select client !";
                ClientScript.RegisterClientScriptBlock(GetType(), "Javascript", "<script>alert(" + lblMsg.Text + ")</script>");
                return;
            }

            if (upldFile.ToString() == "")
            {
                lblMsg.Text = "Please select file !";
                ClientScript.RegisterClientScriptBlock(GetType(), "Javascript", "<script>alert(" + lblMsg.Text + ")</script>");
                return;
            }

            objguestuploadOL = new OL_GuestUpload();
            objguestuploadBL = new BL_GuestUpload();
            DataTable dtEGuestDetails = new DataTable();


            string fileName, filePath;
            fileName = upldFile.PostedFile.FileName;
            int TotalRecord = 0, UploadedRecord = 0, FailureRecord = 0;
            int loopCntr = 0;
            DataSet dtSet = new DataSet();
            DataTable dtTbl = new DataTable();

            dtTbl.Columns.Add("ClientCoId"); dtTbl.Columns.Add("ClientCoAddId"); dtTbl.Columns.Add("Fname");
            dtTbl.Columns.Add("Mname"); dtTbl.Columns.Add("Lname");
            dtTbl.Columns.Add("Phone1"); dtTbl.Columns.Add("EmailId");
            dtTbl.Columns.Add("PaymentTerms"); dtTbl.Columns.Add("Remarks");
            dtTbl.Columns.Add("Active"); dtTbl.Columns.Add("IsVIP");
            dtTbl.Columns.Add("PreAuthNotRequire");
            dtTbl.Columns.Add("Gender"); dtTbl.Columns.Add("EmpCode");
            dtTbl.Columns.Add("UploadStatus"); dtTbl.Columns.Add("UploadType"); dtTbl.Columns.Add("Pass");

            HttpPostedFile myFile = upldFile.PostedFile;
            int fileSize = myFile.ContentLength;
            if (upldFile.HasFile)
            {
                DataTable dt = new DataTable();

                if (fileSize > 10240000)
                {
                    lblMsg.Text = "File uploaded should be less than 10 MB.";
                    return;
                }
                byte[] bData = new byte[fileSize];
                myFile.InputStream.Read(bData, 0, fileSize);

                string text = DateTime.Today.ToString("yyyy-MM-ddTHH:mm:ss.fff");


                //DateTime dateOnly=DateTime.;
                //DateTime timeOnly;
                //DateTime combined = dateOnly.Date.Add(timeOnly.TimeOfDay);

                String hour;
                hour = DateTime.Now.ToString("HH");

                String Minute;
                Minute = DateTime.Now.ToString("mm");

                //filePath = System.AppDomain.CurrentDomain.BaseDirectory + "Files\\" + text + "_" + ddlClientCoId.SelectedValue.ToString() + ".xls";
                filePath = System.AppDomain.CurrentDomain.BaseDirectory + "Files\\" + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000") + "_" + hour + Minute + "_" + ddlClientCoId.SelectedValue.ToString() + ".xlsx";
                clsAdmin.SaveFile(filePath, bData);

                dtSet = objAdmin.ExportFromExcelToDataSet(filePath, true, "GG");

                //Gvguest.Caption = Path.GetFileName(filePath);
                Gvguest.DataSource = dtSet;
                Gvguest.DataBind();
                Gvguest.Visible = false;  

            }
            else
            {
                lblMsg.Text = "No file find to upload...";
                return;
            }

            string DuplicateRecords = ""; int DuplicateCount = 0;
            string CCRecords = ""; int CCCount = 0;
            string isStatus = "";
            objguestuploadOL = objguestuploadBL.GetTopStatus(objguestuploadOL);
            if (objguestuploadOL.ObjDataTable.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                dt = objguestuploadOL.ObjDataTable;
                objguestuploadOL.status = Convert.ToInt32(dt.Rows[0]["UploadStatus"]) + 1;
            }
           
            //DataSet dsstatus = new DataSet();
            //dsstatus = objAdmin.GetBulk_status(1);
            //int status;
            //status = Convert.ToInt32(dsstatus.Tables[0].Rows[0]["UploadStatus"]) + 1;

            if (dtSet.Tables[0].Rows.Count > 1)
            {
                lblMsg.Text = "";
                loopCntr = 0;
                while (loopCntr < dtSet.Tables[0].Rows.Count)
                {
                    if ((dtSet.Tables[0].Rows[loopCntr][3].ToString().Trim() != "") && (dtSet.Tables[0].Rows[loopCntr][4].ToString().Trim() != ""))
                    {
                        DataRow tblRow;
                        tblRow = dtSet.Tables[0].Rows[loopCntr];
                        DataSet tmpDS = new DataSet();

                        objguestuploadOL.ClientCoId = Convert.ToInt32(ddlClientCoId.SelectedValue);
                        objguestuploadOL.ClientCoAddId = Convert.ToInt32(lblClientCoAddID.Text);
                        objguestuploadOL.Fname = tblRow[0].ToString();
                        objguestuploadOL.Mname = tblRow[1].ToString();
                        objguestuploadOL.Lname = tblRow[2].ToString();
                        objguestuploadOL.Phone1 = tblRow[3].ToString();
                        objguestuploadOL.EmailId = tblRow[4].ToString();
                        objguestuploadOL.PaymentTerms = tblRow[5].ToString();
                        objguestuploadOL.Remarks = tblRow[6].ToString();
                        objguestuploadOL.Active = Convert.ToBoolean(tblRow[7]);
                        objguestuploadOL.IsVIP = Convert.ToBoolean(tblRow[8]);
                        objguestuploadOL.PreAuthNotRequire = Convert.ToBoolean(tblRow[9]);
                        objguestuploadOL.Gender = tblRow[10].ToString();
                        objguestuploadOL.EmpCode = "";
                        objguestuploadOL.UploadStatus = objguestuploadOL.status;
                        objguestuploadOL.UploadType = "Exl";
                        objguestuploadOL.OutStatus = "Test";
                                               
                        objguestuploadOL = objguestuploadBL.BulkGuest_Upload(objguestuploadOL);
                        isStatus = objguestuploadOL.OutStatus;
                        if (isStatus == "D")
                        {
                            DuplicateCount += 1;
                            DuplicateRecords = DuplicateRecords + "," + (loopCntr + 1);
                            FailureRecord += 1;
                        }

                        else if (isStatus == "C")
                        {
                            CCCount += 1;
                            CCRecords = CCRecords + "," + (loopCntr + 1);
                            FailureRecord += 1;
                        }
                        else
                        {
                            UploadedRecord += 1;
                        }

                        TotalRecord += 1;
                    }

                   

                    loopCntr += 1;
                }


            }
            else
            {
                lblMsg.Text = "No record found to upload...";
                ClientScript.RegisterClientScriptBlock(GetType(), "Javascript", "<script>alert(" + lblMsg.Text + ")</script>");
            }

            if (TotalRecord > 0)
            {
                if (DuplicateRecords != "" || CCRecords!="")
                {
                    if (DuplicateRecords == "")
                    {
                        DuplicateRecords = 0.ToString();
                    }
                    if (CCRecords == "")
                    {
                        CCRecords = 0.ToString();
                    }

                    if (UploadedRecord != 0)
                    {
                       
                    objguestuploadOL = objguestuploadBL.GetDetailAftrSave(objguestuploadOL);
                    DataTable dt = new DataTable();
                    dt = objguestuploadOL.ObjDataTable;
                    GvSavedGuests.DataSource = dt;
                    GvSavedGuests.DataBind();
                    if (ChkSendmail.Checked == true)
                    {
                        btnSend.Visible = true;
                    }
                    else
                    {
                        btnSend.Visible = false;
                    }
                    GvSavedGuests.Visible = true;
                    upldFile.Visible = false;
                    lblSelectFile.Visible = false;
                    pnlSavedGV.Visible = true;
                    pnlGV.Visible = true;
                    lblGv.Visible = false;
                    Gvguest.DataSource = dtSet;
                    Gvguest.DataBind();
                    Gvguest.Visible = true;  
                   
                    }

                    if (UploadedRecord == 0)
                    {
                        lblGv.Visible = false;
                        btnSend.Visible = false;
                        Gvguest.Visible = true;
                        pnlSavedGV.Visible = false;
                        pnlGV.Visible = true; 
                    }

                    lblMsg.Visible = true;
                    lblMsg.Text = "Total record -" + TotalRecord + " ,Uploaded Record -" + UploadedRecord + " ,Duplicate Records -" + DuplicateCount + " and Duplicate Records found in rows " + DuplicateRecords.Remove(0, 1) + " And CC Records -" + CCCount + " and CC Records found in rows " + CCRecords.Remove(0, 1) + " cannot be uploaded";
                   // lblMsgCC.Text = "Total record -" + TotalRecord + " ,Uploaded Record -" + UploadedRecord + " ,CC Records -" + CCCount + " and CC Records found in rows " + CCRecords.Remove(0, 1) + " cannot be uploaded";
                   
                    upldFile.Visible = false;
                    btnUpload.Visible = false;
                    lblSelectFile.Visible = false;
                    ClientScript.RegisterClientScriptBlock(GetType(), "Javascript", "<script>alert(" + lblMsg.Text + ")</script>");

                }
                else
                {

                    if (UploadedRecord != 0)
                    {

                        objguestuploadOL = objguestuploadBL.GetDetailAftrSave(objguestuploadOL);
                        DataTable dt = new DataTable();
                        dt = objguestuploadOL.ObjDataTable;
                        GvSavedGuests.DataSource = dt;
                        GvSavedGuests.DataBind();
                        if (ChkSendmail.Checked == true)
                        {
                            btnSend.Visible = true;
                        }
                        else
                        {
                            btnSend.Visible = false;
                        }
                        GvSavedGuests.Visible = true;
                        upldFile.Visible = false;
                        lblSelectFile.Visible = false;
                        pnlSavedGV.Visible = true;
                        pnlGV.Visible = false;
                        lblGv.Visible = false; 

                    }

                    if (UploadedRecord == 0)
                    {
                        lblGv.Visible = false;
                        btnSend.Visible = false;
                        Gvguest.Visible = true;
                        pnlSavedGV.Visible = false;
                        pnlGV.Visible = true; 
                    }

                    lblMsg.Visible = true;  
                    lblMsg.Text = "All Rows uploaded successfully !";
                    upldFile.Visible = false;
                    btnUpload.Visible = false;
                    lblSelectFile.Visible = false;
                    lblGv.Visible = false; 
                    ClientScript.RegisterClientScriptBlock(GetType(), "Javascript", "<script>alert(" + lblMsg.Text + ")</script>");

                }
            }
            else
            {
                lblMsg.Visible = true;  
                lblMsg.Text = "Data not uploaded, kindly check excel sheet !";
                ClientScript.RegisterClientScriptBlock(GetType(), "Javascript", "<script>alert(" + lblMsg.Text + ")</script>");
            }
        }

        catch (Exception ex)
        {
            lblMsg.Visible = true;  
            lblMsg.Text = ex.Message;
            ClientScript.RegisterClientScriptBlock(GetType(), "Javascript", "<script>alert(" + lblMsg.Text + ")</script>");

        }
    }


    void Mail(string From, string To, string Subject, string Body)
    {
        try
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.To.Add(To);
            //message.To.Add("developer1@carzonrent.com");
            message.Subject = Subject;
            message.From = new System.Net.Mail.MailAddress(From);
            message.IsBodyHtml = true;
            message.Body = Body;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
            smtp.EnableSsl = true;
            smtp.Credentials = new NetworkCredential("booking@carzonrent.com", "Carz@1212");
            smtp.Send(message);
        }
        catch (Exception ex)
        {
            ErrorLogClass.LogErrorToLogFile(ex, "an error occured");
        }
    }

    public void SendMail_RegisteredGuest(OL_GuestUpload objguestuploadOL)
    {
        string From = "Reserve Team<services@carzonrent.com>";
        string MailTo = objguestuploadOL.EmailId;
        string Subject = "Welcome to Carzonrent.";
        StringBuilder MailBody = new StringBuilder();
        MailBody = MailBody.Append("Dear " + objguestuploadOL.Fname + " " + objguestuploadOL.Lname + ",");
        MailBody = MailBody.Append("<br/>");
        MailBody = MailBody.Append("<br/>");
        MailBody = MailBody.Append("We welcome you to carzonrent. ");
        MailBody = MailBody.Append("Please find below your password to access your account at Carzonrent.");
        MailBody = MailBody.Append("<br/>");
        MailBody = MailBody.Append("<br/>");
        MailBody = MailBody.Append("Password: " + objguestuploadOL.Pass);
        MailBody = MailBody.Append("<br/>");
        MailBody = MailBody.Append("Please Login to the below URL to update your password and perform online transaction.");
        MailBody = MailBody.Append("<br/>");
        MailBody = MailBody.Append("<br/>");
        //MailBody = MailBody.Append("You may visit Carzonrent portal to do transaction online on the following link.");
        //MailBody = MailBody.Append("<br/>");
        //MailBody = MailBody.Append("http://insta.carzonrent.com/Corbooking/Login.aspx");
        MailBody = MailBody.Append("http://corporate.carzonrent.com/CorporateSite/Corporatelogin.asp");
        MailBody = MailBody.Append("<br/>");
        MailBody = MailBody.Append("<br/>");
        MailBody = MailBody.Append("Warm Regards,");
        MailBody = MailBody.Append("<br/>");
        MailBody = MailBody.Append("<br/>");
        MailBody = MailBody.Append("Carzonrent (I) Pvt Ltd");
        MailBody = MailBody.Append("<br/>");
        MailBody = MailBody.Append("<br/>");
        Mail(From, MailTo, Subject, MailBody.ToString());
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        try
        {
            HttpPostedFile myFile = upldFile.PostedFile;
            if (!upldFile.HasFile)
            {
                if (GvSavedGuests.Rows.Count > 0)
                {

                    foreach (GridViewRow gvRow in GvSavedGuests.Rows)
                    {
                        objguestuploadOL = new OL_GuestUpload();
                        objguestuploadBL = new BL_GuestUpload();
                        DataTable dtEGuestDetails = new DataTable();

                        HiddenField hdnPass = (HiddenField)gvRow.FindControl("hdnPass");
                        Label lblClientCoAddId = (Label)gvRow.FindControl("lblClientCoAddId");
                        Label lblFname = (Label)gvRow.FindControl("lblFname");
                        Label lblLname = (Label)gvRow.FindControl("lblLname");
                        Label lblPhone1 = (Label)gvRow.FindControl("lblPhone1");
                        Label lblEmailId = (Label)gvRow.FindControl("lblEmailId");
                        
                        objguestuploadOL.ClientCoAddId = Convert.ToInt32(lblClientCoAddID.Text);
                        objguestuploadOL.Fname = lblFname.Text;
                        objguestuploadOL.Lname = lblLname.Text;
                        objguestuploadOL.Phone1 = lblPhone1.Text;
                        objguestuploadOL.EmailId = lblEmailId.Text;
                        objguestuploadOL.Pass  = hdnPass.Value;
                    

                        objguestuploadOL = objguestuploadBL.SendBulkGuestUploadMailDetails(objguestuploadOL);
                        if (!string.IsNullOrEmpty(objguestuploadOL.EmailId.ToString()))
                        {
                            SendMail_RegisteredGuest(objguestuploadOL);
                        }

                        btnSend.Visible = false;
                        lblMsg.Text = "Congratulations<br />Data successfully Saved.<br />" + "Your details are sent to your E-mail id";
                        lblMsg.Text = "Thanks for uploading your details, Welcome to Carzonrent";
                    }

                    ddlClientCoId.SelectedIndex = 0;
                    btnClear.Visible = true;
                    upldFile.Visible = false;
                    RequiredFieldValidator2.Visible = false;
                    GvSavedGuests.DataSource = null;
                    GvSavedGuests.DataBind();
                    pnlSavedGV.Visible = false; 
 
                }
            }
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        //Page.Response.Redirect(Page.Request.Url.ToString(), true);
        //btnClear_Click(btnClear, new EventArgs());
        //Response.Write("<script type='text/javascript'> setTimeout('location.reload(true); ', timeout);</script>");
        clearAll();
        //Response.Redirect("~/GuestProfile/Uploadguestdetails.aspx",true);
        //Response.Redirect(Request.RawUrl);
        //Server.TransferRequest(Request.Url.AbsolutePath, false);
    }
    protected void ChkSendmail_CheckedChanged(object sender, EventArgs e)
    {
        //if (ChkSendmail.Checked == true)
        //{
        //    btnSend.Visible = true;
        //}
        //else
        //{
        //    btnSend.Visible = false;  
        //}
    }
}