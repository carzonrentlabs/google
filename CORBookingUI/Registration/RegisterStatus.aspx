﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegisterStatus.aspx.cs" Inherits="Registration_RegisterStatus" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">   
    <title>Untitled Page</title>
    <script language="Javascript" type="text/javascript" src="../Scripts/CommonScript.js"></script>
    <script language="javascript" type="text/javascript">
        function goBack() {
            window.history.go(-1)
        }
    </script>  
    <link href="../Styles/HertzInt.css" type="text/css" rel="stylesheet" />
</head>
<body>
   <%-- <form id="form1" runat="server">
    <div>    
    </div>
    </form>--%>
    <form id="frmregister" name="frmregister" method="post" runat="server">

        <script language="Javascript" type="text/javascript" src="../Scripts/CommonScript.js"></script>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 800px; padding-left: 60px;
            padding-bottom: 40px;">
            <tr>
                <td align="left">
                    <img src="../Images/logo_carzonret.gif" />&nbsp;</td>
            </tr>
        </table>
        <table border="2" cellpadding="0" cellspacing="0" style="width: 65%; border-width: medium;"
            align="center">
            <tr>
                <td style="font-weight: bold; font-size: larger; line-height: 30pt; font-family: Calibri;
                    text-align: center; height: 40px; color: Red; font-size: large; border-bottom-width: medium;
                    background-color: #A4A4A4">
                    Return Result
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr style="padding-top: 20px;">
                            <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                                Message:
                            </td>
                            <td>
                                &nbsp;</td>
                            <td align="left">
                                <asp:Label ID="txtMessage" runat="server"></asp:Label>
                            </td>
                            <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                                RequestMode:
                            </td>
                            <td>
                                &nbsp;</td>
                            <td align="left">
                                <asp:Label ID="txtrmode" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                                Code:
                            </td>
                            <td>
                                &nbsp;</td>
                            <td align="left">
                                <asp:Label ID="txtcode" runat="server" MaxLength="12"></asp:Label>
                            </td>
                            <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                                Client ID:
                            </td>
                            <td>
                                &nbsp;</td>
                            <td align="left">
                                <asp:Label ID="txtClientID" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align: center">
                                <asp:Button ID="btnRegister" Text="Go To Registration" Font-Bold="True" Font-Size="Medium" 
                                    Font-Names="Calibri" BorderStyle="Groove" ForeColor="Red" runat="server" OnClick="btnRegister_Click" />
                                <asp:Button ID="btnBookNow" Text="BookNow" Font-Bold="True" Font-Size="Medium" 
                                    Font-Names="Calibri" BorderStyle="Groove" ForeColor="Red" runat="server" OnClick="btnBookNow_Click" />
                                <asp:Button ID="btnClose" Text="Close Window" Font-Bold="True" Font-Size="Medium"
                                    Font-Names="Calibri" BorderStyle="Groove" ForeColor="Red" runat="server" OnClick="btnClose_Click" />
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lblMsg" runat="server" Font-Bold="True" ForeColor="Green"></asp:Label></td>
                    <asp:HiddenField ID="hdnClientID" Value="" runat="server" />
            </tr>
        </table>
        <div>
            <%--<input type="hidden" id="UserName" name="UserName" runat="server" />
            <input type="hidden" id="RefNo" name="RefNo" runat="server" />--%>
            <%--<input type="hidden" id="CustOrgRegId" name="CustOrgRegId" runat="server" />
            <input type="hidden" id="RequestMode" name="RequestMode" runat="server" />--%>
        </div>
    </form>
</body>
</html>
