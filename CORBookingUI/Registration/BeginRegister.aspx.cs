﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.BusinessFramework;
using System.Data;
using System.Text;
using System.Net;
using System.IO;
using System.Security.Cryptography;
/// <summary>
/// The purpose of thsi UI is to register Guest Users 
/// </summary>
public partial class BeginRegister : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            DataSet DS = new DataSet();
            BL_Registration objRegistrationBL = new BL_Registration();
            string EmailFinal = "";

            string[] GuestEmailDetails = txtEmail.Text.Trim().Split('@');
            string[] EmailSplit = GuestEmailDetails[1].Split('.');

            EmailFinal = IgnoreLastDomainPart(EmailFinal, EmailSplit);

            if (EmailFinal == "")
            {
                lblmessage.Text = "Invalid EmailID.";
            }
            else
            {
                lblmessage.Text = "";
                DS = objRegistrationBL.SearchClientByEmail(EmailFinal);
                
                ddlclientname.Items.Clear();
                ddlclientname.Visible = false;
                lblClientName.Visible = false;
                btnRedirect.Visible = false;

                if (DS.Tables[0].Rows.Count > 0)
                {
                    int k = 0;
                    for (int i = 0; i <= DS.Tables[0].Rows.Count - 1; i++)
                    {
                        ListItem clientlist = new ListItem();
                        string ValidEmail;

                        ValidEmail = EmailValidation(txtEmail.Text.Trim(), DS.Tables[0].Rows[i]["EmailID"].ToString());
                        if (ValidEmail == "Success")
                        {
                            if (k == 0)
                            {
                                ListItem defaultvalue = new ListItem();
                                defaultvalue.Value = "0";
                                defaultvalue.Text = "Select";
                                ddlclientname.Items.Add(defaultvalue);
                                k = 1;    
                            }
                            clientlist.Value = DS.Tables[0].Rows[i]["ClientCoID"].ToString();
                            clientlist.Text = DS.Tables[0].Rows[i]["ClientCoName"].ToString();
                            ddlclientname.Items.Add(clientlist);
                        }
                    }
                    if (k == 1)
                    {
                        ddlclientname.Visible = true;
                        lblClientName.Visible = true;
                        btnRedirect.Visible = true;
                    }
                    else
                    {
                        lblmessage.Text = "EmailID is not registered.";
                        hllink.Visible = true;
                    }

                    //ddlclientname.SelectedValue = "0";
                }
                else
                {
                    lblmessage.Text = "EmailID is not registered.";
                    hllink.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            lblmessage.Text = "Invalid EmailID.";
        }
    }

    protected void btnRedirect_Click(object sender, EventArgs e)
    {
        //Response.Redirect("GuestRegistration.aspx?ClientCoId=" + ddlclientname.SelectedItem.Value, true);
        string url = "http://insta.carzonrent.com/corbooking/Registration/GuestRegistration.aspx?ClientCoId=" + ddlclientname.SelectedItem.Value; //Live
        //string url = "http://localhost:4053/CORBookingUI/Registration/GuestRegistration.aspx?ClientCoId=" + ddlclientname.SelectedItem.Value; //Test

        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        sb.Append("<html><head></head><body>");
        sb.Append("<form name='form1' method='POST' action='" + url + "'>");
        sb.Append("<input type='hidden' value='" + txtEmail.Text.Trim() + "' id = 'hdnRegisteredEmailID' name = 'hdnRegisteredEmailID'>");
        sb.Append("<input type='hidden' value='" + ddlclientname.SelectedItem.Value + "' id = 'hdnClientCoID' name = 'hdnClientCoID'>");
        sb.Append("</form>");
        sb.Append("<script language='javascript'>document.form1.submit();</script>");
        sb.Append("</body></html>");
        //return sb.ToString();
        Response.Write(sb);
        Response.End();
    }

    public string IgnoreLastDomainPart(string EmailFinal, string[] EmailSplit)
    {
        if (EmailSplit.Length > 1)
        {
            for (int i = 0; i <= EmailSplit.Length - 1; i++)
            {
                if (i != EmailSplit.Length - 1)
                {
                    if (EmailSplit[i].ToLower() != "co")
                    {
                        if (EmailFinal == "")
                        {
                            EmailFinal = EmailSplit[i];
                        }
                        else
                        {
                            EmailFinal = EmailFinal + "." + EmailSplit[i];
                        }
                    }
                }
            }
        }
        return EmailFinal;
    }

    private string EmailValidation(string GuestEmailID, string RegisteredEmailID)
    {
        string Status = "Failure";
        //string EmailID = "techreports@carzonrent.com";

        try
        {
            string[] GuestEmailDetails = GuestEmailID.Split('@');
            string[] RegisteredEmailDetails = RegisteredEmailID.Split('@');

            if (GuestEmailDetails[1] == RegisteredEmailDetails[1])
            {
                Status = "Success";
            }
            else
            {
                string RegisteredEmailFinal = "", GuestDomainFinal = "";

                string[] RegisteredEmailSplit = RegisteredEmailDetails[1].Split('.');
                RegisteredEmailFinal = IgnoreLastDomainPart(RegisteredEmailFinal, RegisteredEmailSplit);

                if (GuestEmailDetails[1].Contains("."))
                {

                    string[] GuestDomainSplit = GuestEmailDetails[1].Split('.');
                    GuestDomainFinal = IgnoreLastDomainPart(GuestDomainFinal, GuestDomainSplit);

                    GuestDomainSplit = GuestDomainFinal.Split('.');

                    if (GuestDomainFinal == RegisteredEmailFinal)
                    {
                        Status = "Success";
                    }
                    else
                    {
                        if (GuestDomainSplit.Length > 2)
                        {
                            if (RegisteredEmailDetails[1].Contains(GuestDomainSplit[0]))
                            {
                                if (GuestDomainSplit[1] + "." + GuestDomainSplit[2] == RegisteredEmailDetails[1])
                                {
                                    Status = "Success";
                                }
                                else
                                {
                                    Status = "Failure";
                                }
                            }
                        }
                        else if (GuestDomainSplit.Length == 2)
                        {
                            if (GuestDomainSplit[1] == RegisteredEmailFinal)
                            {
                                Status = "Success";
                            }
                            else
                            {
                                Status = "Failure";
                            }
                        }
                        else
                        {
                            Status = "Failure";
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Status = ex.ToString(); //"Failure";
        }
        return Status;
    }
}