﻿//===============================================================================
//Created By :	<Krishna Kumar>
//Create date:  <20-06-2015>
//Description:	<The purpose of thsi UI is to register Guest Users >
//===============================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.BusinessFramework;
using System.Data;
using System.Text;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using ReportingTool;
/// <summary>
/// The purpose of thsi UI is to register Guest Users 
/// </summary>
public partial class Registration_GuestRegistration : System.Web.UI.Page
{
    OL_Registration objRegistrationOL = null;
    BL_Registration objRegistrationBL = null;
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //string a = GeneralMethos.Encrypt("3771");

            trGuestRegistration.Visible = true;
            lblMessage.Text = "";
            Save.Visible = true;
            btnModify.Visible = true;
            if (!string.IsNullOrEmpty(Request.Form["hdnRegisteredEmailID"]))
            {
                txtEmailID.Text = Request.Form["hdnRegisteredEmailID"].ToString();
                txtEmailID.Enabled = false;
            }
            if (!string.IsNullOrEmpty(Request.Form["hdnClientCoID"]))
            {
                hdnClientCoID.Value = Request.Form["hdnClientCoID"].ToString();
                CustOrgRegId.Value = Request.Form["hdnClientCoID"].ToString();
            }

            BindRegisteredGuest();
        }
    }
    /// <summary>
    /// Bind Already Registered Guest
    /// </summary>
    private void BindRegisteredGuest()
    {
        txtFName.Focus();
        btnModify.Enabled = false;
        objRegistrationOL = new OL_Registration();
        objRegistrationBL = new BL_Registration();
        DataTable dtGuestDetails = new DataTable();
        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["Email"]) || !String.IsNullOrEmpty(Request.QueryString["ID"]))
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Email"]))
                {
                    objRegistrationOL.EmailId = Request.QueryString["Email"].ToString();
                    objRegistrationOL.ClientIndivId = 0;
                }
                else
                {
                    objRegistrationOL.EmailId = "";
                    objRegistrationOL.ClientIndivId = Convert.ToInt32(Request.QueryString["ID"]);
                }

                if (Request.QueryString["ID"] != null)
                {
                    try
                    {
                        NewChargingWebService.Service RecheckRegistration = new NewChargingWebService.Service();
                        string abc = RecheckRegistration.PaymateCorporateRegistrationStatusCheck(objRegistrationOL.ClientIndivId);
                    }
                    catch (Exception ex)
                    { 
                    
                    }
                }

                objRegistrationOL = objRegistrationBL.BindRegisteredGuest(objRegistrationOL);
                if (objRegistrationOL.FirstName != "")
                {
                    txtFName.Text = objRegistrationOL.FirstName;
                    txtLName.Text = objRegistrationOL.LastName;
                    txtEmailID.Text = objRegistrationOL.EmailId;
                    txtEmpCode.Text = objRegistrationOL.EmployeeCode;
                    txtMobNo.Text = objRegistrationOL.MobileNo;
                    ddlGender.SelectedValue = objRegistrationOL.Gender;
                    //ddlCCType.SelectedValue = objRegistrationOL.CCType.ToString();
                    ViewState["ClientIndivId"] = objRegistrationOL.ClientIndivId;

                    hdnPaymentTerms.Value = objRegistrationOL.PaymentTerms;

                    txtFName.ReadOnly = true;
                    txtLName.ReadOnly = true;
                    txtMobNo.ReadOnly = true;
                    txtEmailID.ReadOnly = true;
                    txtEmpCode.ReadOnly = true;

                    if ((!String.IsNullOrEmpty(Request.QueryString["AutoSubmit"]) && Request.QueryString["AutoSubmit"] == "true") || objRegistrationOL.AutoRedirect)
                    {
                        Save_Click(Save, null);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLogClass.LogErrorToLogFile(ex, "an error occured");
        }
    }
    /// <summary>
    /// User save it detail
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Save_Click(object sender, EventArgs e)
    {
        string MailStatus = "";
        objRegistrationOL = new OL_Registration();
        objRegistrationBL = new BL_Registration();
        DataTable dtEGuestDetails = new DataTable();

        if (chkIndia.Checked == true && txtMobNo.Text.Trim().Length > 10)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('For India only 10 digits is valid.');", true);
            return;
        }

        objRegistrationOL = GetControlsValues(objRegistrationOL);
        if (!string.IsNullOrEmpty((Request.QueryString["ID"])))
        {
            objRegistrationOL.ClientIndivId = Convert.ToInt32(Request.QueryString["ID"]);
        }
        else
        {
            objRegistrationOL.ClientIndivId = 0;
        }
        dtEGuestDetails = objRegistrationBL.ExistingGuest(objRegistrationOL);

        if (dtEGuestDetails.Rows.Count > 0 && Convert.ToBoolean(dtEGuestDetails.Rows[0]["Active"]) == false)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Your account has been deactived.Please contact your implant associate.');", true);
            return;
        }

        if (dtEGuestDetails != null && dtEGuestDetails.Rows.Count > 0)
        {
            txtFName.Text = Convert.ToString(dtEGuestDetails.Rows[0]["FName"]);
            txtLName.Text = Convert.ToString(dtEGuestDetails.Rows[0]["LName"]);
            txtEmailID.Text = Convert.ToString(dtEGuestDetails.Rows[0]["EmailID"]);
            txtMobNo.Text = Convert.ToString(dtEGuestDetails.Rows[0]["Phone1"]);

            txtFName.ReadOnly = true;
            txtLName.ReadOnly = true;
            txtEmailID.ReadOnly = true;

            hdnClientRegisterYN.Value = Convert.ToString(Convert.ToInt32(dtEGuestDetails.Rows[0]["ClientRegisteredYN"]));
            hdnCCRegisterYN.Value = Convert.ToString(Convert.ToInt32(dtEGuestDetails.Rows[0]["CCRegisteredYN"]));

            ddlGender.SelectedValue = Convert.ToString(dtEGuestDetails.Rows[0]["Gendor"]);

            hdnPaymentTerms.Value = Convert.ToString(dtEGuestDetails.Rows[0]["paymentterms"]);

            Save.Enabled = false;
            btnModify.Enabled = true;

            objRegistrationOL.ClientIndivId = Convert.ToInt32(dtEGuestDetails.Rows[0]["ClientCoIndivId"]);
            ViewState["ClientIndivId"] = Convert.ToString(dtEGuestDetails.Rows[0]["ClientCoIndivId"]);
            hdnisPaymateCorporateModule.Value = Convert.ToString(dtEGuestDetails.Rows[0]["isPaymateCorporateModule"]);

            ConsolidateRegistrationCall(objRegistrationOL, false); //Registration Proccess

            /*
            if (objRegistrationOL.isPaymateCorporateModule && (objRegistrationOL.PaymentTerms == "CC" || hdnPaymentTerms.Value == "CC" || Convert.ToBoolean(Request.QueryString["forceSaveCard"]) == true))
            {

                string ResponseStatus = "";
                string Status = "";
                if (Convert.ToBoolean(hdnisPaymateCorporateModule.Value))
                {
                    Status = "U";
                }
                else
                {
                    Status = "A";
                }
                ResponseStatus = Registration(objRegistrationOL, "", Status);    //Guest Details on paymate
                Response.Write(ResponseStatus);
                Response.End();

            }
            else if (!objRegistrationOL.isPaymateCorporateModule && (objRegistrationOL.PaymentTerms == "CC" || hdnPaymentTerms.Value == "CC" || Convert.ToBoolean(Request.QueryString["forceSaveCard"]) == true))
            {
                btnModify_Click(sender, e);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Mobile No. and Employee Code are already registered. You can Modify the information.');", true);
                //btnModify_Click(sender, e);
            }

            if (Request.QueryString["Email"] != "" && Request.QueryString["Email"] != null)
            {
                string returnvalue;
                returnvalue = CCRegistration("UCC");
                Response.Write(returnvalue);
                Reset();
            }
            */
        }
        else
        {
            hdnClientRegisterYN.Value = "0";
            hdnCCRegisterYN.Value = "0";

            objRegistrationOL.Password = GeneratePassword();
            objRegistrationOL = objRegistrationBL.RegisterGuest(objRegistrationOL);  //Guest Owned database
            ViewState["ClientIndivId"] = objRegistrationOL.ClientIndivId;

            //if (!string.IsNullOrEmpty(Request.Form["hdnClientCoID"]))
            //if (!string.IsNullOrEmpty(hdnClientCoID.Value))
            if(OldRegistrationProcessYN.Value != "1")
            //if (objRegistrationOL.isPaymateCorporateModule)
            {
                //MailStatus = SendMail_RegisteredGuest(objRegistrationOL);
                MailStatus = objAdmin.SendMail_RegisteredGuest(objRegistrationOL);
            }

            if (objRegistrationOL.PaymentTerms == "CC" || hdnPaymentTerms.Value == "CC" || Convert.ToBoolean(Request.QueryString["forceSaveCard"]) == true)
            {
                //string ResponseStatus = Registration(objRegistrationOL, "ACD", "A");    //Guest Details on paymate
                ////new registration Process
                ////if (objRegistrationOL.ClientCoId == 1171)
                //if (objRegistrationOL.isPaymateCorporateModule)
                //{
                //    Response.Write(ResponseStatus);
                //    Response.End();
                //}

                //string[] Response1 = ResponseStatus.Split('.');
                //string staus = Response1[0].ToString();
                //string[] splitStatus = staus.Split('|');
                //string splitCode = splitStatus[0].ToString();
                //string splitMessage = splitStatus[1].ToString();

                //if (objRegistrationOL.ClientIndivId > 1 && splitCode == "400")
                //{
                //    objRegistrationOL.Status = 0;
                //    objRegistrationOL = objRegistrationBL.UpdateGuestStatus(objRegistrationOL);  // Update client RegistrationYN =1
                //    string returnvalue;
                //    returnvalue = CCRegistration("ACC");
                //    Response.Write(returnvalue);
                //    Reset();
                //}
                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Error - " + splitMessage + " ');", true);
                //}

                ConsolidateRegistrationCall(objRegistrationOL, false); //Registration Proccess
            }
            else
            {
                if (btnregister.Visible == false)
                {
                    string EmailID = txtEmailID.Text;
                    Reset();

                    //MailStatus = SendMail_RegisteredGuest(objRegistrationOL);
                    //if (OldRegistrationProcessYN.Value == "1")
                    //{
                    //    MailStatus = objAdmin.SendMail_RegisteredGuest(objRegistrationOL);
                    //}

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Thank you for registering. Please check your email - "
                        + EmailID + " for login credentials');", true);
                }
            }
        }
    }

    private void Reset()
    {
        txtFName.Text = "";
        txtLName.Text = "";
        txtEmailID.Text = "";
        txtMobNo.Text = "";
        txtEmpCode.Text = "";
        if (txtEmailID.Enabled == false)
        {
            txtEmailID.Enabled = true;
        }
    }
    /// <summary>
    /// GetControlsValues Set Controls value into property.
    /// </summary>
    /// <param name="objRegistrationOL"></param>
    /// <returns></returns>
    private OL_Registration GetControlsValues(OL_Registration objRegistrationOL)
    {
        int ClientCoid = 0;
        if (!string.IsNullOrEmpty(hdnClientCoID.Value))
        {
            objRegistrationOL.ClientCoId = Convert.ToInt32(hdnClientCoID.Value);
            CustOrgRegId.Value = hdnClientCoID.Value;
        }
        else
        {
            if (int.TryParse(Request.QueryString["ClientCoId"], out ClientCoid))
            {
                objRegistrationOL.ClientCoId = ClientCoid;
            }
            else
            {
                if (Request.QueryString["ClientCoId"]==null)
                {
                    objRegistrationOL.ClientCoId = ClientCoid;
                }
                else
                {
                    objRegistrationOL.ClientCoId = Convert.ToInt32(GeneralMethos.Decrypt(HttpUtility.UrlDecode(Request.QueryString["ClientCoId"])));
                }
            }
            CustOrgRegId.Value = objRegistrationOL.ClientCoId.ToString();
        }
        //check new paymate registration process
        ClientDetails CD = new ClientDetails();
        //objRegistrationOL.isPaymateCorporateModule = objRegistrationBL.CheckPaymentCorporateModule(objRegistrationOL.ClientCoId);
        CD = objRegistrationBL.CheckPaymentCorporateModule(objRegistrationOL.ClientCoId);

        if (CD.isPaymateCorporateModule)
        {
            objRegistrationOL.isPaymateCorporateModule = true;
        }
        else
        {
            objRegistrationOL.isPaymateCorporateModule = false;
        }

        if (CD.ClientCoName != null)
        {
            if (CD.ClientCoName.Contains("Google India Private Limited - Direct - G2G"))
            {
                OldRegistrationProcessYN.Value = "1";
                btnregister.Visible = false;
            }
            else
            {
                OldRegistrationProcessYN.Value = "0";
                //if (CD.PaymentTerms == "CC" && objRegistrationOL.isPaymateCorporateModule)
                if (objRegistrationOL.isPaymateCorporateModule)
                {
                    btnregister.Visible = true;
                    trGuestRegistration.Visible = false;
                    lblMessage.Text = "You have Successfully registered. Please check your mail for password";
                    Save.Visible = false;
                    btnModify.Visible = false;
                    btnBookNow.Visible = true;
                }
                else
                {
                    btnregister.Visible = false;
                    trGuestRegistration.Visible = true;
                    lblMessage.Text = "";
                    Save.Visible = true;
                    btnModify.Visible = true;
                }
            }
        }
        
        objRegistrationOL.PaymentTerms = CD.PaymentTerms;
        //end new paymate registration process

        //objRegistrationOL.ClientCoId = Convert.ToInt32(GeneralMethos.Decrypt(strClientCoId)); 

        objRegistrationOL.FirstName = txtFName.Text.Trim();
        objRegistrationOL.LastName = txtLName.Text.Trim();
        objRegistrationOL.MobileNo = txtMobNo.Text.Trim();
        objRegistrationOL.EmailId = txtEmailID.Text.Trim();
        objRegistrationOL.EmployeeCode = txtEmpCode.Text.Trim();
        objRegistrationOL.Gender = ddlGender.SelectedItem.Value;
        //if(!String.IsNullOrEmpty(Request.QueryString["ID"]))
        //{
        //    objRegistrationOL.ClientIndivId = Convert.ToInt32(Request.QueryString["ID"]);
        //}
        //objRegistrationOL.CCType = Convert.ToInt32(ddlCCType.SelectedItem.Value);
        return objRegistrationOL;
    }
    /// <summary>
    /// Generate password in encrypted format
    /// </summary>
    /// <returns></returns>
    private string GeneratePassword()
    {
        string strPwdchar = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string strPwd = "";
        Random rnd = new Random();
        for (int i = 0; i <= 7; i++)
        {
            int iRandom = rnd.Next(0, strPwdchar.Length - 1);
            strPwd += strPwdchar.Substring(iRandom, 1);
        }
        return strPwd;
    }
  
    public string Registration(OL_Registration objRegistrationOL, string mode, string CreateUpdateStatus)
    {
        string ReturnString = "";

        if (objRegistrationOL.isPaymateCorporateModule)
        {
            ReturnString = CCRegistrationAmexMaster(CreateUpdateStatus, objRegistrationOL.ClientIndivId.ToString());
        }
        else if (OldRegistrationProcessYN.Value == "1")
        {
            //ReturnString = MasterVisaRegistration(objRegistrationOL, mode);
            ReturnString = CCRegistrationAmexMaster(CreateUpdateStatus, objRegistrationOL.ClientIndivId.ToString());
        }
        return ReturnString;
    }

    private static string MasterVisaRegistration(OL_Registration objRegistrationOL, string mode)
    {
        string url = string.Empty;
        url = "https://secure.paymate.co.in/CCAPI/APIReg.aspx";

        string UserName, RefNo, CustOrgRegId, ContactNo
        , EmailId, Name, Address, OtherDetails, RequestMode;

        UserName = "00011500"; //Not to Change
        RefNo = "WNOUD"; //Not to Change
        CustOrgRegId = Convert.ToString(objRegistrationOL.ClientIndivId); //ClientCoIndivID
        ContactNo = objRegistrationOL.MobileNo;
        EmailId = objRegistrationOL.EmailId;
        Name = objRegistrationOL.FirstName + " " + objRegistrationOL.LastName;
        Address = "Carzonrent"; //Blank //Harcore
        OtherDetails = "Others"; //Blank //Harcore
        RequestMode = mode; //ACD for adding //UCD for updating
        StringBuilder strNewPost = new StringBuilder("https://secure.paymate.co.in/CCAPI/APIReg.aspx?UserName=" + UserName);
        strNewPost.Append("&RefNo=" + RefNo + "&CustOrgRegId=" + CustOrgRegId + "&ContactNo=" + ContactNo);
        strNewPost.Append("&EmailId=" + EmailId + "&Name=" + Name + "&Address=" + Address);
        strNewPost.Append("&OtherDetails=" + OtherDetails + "&RequestMode=" + RequestMode);

        byte[] buffer1 = System.Text.Encoding.GetEncoding(1252).GetBytes(strNewPost.ToString());
        try
        {
            WebRequest request1 = WebRequest.Create(strNewPost.ToString());
            request1.Method = "POST";
            request1.ContentLength = buffer1.Length;

            request1.ContentType = "application/x-www-form-urlencoded";
            Stream dataStream1 = request1.GetRequestStream();
            dataStream1.Write(buffer1, 0, buffer1.Length);
            dataStream1.Close();

            HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse();
            Stream stm1 = response1.GetResponseStream();
            StreamReader sr1 = new StreamReader(stm1);
            string result1 = sr1.ReadToEnd();
            return result1;
        }
        catch (Exception ex)
        {
            return "0";
        }
        //******************************************************************************************************
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="Status"></param>
    /// <returns></returns>
    public string CCRegistration(string Status) //, int CCtype)
    {
        string ReturnString = "";
        //if (objRegistrationOL.ClientCoId == 1171)
        if (objRegistrationOL.isPaymateCorporateModule)
        {
            ReturnString = CCRegistrationAmexMaster("U", objRegistrationOL.ClientIndivId.ToString());
        }
        else
        {
            //ReturnString = CCRegistrationMaster(Status);
            ReturnString = CCRegistrationAmexMaster("U", objRegistrationOL.ClientIndivId.ToString());
        }
        return ReturnString;
    }

    private string CCRegistrationAmexMaster(string Action, string ClientIndivID)
    {
        //string url = "http://123.201.124.141/Beta/PMCorporate/RegisterTraveller.aspx";
        string url = "https://secure.paymate.co.in/pmcorporate/RegisterTraveller.aspx";

        string EncryptedParameter = "";
        string SysRegCode = "", EmpName = "", EmailId = "", MobileNo = "", travellerid = "";
        //SysRegCode = "tVYfXG"; //test
        //SysRegCode = "tQGcJM";  //Live
        SysRegCode = objRegistrationBL.GetPaymateSysRegCodeForIndiv(Convert.ToInt32(ClientIndivID));
        string AuthUserName = "", AuthPassword = "", AuthCode = "";

        //Live
        AuthUserName = "67592538";
        AuthPassword = "5BF24F3AA4CA";
        AuthCode = "2511C6A40B4A";

        EmpName = txtFName.Text.Trim() + " " + txtLName.Text.Trim();
        EmailId = txtEmailID.Text.Trim();
        MobileNo = txtMobNo.Text.Trim();
        travellerid = ClientIndivID.ToString();

        EncryptedParameter = URLEncrypt("BD=" + travellerid + "|" + SysRegCode + "|" + EmpName + "|"
            + EmailId + "|" + MobileNo + "|" + Action + "|" + AuthUserName + "|" + AuthPassword + "|" + AuthCode);

        url = url + "?" + EncryptedParameter;
        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        sb.Append("<html><head></head><body>");
        sb.Append("<form name='form1' method='POST' action='" + url + "'>");
        sb.Append("</form>");
        sb.Append("<script language='javascript'>document.form1.submit();</script>");
        sb.Append("</body></html>");
        return sb.ToString();

        //Response.Write(sb.ToString());
    }

    private string CCRegistrationMaster(string Status)
    {
        string url = "https://secure.paymate.co.in/CCAPI/APICCReg.aspx";

        //string url = "https://secure.paymate.co.in/Beta/CCAPI/APICCReg.aspx";  //Local testing 
        //string RedirectURL = " http://localhost:4053/CORBookingUI/Registration/RegisterStatus.aspx"; //Local

        string RedirectURL = "http://insta.carzonrent.com/Corbooking/Registration/RegisterStatus.aspx"; //live

        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        sb.Append("<html><head></head><body>");
        sb.Append("<form name='form1' method='POST' action='" + url + "'>");
        sb.Append("<input type='hidden' value='00011500' name='UserName'>");
        sb.Append("<input type='hidden' value='WNOUD' name='RefNo'>");
        sb.Append("<input type='hidden' value='" + Convert.ToInt32(ViewState["ClientIndivId"]) + "' name='CustOrgRegId'>");
        sb.Append("<input type='hidden' value='" + Status + "' name='RequestMode'>");
        sb.Append("<input type='hidden' value='" + RedirectURL + "' name='RedirectURL'>");
        sb.Append("</form>");
        sb.Append("<script language='javascript'>document.form1.submit();</script>");
        sb.Append("</body></html>");
        return sb.ToString();
    }
    /// <summary>
    /// btnModify_Click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnModify_Click(object sender, EventArgs e)
    {
        objRegistrationOL = new OL_Registration();
        objRegistrationBL = new BL_Registration();
        DataTable dtEGuestDetails = new DataTable();

        if (chkIndia.Checked == true && txtMobNo.Text.Trim().Length > 10)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('For India only 10 digits is valid');", true);
            return;
        }

        objRegistrationOL = GetControlsValues(objRegistrationOL);
        objRegistrationOL.ClientIndivId = Convert.ToInt32(ViewState["ClientIndivId"]);
        dtEGuestDetails = objRegistrationBL.ExistingGuest(objRegistrationOL);
        if (dtEGuestDetails != null && dtEGuestDetails.Rows.Count > 0 && string.IsNullOrEmpty(Request.QueryString["ID"]))
        {
            txtFName.Text = Convert.ToString(dtEGuestDetails.Rows[0]["FName"]);
            txtLName.Text = Convert.ToString(dtEGuestDetails.Rows[0]["LName"]);
            txtEmailID.Text = Convert.ToString(dtEGuestDetails.Rows[0]["EmailID"]);
            txtMobNo.Text = Convert.ToString(dtEGuestDetails.Rows[0]["Phone1"]);
            txtFName.ReadOnly = true;
            txtLName.ReadOnly = true;
            txtEmailID.ReadOnly = true;
            hdnClientRegisterYN.Value = Convert.ToString(dtEGuestDetails.Rows[0]["ClientRegisteredYN"]);
            hdnCCRegisterYN.Value = Convert.ToString(dtEGuestDetails.Rows[0]["CCRegisteredYN"]);

            ViewState["ClientIndivId"] = Convert.ToString(dtEGuestDetails.Rows[0]["ClientCoIndivID"]);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Mobile No. and Employee Code are already registered!');", true);
            Save.Enabled = false;
            btnModify.Enabled = true;
        }
        else
        {


            objRegistrationOL.Status = 0;
            DataTable dtGetRegistration = new DataTable();

            objRegistrationOL.FirstName = txtFName.Text;
            objRegistrationOL.LastName = txtLName.Text;
            objRegistrationOL.MobileNo = txtMobNo.Text;
            objRegistrationOL.EmailId = txtEmailID.Text;
            objRegistrationOL.Gender = ddlGender.SelectedItem.Value;

            objRegistrationOL = objRegistrationBL.UpdateIndivInfo(objRegistrationOL);
            if (objRegistrationOL.Status > 0)
            {
                objRegistrationOL.ClientIndivId = Convert.ToInt32(ViewState["ClientIndivId"]);
                dtGetRegistration = objRegistrationBL.GetRegistrationStatus(objRegistrationOL);

                hdnClientRegisterYN.Value = Convert.ToString(Convert.ToInt32(dtGetRegistration.Rows[0]["ClientRegisteredYN"]));
                hdnCCRegisterYN.Value = Convert.ToString(Convert.ToInt32(dtGetRegistration.Rows[0]["CCRegisteredYN"]));


                ConsolidateRegistrationCall(objRegistrationOL, false); //Registration Proccess

                /*
                string Mode = "", CCMode = "";
                string CreateModifyGuest = "";
                if (Convert.ToInt32(dtGetRegistration.Rows[0]["ClientRegisteredYN"]) == 0)
                {
                    Mode = "ACD";
                    CCMode = "ACC";
                    CreateModifyGuest = "A";
                }
                else if (Convert.ToInt32(dtGetRegistration.Rows[0]["ClientRegisteredYN"]) == 1)
                {
                    if (Convert.ToInt32(dtGetRegistration.Rows[0]["CCRegisteredYN"]) == 1)
                    {
                        CCMode = "UCC";
                        CreateModifyGuest = "U";
                    }
                    else
                    {
                        CCMode = "ACC";
                        CreateModifyGuest = "A";
                    }
                    Mode = "UCD";
                }

                //New Registration Process start
                if (objRegistrationOL.PaymentTerms == "CC" || hdnPaymentTerms.Value == "CC" || Convert.ToBoolean(Request.QueryString["forceSaveCard"]) == true)
                {
                    string ResponseStatus = Registration(objRegistrationOL, Mode, CreateModifyGuest);
                    //if (objRegistrationOL.ClientCoId == 1171)
                    if (objRegistrationOL.isPaymateCorporateModule)
                    {
                        Response.Write(ResponseStatus);
                        Response.End();
                    }
                    string[] Response1 = ResponseStatus.Split('.');
                    string staus = Response1[0].ToString();
                    string[] splitStatus = staus.Split('|');
                    string splitCode = splitStatus[0].ToString();
                    string splitMessage = splitStatus[1].ToString();
                    if (splitCode == "400")
                    {
                        objRegistrationOL.Status = 0;
                        objRegistrationOL = objRegistrationBL.UpdateRegistrationStatus(objRegistrationOL);
                        //string Status = "ACC";
                        string returnvalue;
                        returnvalue = CCRegistration(CCMode); //Code to redirect to Credit Card Registration
                        Response.Write(returnvalue);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Information Updated..');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Error in Updating Information at Paymate.');", true);
                        btnModify.Enabled = true;
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Information Updated..');", true);
                }
               */
            }
        }
    }

    public string URLEncrypt(string strencrypt)
    {
        string cipher = "";
        DESCryptoServiceProvider des = new DESCryptoServiceProvider();
        byte[] key = ASCIIEncoding.ASCII.GetBytes("Paymate1");
        byte[] iv = ASCIIEncoding.ASCII.GetBytes("Paymate1");
        try
        {
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, iv),
            CryptoStreamMode.Write);
            StreamWriter sw = new StreamWriter(cs);
            sw.Write(strencrypt);
            sw.Flush();
            cs.FlushFinalBlock();
            sw.Flush();
            byte[] hash = ms.GetBuffer();
            for (int i = 0; i < ms.Length; i++)
                cipher += BitConverter.ToString(hash, i, 1);
            sw.Close();
            cs.Close();
            ms.Close();
        }
        catch (Exception ex)
        {
            cipher = ex.Message;
        }
        return cipher;
    }

    protected void btnregister_Click(object sender, EventArgs e)
    {
        objRegistrationOL = new OL_Registration();
        objRegistrationBL = new BL_Registration();
        objRegistrationOL = GetControlsValues(objRegistrationOL);

        if (!string.IsNullOrEmpty((Request.QueryString["ID"])))
        {
            objRegistrationOL.ClientIndivId = Convert.ToInt32(Request.QueryString["ID"]);
        }

        if (!string.IsNullOrEmpty(ViewState["ClientIndivId"].ToString()))
        {
            objRegistrationOL.ClientIndivId = Convert.ToInt32(ViewState["ClientIndivId"]);
        }

        ConsolidateRegistrationCall(objRegistrationOL, true); //Registration Proccess
    }

    public void ConsolidateRegistrationCall(OL_Registration objRegistrationOL, bool RegisterButtonYN)
    {
        //string status = "";

        string Mode = "", CCMode = "";
        string CreateModifyGuest = "";
        bool ClientRegisterYN = false; bool CCRegisterYN = false;
        if (hdnClientRegisterYN.Value == "1")
        {
            ClientRegisterYN = true;
        }
        if (hdnCCRegisterYN.Value == "1")
        {
            CCRegisterYN = true;
        }

        //if (Convert.ToInt32(dtGetRegistration.Rows[0]["ClientRegisteredYN"]) == 0)
        if (!ClientRegisterYN)
        {
            Mode = "ACD";
            CCMode = "ACC";
            CreateModifyGuest = "A";
        }
        //else if (Convert.ToInt32(dtGetRegistration.Rows[0]["ClientRegisteredYN"]) == 1)
        else if (ClientRegisterYN)
        {
            //if (Convert.ToInt32(dtGetRegistration.Rows[0]["CCRegisteredYN"]) == 1)
            if(CCRegisterYN)
            {
                CCMode = "UCC";
                CreateModifyGuest = "U";
            }
            else
            {
                CCMode = "ACC";
                CreateModifyGuest = "A";
            }
            Mode = "UCD";
        }

        if (ClientRegisterYN && CCRegisterYN && !Convert.ToBoolean(hdnisPaymateCorporateModule.Value))
        {
            OldRegistrationProcessYN.Value = "1";
        }

        bool AutoRedirectYN = false;
        if (OldRegistrationProcessYN.Value == "1" || RegisterButtonYN == true || Convert.ToBoolean(Request.QueryString["forceSaveCard"]) == true)
        {
            AutoRedirectYN = true;
        }
        //else
        //{ 
        
        //}

        //New Registration Process start
        //if (objRegistrationOL.PaymentTerms == "CC" || hdnPaymentTerms.Value == "CC" || Convert.ToBoolean(Request.QueryString["forceSaveCard"]) == true)
        //if (objRegistrationOL.PaymentTerms == "CC" || hdnPaymentTerms.Value == "CC" || AutoRedirectYN == true)
        if((objRegistrationOL.isPaymateCorporateModule || OldRegistrationProcessYN.Value == "1") && AutoRedirectYN == true)
        {
            if (!CCRegisterYN)
            {
                
                string ResponseStatus = Registration(objRegistrationOL, Mode, CreateModifyGuest);

                if (objRegistrationOL.isPaymateCorporateModule)
                {
                    Response.Write(ResponseStatus);
                    Response.End();
                }
                string[] Response1 = ResponseStatus.Split('.');
                string staus = Response1[0].ToString();
                string[] splitStatus = staus.Split('|');
                string splitCode = splitStatus[0].ToString();
                string splitMessage = splitStatus[1].ToString();
                if (splitCode == "400")
                {
                    objRegistrationOL.Status = 0;
                    objRegistrationOL = objRegistrationBL.UpdateRegistrationStatus(objRegistrationOL);
                    //string Status = "ACC";
                    string returnvalue;
                    returnvalue = CCRegistration(CCMode); //Code to redirect to Credit Card Registration
                    Response.Write(returnvalue);
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Information Updated..');", true);
                }
                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Error in Updating Information at Paymate.');", true);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('" + splitMessage + "');", true);
                    btnModify.Enabled = true;
                }
            }
            else
            {
                string returnvalue1;
                returnvalue1 = CCRegistration(CCMode); //Code to redirect to Credit Card Registration
                Response.Write(returnvalue1);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Information Updated..');", true);
            }
        }
        else
        {
            if (!objRegistrationOL.isPaymateCorporateModule)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "javascript:alert('Information Updated..');", true);
            }
            //Reset();
            //if (btnregister.Visible == false)
            //{
            //    btnBookNow.Visible = true;
            //}
            //Response.Redirect("RegisterStatus.aspx");
        }
        //Reset();
        //return status;
    }

    protected void btnBookNow_Click(object sender, EventArgs e)
    {
        Response.Redirect("https://corporate.carzonrent.com/CorporateSite/Corporatelogin.asp");
    }
}