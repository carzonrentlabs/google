﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UploadGuestDetails.aspx.cs" Inherits="GuestProfile_UploadGuestDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>::Guest Registration::</title>
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function goBack() {
            window.history.go(-1)
        }
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8) {
                keychar = String.fromCharCode(key);
            }
            if (key == 13) {
                key = 8;
                keychar = String.fromCharCode(key);
            }
            return reg.test(keychar);
        }
    </script>

    <script type="text/javascript">
        function reloadPage() {
            window.location.reload()
        }
</script>
    <style type="text/css">
        #form1
        {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; padding-left: 60px;
            padding-bottom: 20px; padding-top: 10px;">
            <tr>
                <td align="left" style="height: 122px">
                    <img src="../Images/logo_carzonret.gif" alt="" />&nbsp;
                </td>
            </tr>
        </table>
        <table border="2" cellpadding="0" cellspacing="0" style="width: 100%; border-width: medium;"
            align="center">
            <tr>
                <td style="font-weight: bold; font-size: larger; line-height: 30pt; font-family: Calibri;
                    text-align: center; height: 40px; color: Red; font-size: large; border-bottom-width: medium;
                    background-color: #FBF18C">
                    Upload Bulk Guest Details &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td colspan="3">
                                
                            </td>
                        </tr>

                         <tr>
                            <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;
                                width: 40%;">
                               </td>
                            <td style="width: 2%;">
                                &nbsp;</td>
                            <td align="left">
                                 <b><a href="../Files/UploadGuestData.xlsx" target ="_blank">Download Sample Format</a></b> <br />
                                <asp:Label ID="lblMsgCCC" runat="server" 
                                     Text="Note : Client having credit card will not allow to enter in system." 
                                     style="font-family: Verdana; font-size: small"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;
                                width: 40%;">
                                <asp:Label ID="Label1" runat="server" Text="Send Mail:" Font-Names="Verdana" 
                                    Font-Size="X-Small"></asp:Label></td>
                            <td style="width: 2%;">
                                &nbsp;</td>
                            <td align="left">
                                <asp:CheckBox ID="ChkSendmail" runat="server" Text="" 
                                    oncheckedchanged="ChkSendmail_CheckedChanged" AutoPostBack="True" 
                                    Font-Names="Verdana" Font-Size="X-Small" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;
                                width: 40%;">
                                <asp:Label ID="lblClientName" runat="server" Text="Client Name:" 
                                    Font-Names="Verdana" Font-Size="X-Small"></asp:Label>
                            </td>
                            <td style="width: 2%;">
                                &nbsp;
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlClientCoId" runat="server" Visible="true" 
                                    onselectedindexchanged="ddlClientCoId_SelectedIndexChanged" 
                                    AutoPostBack="True" Font-Names="Verdana" Font-Size="X-Small" 
                                    Width="300px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    InitialValue="0" ControlToValidate="ddlClientCoId" ValidationGroup="Validate"
                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                &nbsp;<asp:Label ID="lblClientCoAddID" runat="server" Visible="False"></asp:Label>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;
                                width: 40%;">
                                 <asp:Label ID="lblSelectFile" runat="server" Text="Select file to upload:" 
                                     Font-Names="Verdana" Font-Size="X-Small"></asp:Label>
                                </td>
                            <td style="width: 2%;">
                                &nbsp;</td>
                            <td align="left">
                                <asp:FileUpload ID="upldFile" runat="server" Font-Names="Verdana" 
                                    Font-Size="X-Small" Height="20px" />
                            &nbsp;<asp:Button ID="btnUpload" runat="server" Font-Names="Verdana" 
                                    Font-Size="X-Small" Text="Upload" onclick="btnUpload_Click" />

                                    &nbsp;<asp:Button ID="btnClear" runat="server" Text="Clear" 
                                    onclick="btnClear_Click" Font-Names="Verdana" Font-Size="X-Small" 
        />

        
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                &nbsp; <asp:Label ID="lblMsgCC" runat="server" Text=""></asp:Label>
                                </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" 
                                style="font-size: larger; line-height: 30pt; font-family: Calibri;
                                text-align: center; height: 40px; color: Red; font-size: large; border-bottom-width: medium;">
                                &nbsp;
                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                                <asp:Label ID="lblGv" runat="server" Font-Names="Verdana" Font-Size="Small" 
                                    ForeColor="Red" Visible="False">Uploaded Guests</asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                    ControlToValidate="upldFile" Display="Dynamic" 
                                    ErrorMessage="Select file to upload" Font-Names="Verdana" Font-Size="Small"></asp:RequiredFieldValidator>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" 
                                style="font-size: larger; line-height: 30pt; font-family: Calibri;
                                text-align: center; height: 40px; color: Red; font-size: large; border-bottom-width: medium;">
                                <asp:Panel ID="pnlGV" runat="server">
                               
                                
                                    <div style="width: 100%; height: 250px; overflow: scroll">
                                    <asp:GridView ID="Gvguest" runat="server" BorderColor="Black" 
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="0" Font-Names="Verdana" 
                                    Font-Size="XX-Small" ForeColor="#333333" HorizontalAlign="Center" 
                                    Width="100%" PageSize="100">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="5%" 
                                            HeaderText="Sr No">
                                            <ItemTemplate>
                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="table_04" HorizontalAlign="Left" />
                                            <ItemStyle CssClass="table_02" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                </asp:GridView></div> 
                                 </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" 
                                style="font-size: larger; line-height: 30pt; font-family: Calibri;
                                text-align: center; height: 40px; color: Red; font-size: large; border-bottom-width: medium;">
                                
                                 <asp:Panel ID="pnlSavedGV" runat="server">
                               
                                
                                    <div style="width: 100%; height: 250px; overflow: scroll">
                                    <asp:GridView ID="GvSavedGuests" runat="server" AutoGenerateColumns="False" 
                                    BorderColor="#404040" BorderStyle="Solid" BorderWidth="1px" CellPadding="0" 
                                    EmptyDataText="No Records Found" Font-Names="Verdana" Font-Size="XX-Small" 
                                    ForeColor="#333333" SkinID="SandAndSkywithoutwidth" Width="100%" PageSize="100">
                                    <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sr No">
                                            <ItemTemplate>
                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="table_04" HorizontalAlign="Left" Width="5%" />
                                            <ItemStyle CssClass="table_02" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ClientCoId">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClientCoId" runat="server" Text='<%#Bind("ClientCoId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ClientCoAddId">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClientCoAddId" runat="server" 
                                                    Text='<%#Bind("ClientCoAddId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fname">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFname" runat="server" Text='<%#Bind("Fname") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mname">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMname" runat="server" Text='<%#Bind("Mname") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lname">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLname" runat="server" Text='<%#Bind("Lname") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phone1">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPhone1" runat="server" Text='<%#Bind("Phone1") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EmailId">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmailId" runat="server" Text='<%#Bind("EmailId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PaymentTerms">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentTerms" runat="server" 
                                                    Text='<%#Bind("PaymentTerms") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRemarks" runat="server" Text='<%#Bind("Remarks") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Active">
                                            <ItemTemplate>
                                                <asp:Label ID="lblActive" runat="server" Text='<%#Bind("Active") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="IsVIP">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIsVIP" runat="server" Text='<%#Bind("IsVIP") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PreAuthNotRequire">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPreAuthNotRequire" runat="server" 
                                                    Text='<%#Bind("PreAuthNotRequire") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="FacilitatorId">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFacilitatorId" runat="server" 
                                                    Text='<%#Bind("FacilitatorId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Gender">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGender" runat="server" Text='<%#Bind("Gender") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EmpCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpCode" runat="server" Text='<%#Bind("EmpCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnStatus" runat="server" 
                                                    Value='<%#Bind("UploadStatus") %>' />
                                                     <asp:HiddenField ID="hdnPass" runat="server" 
                                                    Value='<%#Bind("Pass") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                </asp:GridView>
                                </div>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:Button ID="btnSend" runat="server" Text="Send mail" onclick="btnSend_Click" />
    &nbsp;<%--<input type="button" value="Reload Page" onClick="document.location.reload(true)">--%></form>
</body>
</html>
