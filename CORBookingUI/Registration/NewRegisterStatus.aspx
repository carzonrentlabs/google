﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewRegisterStatus.aspx.cs"
    Inherits="NewRegisterStatus" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <script language="Javascript" type="text/javascript" src="../Scripts/CommonScript.js"></script>
    <script language="javascript" type="text/javascript">
        function goBack() {
            window.history.go(-1)
        }
    </script>
    <link href="../Styles/HertzInt.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="frmregister" name="frmregister" method="post" runat="server">
    <script language="Javascript" type="text/javascript" src="../Scripts/CommonScript.js"></script>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 800px; padding-left: 60px;
        padding-bottom: 40px;">
        <tr>
            <td align="left">
                <img src="../Images/logo_carzonret.gif" alt="" />&nbsp;
            </td>
        </tr>
    </table>
    <table border="2" cellpadding="0" cellspacing="0" style="width: 65%; border-width: medium;"
        align="center">
        <tr>
            <td style="font-weight: bold; font-size: larger; line-height: 30pt; font-family: Calibri;
                text-align: center; height: 40px; color: Red; font-size: large; border-bottom-width: medium;
                background-color: #A4A4A4">
                Return Result
            </td>
        </tr>
        <tr>
            <td align="center">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr style="padding-top: 20px;">
                        <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                            Employee Name:
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:Label ID="lblempname" runat="server"></asp:Label>
                            <asp:HiddenField ID="hdnTravellerId" runat="server" />
                            <asp:HiddenField ID="hdnSysRegCode" runat="server" />
                        </td>
                        <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                            Email ID:
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:Label ID="lblemail" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                    <tr style="padding-top: 20px;">
                        <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                            Mobile:
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:Label ID="lblmobile" runat="server"></asp:Label>
                        </td>
                        <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                            Card Type:
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:Label ID="lblcartype" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                            Credit Card No:
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:Label ID="txtccno" runat="server" MaxLength="12"></asp:Label>
                        </td>
                        <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                            Action:
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:Label ID="txtaction" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                            Address:
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:Label ID="lblAddress" runat="server" MaxLength="12"></asp:Label>
                        </td>
                        <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td align="left">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: center">
                            <asp:Button ID="btnClose" Text="Close Window" Font-Bold="True" Font-Size="Medium"
                                Font-Names="Calibri" BorderStyle="Groove" ForeColor="Red" runat="server" OnClick="btnClose_Click" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lblMsg" runat="server" Font-Bold="True" ForeColor="Green"></asp:Label>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
