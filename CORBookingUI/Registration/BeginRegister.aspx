﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BeginRegister.aspx.cs" Inherits="BeginRegister" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>::Guest Registration::</title>
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function goBack() {
            window.history.go(-1)
        }
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8) {
                keychar = String.fromCharCode(key);
            }
            if (key == 13) {
                key = 8;
                keychar = String.fromCharCode(key);
            }
            return reg.test(keychar);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; padding-left: 60px;
            padding-bottom: 20px; padding-top: 10px;">
            <tr>
                <td align="left" style="height: 122px">
                    <img src="../Images/logo_carzonret.gif" alt="" />&nbsp;
                </td>
            </tr>
        </table>
        <table border="2" cellpadding="0" cellspacing="0" style="width: 70%; border-width: medium;"
            align="center">
            <tr>
                <td style="font-weight: bold; font-size: larger; line-height: 30pt; font-family: Calibri;
                    text-align: center; height: 40px; color: Red; font-size: large; border-bottom-width: medium;
                    background-color: #FBF18C">
                    Search Email &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td colspan="6">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;
                                width: 40%;">
                                Official Email ID:
                            </td>
                            <td style="width: 2%;">
                                &nbsp;
                            </td>
                            <td align="left" colspan="4">
                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                &nbsp;&nbsp;
                                <%--</td>
                            <td align="left" style="font-weight: bold; font-family: Calibri; font-size: small;"
                                colspan="3">--%>
                                <asp:Button ID="btnSearch" Text="Begin Registration" runat="server" Font-Bold="True"
                                    Font-Size="Medium" Font-Names="Calibri" ForeColor="White" OnClick="btnSearch_Click"
                                    ValidationGroup="Search" BackColor="CadetBlue" />
                                &nbsp;&nbsp;
                                <asp:regularexpressionvalidator id="regexEmailValid" runat="server" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    controltovalidate="txtEmail" errormessage="Invalid EmailID" xmlns:asp="#unknown"
                                    validationgroup="Search" forecolor="Red">
                                </asp:regularexpressionvalidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;
                                width: 40%;">
                                <asp:Label ID="lblClientName" runat="server" Text="Client Name:" Visible="false"></asp:Label>
                            </td>
                            <td style="width: 2%;">
                                &nbsp;
                            </td>
                            <td align="left" colspan="4">
                                <asp:DropDownList ID="ddlclientname" runat="server" Visible="false">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    InitialValue="0" ControlToValidate="ddlclientname" ValidationGroup="Validate"
                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                &nbsp;&nbsp;
                                <%--</td>
                            <td align="left" style="font-weight: bold; font-family: Calibri; font-size: small;"
                                colspan="3">--%>
                                <asp:Button ID="btnRedirect" Text="Register Guest" runat="server" Font-Bold="True"
                                    Font-Size="Medium" Font-Names="Calibri" BorderStyle="Groove" ForeColor="White"
                                    OnClick="btnRedirect_Click" Visible="false" ValidationGroup="Validate" BackColor="CadetBlue" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" style="font-size: larger; line-height: 30pt; font-family: Calibri;
                                text-align: center; height: 40px; color: Red; font-size: large; border-bottom-width: medium;">
                                &nbsp;
                                <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
                                <br />
                                <asp:HyperLink ID="hllink" NavigateUrl="http://corporate.carzonrent.com/CorporateSite/CompanyRegistration.asp"
                                    runat="server" Visible="false" Text="Company not registered - Create new Company"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
