﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GuestRegistration.aspx.cs"
    Inherits="Registration_GuestRegistration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>::Guest Registration::</title>
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function goBack() {
            window.history.go(-1)
        }
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8) {
                keychar = String.fromCharCode(key);
            }
            if (key == 13) {
                key = 8;
                keychar = String.fromCharCode(key);
            }
            return reg.test(keychar);
        }
        $(function () {
            $("#<%=Save.ClientID%>").click(function () {

                if ($("#<%=chkIndia.ClientID%>").is(':checked') && $("#<%=txtMobNo.ClientID%>").val().length > 10) {
                    alert("For India only 10 digits is valid.")
                    return false;
                }
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; padding-left: 60px;
            padding-bottom: 20px; padding-top: 10px;">
            <tr>
                <td align="left" style="height: 122px">
                    <img src="../Images/logo_carzonret.gif" />&nbsp;
                </td>
            </tr>
        </table>
        <table border="2" cellpadding="0" cellspacing="0" style="width: 70%; border-width: medium;"
            align="center">
            <tr>
                <td style="font-weight: bold; font-size: larger; line-height: 30pt; font-family: Calibri;
                    text-align: center; height: 40px; color: Red; font-size: large; border-bottom-width: medium;
                    background-color: #FBF18C">
                    Add Guest Profile / CC Details
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <div id="trGuestRegistration" runat="server">
                            <tr>
                                <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;
                                    padding-top: 20px;">
                                    First Name:
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left" style="padding-top: 20px;">
                                    <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="R1" runat="server" ErrorMessage="*" ControlToValidate="txtFName"></asp:RequiredFieldValidator>
                                </td>
                                <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;
                                    padding-top: 20px;">
                                    Last Name:
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left" style="padding-top: 20px;">
                                    <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="R2" runat="server" ErrorMessage="*" ControlToValidate="txtLName"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                                    Mobile No:
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtMobNo" runat="server" MaxLength="16" onkeypress="return allownumbers(event);"></asp:TextBox>
                                    <asp:CheckBox ID="chkIndia" runat="server" Checked="true" />India
                                    <asp:RequiredFieldValidator ID="R3" runat="server" ErrorMessage="*" ControlToValidate="txtMobNo"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regMobileNumber" runat="server" ErrorMessage="InValid No."
                                        ValidationExpression="([0-9]){10,15}" ControlToValidate="txtMobNo"></asp:RegularExpressionValidator>
                                </td>
                                <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                                    EmailID:
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtEmailID" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="R4" runat="server" ErrorMessage="*" ControlToValidate="txtEmailID"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regEmailValidator" runat="server" ErrorMessage="InValid ID"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmailID"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                                    &nbsp; Gender:
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlGender" name="ddlGender" runat="server">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Male" Value="M"></asp:ListItem>
                                        <asp:ListItem Text="Female" Value="F"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                        InitialValue="" ControlToValidate="ddlGender"></asp:RequiredFieldValidator>
                                    &nbsp;
                                </td>
                                <td align="right" style="font-weight: bold; font-family: Calibri; font-size: small;">
                                    &nbsp;
                                    <asp:Label ID="lblempcode" Visible="false" runat="server" Text="Employee Code:"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td align="left">
                                    &nbsp;
                                    <asp:TextBox ID="txtEmpCode" Visible="false" runat="server" MaxLength="20"></asp:TextBox>
                                </td>
                            </tr>
                        </div>
                        <div>
                            <tr>
                                <td colspan="6" align="center" style="font-weight: bold; font-family: Calibri; font-size: small;">
                                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </div>
                        <tr>
                            <td colspan="6">
                                &nbsp;
                                <asp:HiddenField ID="hdnisPaymateCorporateModule" Value="" runat="server" />
                                <asp:HiddenField ID="hdnPaymentTerms" Value="" runat="server" />
                                <asp:HiddenField ID="hdnClientCoID" Value="" runat="server" />
                                <asp:HiddenField ID="OldRegistrationProcessYN" Value="" runat="server" />
                                <asp:HiddenField ID="hdnClientRegisterYN" Value="" runat="server" />
                                <asp:HiddenField ID="hdnCCRegisterYN" Value="" runat="server" />
                                <asp:HiddenField ID="CustOrgRegId" Value="" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align: center">
                                <asp:Button ID="Save" Text="Save" runat="server" OnClick="Save_Click" Font-Bold="True"
                                    Font-Size="Medium" Font-Names="Calibri" BorderStyle="Groove" ForeColor="White"
                                    BackColor="CadetBlue" />&nbsp;&nbsp;
                                <asp:Button ID="btnModify" runat="server" Text="Modify" Font-Bold="True" Font-Size="Medium"
                                    Font-Names="Calibri" BorderStyle="Groove" ForeColor="White" OnClick="btnModify_Click"
                                    BackColor="CadetBlue" />
                                &nbsp;&nbsp;
                                <asp:Button ID="btnregister" runat="server" Text="Save Credit Card" Font-Bold="True"
                                    Font-Size="Medium" Font-Names="Calibri" BorderStyle="Groove" ForeColor="White"
                                    BackColor="CadetBlue" OnClick="btnregister_Click" Visible="false" />
                                &nbsp;&nbsp;
                                <asp:Button ID="btnBookNow" Text="Login" Font-Bold="True" Font-Size="Medium" Font-Names="Calibri"
                                    BorderStyle="Groove" ForeColor="Red" runat="server" OnClick="btnBookNow_Click"
                                    Visible="false" />
                                <%--&nbsp;&nbsp;
                                <asp:Button ID="btnClose" runat="server" Text="Close Window" Font-Bold="True" Font-Size="Medium"
                                    Font-Names="Calibri" BorderStyle="Groove" ForeColor="White" BackColor="CadetBlue" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
