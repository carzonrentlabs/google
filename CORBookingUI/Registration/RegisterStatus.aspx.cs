﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.BusinessFramework;
using System.Text;
using System.Net;
using System.IO;
using ReportingTool;

public partial class Registration_RegisterStatus : System.Web.UI.Page
{
    OL_Registration objRegistrationOL = null;
    BL_Registration objRegistrationBL = null;
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                string MailStatus = "";

                objRegistrationOL = new OL_Registration();
                objRegistrationBL = new BL_Registration();
                txtMessage.Text = Request["ErrorMessage"];
                txtrmode.Text = Request["RequestMode"];
                txtcode.Text = Request["ErrorCode"];
                txtClientID.Text = Request["CustOrgRegId"];
                if (Request["ErrorCode"] == "API15" || Request["ErrorCode"] == "API16") // || Request["ErrorCode"] == "API14")
                {
                    objRegistrationOL.Status = 0;
                    objRegistrationOL.ClientIndivId = Convert.ToInt32(Request["CustOrgRegId"]);
                    objRegistrationOL = objRegistrationBL.UpdateCCStatus(objRegistrationOL);
                    if (objRegistrationOL.Status == 1)
                    {
                        objRegistrationOL = objRegistrationBL.SendRegistrationMailDetails(objRegistrationOL);
                        hdnClientID.Value = objRegistrationOL.ClientCoId.ToString();
                        if (!string.IsNullOrEmpty(objRegistrationOL.EmailId.ToString()) && !string.IsNullOrEmpty(objRegistrationOL.Password.ToString()))
                        {
                            MailStatus = objAdmin.SendMail_RegisteredGuest(objRegistrationOL);
                        }
                    }

                    btnRegister.Visible = false;
                    btnBookNow.Visible = true;
                    lblMsg.Text = "Congratulations<br />You are successfully registered.<br />" + "Your login details are sent to your registered E-mail id";
                }
                else
                {
                    btnRegister.Visible = true;
                    btnBookNow.Visible = false;
                }
                DataTable dtStatus = new DataTable();
                dtStatus = objRegistrationBL.GetRegistrationStatus(objRegistrationOL);
                ViewState["EmailID"] = dtStatus.Rows[0]["EmailID"];
            }
            catch (Exception ex)
            {
                ErrorLogClass.LogErrorToLogFile(ex, "an error occured");
            }
        }
    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        Response.Redirect("GuestRegistration.aspx?Email=" + ViewState["EmailID"]);
    }
    protected void btnBookNow_Click(object sender, EventArgs e)
    {
        if (hdnClientID.Value == "2806")
        {
            Response.Redirect("../Login.aspx");
        }
        else
        {
            Response.Redirect("https://corporate.carzonrent.com/CorporateSite/Corporatelogin.asp");
        }
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
        ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.opener.location.reload();", true);
    }
}