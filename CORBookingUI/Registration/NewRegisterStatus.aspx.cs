﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.BusinessFramework;
using System.Text;
using System.Net;
using System.IO;
using ReportingTool;

public partial class NewRegisterStatus : System.Web.UI.Page
{
    OL_Registration objRegistrationOL = null;
    BL_Registration objRegistrationBL = null;
    clsAdmin objAdmin = new clsAdmin();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                string MailStatus = "";

                objRegistrationOL = new OL_Registration();
                objRegistrationBL = new BL_Registration();

                hdnTravellerId.Value = Request["TravellerId"];
                hdnSysRegCode.Value = Request["SysRegCode"];
                lblempname.Text = Request["TravellerName"];
                lblemail.Text = Request["EmailId"];
                lblmobile.Text = Request["MobileNo"];
                lblcartype.Text = Request["CardType"];
                txtccno.Text = Request["CreditCardNo"];
                txtaction.Text = Request["Action"];
                lblAddress.Text = Request["Address"];

                objRegistrationOL.Status = 0;
                objRegistrationOL.ClientIndivId = Convert.ToInt32(Request["TravellerId"]);
                objRegistrationOL.FirstName = Convert.ToString(Request["TravellerName"]);
                objRegistrationOL.LastName = "";
                objRegistrationOL.EmailId = Convert.ToString(Request["EmailId"]);
                objRegistrationOL.MobileNo = Convert.ToString(Request["MobileNo"]);
                objRegistrationOL.CCNo = Convert.ToString(Request["CreditCardNo"]);
                try
                {
                    objRegistrationOL.Address = Convert.ToString(Request["Address"]).Trim();
                }
                catch(Exception ex)
                {
                    objRegistrationOL.Address = "";
                }

                if (Convert.ToString(Request["CardType"]).ToUpper() == "AMEX")
                {
                    objRegistrationOL.CCType = 2;
                }
                if (Convert.ToString(Request["CardType"]).ToUpper() == "MASTER")
                {
                    objRegistrationOL.CCType = 3;
                }
                if (Convert.ToString(Request["CardType"]).ToUpper() == "VISA")
                {
                    objRegistrationOL.CCType = 1;
                }

                objRegistrationOL.Action = Convert.ToString(Request["Action"]);

                objRegistrationOL = objRegistrationBL.UpdateCCStatus_NewProcess(objRegistrationOL);
                //if (objRegistrationOL.Status == 1)
                //{
                //    objRegistrationOL = objRegistrationBL.SendRegistrationMailDetails(objRegistrationOL);
                //    if (!string.IsNullOrEmpty(objRegistrationOL.EmailId.ToString()) && !string.IsNullOrEmpty(objRegistrationOL.Password.ToString()))
                //    {
                //        SendMail_RegisteredGuest(objRegistrationOL);
                //    }
                //}
                //MailStatus = objAdmin.SendMail_RegisteredGuest(objRegistrationOL);

                //lblMsg.Text = "Congratulations<br />You are successfully registered.<br />" + "Your login details are sent to your registered E-mail id";
                lblMsg.Text = "Thanks for registering your card details, Welcome to Carzonrent";
                DataTable dtStatus = new DataTable();
                dtStatus = objRegistrationBL.GetRegistrationStatus(objRegistrationOL);
                ViewState["EmailID"] = dtStatus.Rows[0]["EmailID"];
            }
            catch (Exception ex)
            {
                lblMsg.Text = "Error in return status : " + ex.ToString();
                ErrorLogClass.LogErrorToLogFile(ex, "an error occured");
            }
        }
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
        ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.opener.location.reload();", true);
    }
}