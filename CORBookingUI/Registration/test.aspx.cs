﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.BusinessFramework;
using System.Drawing;
using System.Security.Cryptography;
using System.IO;
using System.Text;

public partial class Registration_test : System.Web.UI.Page
{
    BL_Booking objBookingBL = new BL_Booking();
    OL_Booking objBookingOL = new OL_Booking();
    OL_ErrorLogDetails objErrorLogDetailsOL = new OL_ErrorLogDetails();


    protected void Page_Load(object sender, EventArgs e)
    {
        //OldCCRegistration();
        //CCRegistrationAmexMaster();
        //string Status = EmailValidation("rahul@carzonrent.com", "abc@carzonrent.com"); //success
        //string Status = EmailValidation("milind@india.ti.com", "abc@ti.com"); //success
        //string Status = EmailValidation("takehiko.tsuboi@carzonrent.abc.com", "abc@abc.com"); //success
        //string Status = EmailValidation("absdfd@abc.carzonrent.com", "abc@abc.com"); //Failure
        //string Status = EmailValidation("absdfd@accenture.com", "abc@accenture.co.uk"); //success
        string Status = EmailValidation("absdfd@in.ibm.com", "abc@ibm.com"); //success

        // @accenture.com - @accenture co.uk (ignore co)
        // @in ibm - @in ibm
        // 

        Response.Write(Status);
    }

    private string EmailValidation(string GuestEmailID, string RegisteredEmailID)
    {
        string Status = "Failure";
        //string EmailID = "techreports@carzonrent.com";

        try
        {
            string[] GuestEmailDetails = GuestEmailID.Split('@');
            string[] RegisteredEmailDetails = RegisteredEmailID.Split('@');

            if (GuestEmailDetails[1] == RegisteredEmailDetails[1])
            {
                Status = "Success";
            }
            else
            {
                string RegisteredEmailFinal = "", GuestDomainFinal = "";

                string[] RegisteredEmailSplit = RegisteredEmailDetails[1].Split('.');
                RegisteredEmailFinal = IgnoreLastDomainPart(RegisteredEmailFinal, RegisteredEmailSplit);

                if (GuestEmailDetails[1].Contains("."))
                {

                    string[] GuestDomainSplit = GuestEmailDetails[1].Split('.');
                    GuestDomainFinal = IgnoreLastDomainPart(GuestDomainFinal, GuestDomainSplit);

                    GuestDomainSplit = GuestDomainFinal.Split('.');

                    if (GuestDomainFinal == RegisteredEmailFinal)
                    {
                        Status = "Success";
                    }
                    else
                    {
                        if (GuestDomainSplit.Length > 2)
                        {
                            if (RegisteredEmailDetails[1].Contains(GuestDomainSplit[0]))
                            {
                                if (GuestDomainSplit[1] + "." + GuestDomainSplit[2] == RegisteredEmailDetails[1])
                                {
                                    Status = "Success";
                                }
                                else
                                {
                                    Status = "Failure";
                                }
                            }
                        }
                        else if (GuestDomainSplit.Length == 2)
                        {
                            if (GuestDomainSplit[1] == RegisteredEmailFinal)
                            {
                                Status = "Success";
                            }
                            else
                            {
                                Status = "Failure";
                            }
                        }
                        else
                        {
                            Status = "Failure";
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Status = ex.ToString(); //"Failure";
        }
        return Status;
    }

    private static string IgnoreLastDomainPart(string EmailFinal, string[] EmailSplit)
    {
        if (EmailSplit.Length > 1)
        {
            for (int i = 0; i <= EmailSplit.Length - 1; i++)
            {
                if (i != EmailSplit.Length - 1)
                {
                    if (EmailSplit[i].ToLower() != "co")
                    {
                        if (EmailFinal == "")
                        {
                            EmailFinal = EmailSplit[i];
                        }
                        else
                        {
                            EmailFinal = EmailFinal + "." + EmailSplit[i];
                        }
                    }
                }
            }
        }
        return EmailFinal;
    }


    private void CCRegistrationAmexMaster()
    {
        string url = "http://123.201.124.141/Beta/PMCorporate/RegisterTraveller.aspx";

        string EncryptedParameter = "";
        string SysRegCode = "", EmpName = "", EmailId = "", MobileNo = "", travellerid = "", Action = "";
        SysRegCode = "pOKuFx";
        EmpName = "Test";
        EmailId = "techreports@carzonrent.com";
        MobileNo = "9818107670";
        travellerid = "123456";

        Action = "U";

        EncryptedParameter = URLEncrypt("BD=" + travellerid + "|" + SysRegCode + "|" + EmpName + "|" + EmailId + "|" + MobileNo + "|" + Action);

        url = url + "?" + EncryptedParameter;
        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        sb.Append("<html><head></head><body>");
        sb.Append("<form name='form1' method='POST' action='" + url + "'>");
        sb.Append("</form>");
        sb.Append("<script language='javascript'>document.form1.submit();</script>");
        sb.Append("</body></html>");
        sb.ToString();

        Response.Write(sb.ToString());
    }

    private void OldCCRegistration()
    {
        lblMesaage.Visible = true;
        GeneralUtility.InitializeControls(Form);
        lblMesaage.Text = "invalid Merchant";
        lblMesaage.ForeColor = Color.Red;
        objErrorLogDetailsOL.errorSource = "test.aspx.cs";
        objErrorLogDetailsOL.ErrorMessage = "invalid Merchant";
        objErrorLogDetailsOL.errorBookingId = 6400259;
        objErrorLogDetailsOL.errorCreatedBy = 2359468;
        objErrorLogDetailsOL.errorBlock = "401";
        objBookingBL.SaveLogError(objErrorLogDetailsOL);
        int values = 1171;    //860; //2806
        Response.Redirect("GuestRegistration.aspx?ClientCoId=" + HttpUtility.UrlEncode(GeneralMethos.Encrypt(Convert.ToString(values))), false);
        //Response.Redirect("GuestRegistration.aspx?ClientCoId=" + values, false);
    }

    public string URLEncrypt(string strencrypt)
    {
        string cipher = "";
        DESCryptoServiceProvider des = new DESCryptoServiceProvider();
        byte[] key = ASCIIEncoding.ASCII.GetBytes("Paymate1");
        byte[] iv = ASCIIEncoding.ASCII.GetBytes("Paymate1");
        try
        {
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, iv),
            CryptoStreamMode.Write);
            StreamWriter sw = new StreamWriter(cs);
            sw.Write(strencrypt);
            sw.Flush();
            cs.FlushFinalBlock();
            sw.Flush();
            byte[] hash = ms.GetBuffer();
            for (int i = 0; i < ms.Length; i++)
                cipher += BitConverter.ToString(hash, i, 1);
            sw.Close();
            cs.Close();
            ms.Close();
        }
        catch (Exception ex)
        {
            cipher = ex.Message;
        }
        return cipher;
    }
    protected void btnencrypt_Click(object sender, EventArgs e)
    {
        if (ClientID.Text.Trim() == "")
        {
            lblMesaage.Text = "Please enter clientcoid.";
        }

        string encrypt = "";
        encrypt = URLEncrypt(ClientID.Text);

        lblMesaage.Text = encrypt;
    }
}