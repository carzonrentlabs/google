﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["ClientCoIndivID"] = null;
        Session.Abandon();
        if (Session["ClientCoIndivID"] == null || string.IsNullOrEmpty(Session["ClientCoIndivID"].ToString()))
        {
            Response.Redirect("~/Login.aspx");
        }
       
    }
}