﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/CORMaster.master" AutoEventWireup="true"
    CodeFile="GuestBookingDetails.aspx.cs" Inherits="GuestProfile_GuestBookingDetails" %>

<asp:Content ID="ContentPage" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        var GB_ROOT_DIR = '<%= this.ResolveClientUrl("~/greybox/")%>';
    </script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/AJS_fx.js") %>'></script>
    <script type="text/javascript" src='<%= this.ResolveClientUrl("~/greybox/gb_scripts.js") %>'></script>
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <link href="../DatePickercss/ui.all.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/ui.datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad(sender, args) {

            $("#<%=txtPickUpdate.ClientID%>").datepicker();
            $("#<%=txtPickupToDate.ClientID%>").datepicker();

            $("#liBooking").removeClass('active')
            $("#liBookingEdit").removeClass('active')
            $("#liBookingCancelDetails").addClass('active')
            $("#liBookingUsreProfile").removeClass('active')
            $("#liBookingCancelSearch").removeClass('active')

            $("#<%=btnGetBookingDetails.ClientID%>").click(function () {

                var strDatePickup = $("#<%=txtPickUpdate.ClientID%>").val();
                var strPickupDateTo = $("#<%=txtPickupToDate.ClientID%>").val();

                var PickupDate = new Date(strDatePickup.split("-")[2], strDatePickup.split("-")[0], strDatePickup.split("-")[1]);
                var PickupDateTo = new Date(strPickupDateTo.split("-")[2], strPickupDateTo.split("-")[0], strPickupDateTo.split("-")[1]);

                if ($("#<%=txtPickUpdate.ClientID%>").val() == "") {
                    alert("Select Pick Up date.")
                    $("#<%=txtPickUpdate.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtPickupToDate.ClientID%>").val() == "") {
                    alert("Select Drop off date.")
                    $("#<%=txtPickupToDate.ClientID%>").focus();
                    return false;
                }
                else if (Date.parse(strDatePickup) > Date.parse(strPickupDateTo)) {
                    alert("From  date should not be greater then to off date.");
                    return false;
                }

            });

            $("#<%=gvBooking.ClientID%> a[id*='lnkButtion']").click(function () {
           
                var caption = "Invoice";
                var row = this.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var linkId = this.id
                var hdBookingId = linkId.replace("lnkButtion", "hdBookingId")
                var bookingId = $("#" + hdBookingId).val();
              
                if (bookingId == 0) {
                    var url = "FileNotFound.aspx"
                }
                else {
                    //var url = "http://103.11.85.78/Upload/DutySlips/" + bookingId.toString() + ".pdf"
                    var url = "http://insta.carzonrent.com/Dutyslips/DSClosePrintCC.asp?BookingID=" + bookingId + "&WebPrint=1"
                    alert(url)
                }
                //return GB_showCenter(caption, url, 500, 700)
                //return GB_showCenter(caption, url, 400, 500, callback_fn);
                return GB_showFullScreen(caption, url)
            });

        }
        //Callback function
        function callback_fn() {
            alert(script_loaded);
        }
    </script>
    <div>
        <table class="booking_cor" cellpadding="1" cellspacing="1" border="0">
            <tr>
                <td class="bookings_h" colspan="6" align="center">
                    <b>Guest Booking Details</b>
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="left">
                    <b>Pick-up Form date</b>
                </td>
                <td class="left">
                    <asp:TextBox ID="txtPickUpdate" runat="server" Width="120px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rvf_PickpUpdate" runat="server" Font-Bold="True"
                        ControlToValidate="txtPickUpdate" ErrorMessage="*" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td class="left">
                    <b>Pick-up To date</b>
                </td>
                <td class="left">
                    <asp:TextBox ID="txtPickupToDate" runat="server" Width="120px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rf_PickupToDate" runat="server" Font-Bold="True"
                        ControlToValidate="txtPickupToDate" ErrorMessage="*" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Button ID="btnGetBookingDetails" runat="server" Text="Get Booking" OnClick="btnGetBookingDetails_Click"
                        Width="120px" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <asp:GridView ID="gvBooking" runat="server" AutoGenerateColumns="false" CellPadding="5"
            CellSpacing="5" OnRowDataBound="gvBooking_RowDataBound">
            <Columns>
                <asp:BoundField DataField="BookingID" HeaderText="Booking ID" />
                <asp:BoundField DataField="GuestName" HeaderText="Guest Name" />
                <asp:BoundField DataField="CityName" HeaderText="City Name" />
                <asp:BoundField DataField="PickUpDate" HeaderText="Pick UpDate" />
                <asp:BoundField DataField="PickupTime" HeaderText="Pickup Time" />
                <asp:BoundField DataField="DropOffDate" HeaderText="DropOffDate" />
                <asp:BoundField DataField="DropOffTime" HeaderText="DropOffTime" />
                <asp:BoundField DataField="ClientCoName" HeaderText="Company Name" />
                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                <asp:BoundField DataField="PickupAddress" HeaderText="Pickup Address" />
                <asp:TemplateField HeaderText="Booking Status">
                    <ItemTemplate>
                        <asp:Label ID="lblBookingStatus" runat="server" Text='<%#Eval("BookingStatus") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Print Invoice">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkButtion" runat="server" Text=""></asp:LinkButton>
                        <asp:HiddenField ID="hdBookingId" runat="server" Value='<%#Eval("BookingID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle BackColor="White" ForeColor="#330099" />
            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
        </asp:GridView>
    </div>
</asp:Content>
