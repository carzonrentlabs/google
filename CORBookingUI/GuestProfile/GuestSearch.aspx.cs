﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.SolutionFramework;
using COR.BusinessFramework;
using COR.ObjectFramework;

public partial class GuestProfile_GuestSearch : System.Web.UI.Page
{
    private OL_Booking objBookingOL = null;
    private BL_Booking objBookingBL = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {           
            BindInplantName();
        }
        lblMesaage.Visible = false;
    }

    private void BindInplantName()
    {
        objBookingOL = new OL_Booking();
        objBookingBL = new BL_Booking();
        objBookingOL.ClientCoId = Convert.ToInt32(Session["ClientCoId"]);
        try
        {
            objBookingOL = objBookingBL.GetInplantName(objBookingOL);

            if (objBookingOL.ObjDataTable.Rows.Count > 0)
            {

                ddlInplantName.DataSource = objBookingOL.ObjDataTable;
                ddlInplantName.DataTextField = "ImplantName";
                ddlInplantName.DataValueField = "ImplantId";
                ddlInplantName.DataBind();
                ddlInplantName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlInplantName.DataSource = null;
                ddlInplantName.DataBind();
            }

        }
        catch (Exception Ex)
        {
            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            throw new Exception(Ex.Message);
        }
    }
    protected void bntSearch_Click(object sender, EventArgs e)
    {
        objBookingOL = new OL_Booking();
        objBookingBL = new BL_Booking();
        try
        {
            System.Threading.Thread.Sleep(1000);
            if (!string.IsNullOrEmpty(txtSearchKey.Text.Trim()) && Convert.ToInt16(rblSearch.SelectedValue) == 0)
            {
                objBookingOL.GuestSearchKey = txtSearchKey.Text.Trim();
                objBookingOL.SearchOption = Convert.ToInt16(rblSearch.SelectedValue);
            }
            else if (!string.IsNullOrEmpty(txtSearchKey.Text.Trim()) && Convert.ToInt16(rblSearch.SelectedValue) == 1)
            {
                objBookingOL.GuestSearchKey = txtSearchKey.Text.Trim();
                objBookingOL.SearchOption = Convert.ToInt16(rblSearch.SelectedValue);
            }
            else if (!string.IsNullOrEmpty(txtSearchKey.Text.Trim()) && Convert.ToInt16(rblSearch.SelectedValue) == 2)
            {
                objBookingOL.GuestSearchKey = txtSearchKey.Text.Trim();
                objBookingOL.GuestLastName = txtGuestLastName.Text.Trim();
                objBookingOL.SearchOption = Convert.ToInt16(rblSearch.SelectedValue);
            }

            objBookingOL.ClientCoId = Convert.ToInt32(Session["ClientCoId"]);
            objBookingOL = objBookingBL.GetGuestDetailsOnMobileBasis(objBookingOL);
            if (objBookingOL.DbOperationStatus == CommonConstant.SUCCEED &&  objBookingOL.ObjDataTable.Rows.Count >0 )
            {
                //pnlGuestDetails.Visible = true;
                //lblGuestEmailId.Text =Convert.ToString( objBookingOL.GuestEmailId);
                //lblGuestName.Text = Convert.ToString(objBookingOL.GuestName);
                //lblGuestMobileNo.Text = Convert.ToString(objBookingOL.GuestPhoneNo);
                //Session["ClientCoIndivID"] = objBookingOL.ClientCoIndivID.ToString();                   
                //Session["fin_year"] = "2010-2011";
                grvSearchGuest.DataSource = objBookingOL.ObjDataTable;
                grvSearchGuest.DataBind();
            }
            else
            {
                //pnlGuestDetails.Visible = false;
                Session["ClientCoIndivID"] = 0;
                grvSearchGuest.DataSource = null;
                grvSearchGuest.DataBind();
                lblMesaage.Visible = true;
                lblMesaage.Text = "Client has not registered.";
                lblMesaage.ForeColor = System.Drawing.Color.Red;
            }

        }
        catch (Exception Ex)
        {
            ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
            lblMesaage.Visible = true;
            lblMesaage.Text = Ex.Message;
            lblMesaage.ForeColor = System.Drawing.Color.Red;
        }

    }
    protected void bntNext_Click(object sender, EventArgs e)
    {
        Response.Redirect("../Booking/BookingSystem.aspx", false);
    }
    protected void grvSearchGuest_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        objBookingOL = new OL_Booking();
        objBookingBL = new BL_Booking();

        if (e.CommandName.Equals("GoNext"))
        {
            try
            {
                int index = Convert.ToInt16(e.CommandArgument);
                GridViewRow row = grvSearchGuest.Rows[index];
                Session["ClientCoIndivID"] = (int)grvSearchGuest.DataKeys[index].Values[1];
                objBookingOL.ClientCoId = Convert.ToInt32(Session["ClientCoId"]);
                Session["fin_year"] = "2010-2011";
                Session["ImplantId"] = ddlInplantName.SelectedValue;
                Response.Redirect("../Booking/BookingSystem.aspx", false);
            }
            catch (Exception Ex)
            {
                ErrorLogClass.LogErrorToLogFile(Ex, "an error occured");
                throw new Exception(Ex.Message);
            }
        }
    }
}