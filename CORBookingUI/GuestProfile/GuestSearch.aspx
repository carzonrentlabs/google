﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/CORImplantMaster.master"
    AutoEventWireup="true" CodeFile="GuestSearch.aspx.cs" Inherits="GuestProfile_GuestSearch" %>

<asp:Content ID="ContentPage" ContentPlaceHolderID="ImplantContent" runat="Server">
    <script type="text/javascript">
        //$(function () {
        function pageLoad(sender, args) {
            var listValue = $("#<%=rblSearch.ClientID%>").find(":checked").val();
            if (listValue == 0) {
                $("#<%=lblSearchName.ClientID%>").text("Guest Mobile No");
                $("#trGuestLastName").css("visibility", "hidden");
            }
            else if (listValue == 1) {
                $("#<%=lblSearchName.ClientID%>").text("Guest EMail Id");
                $("#trGuestLastName").css("visibility", "hidden");
            }
            else if (listValue == 2) {
                $("#<%=lblSearchName.ClientID%>").text("Guest First Name");
                $("#trGuestLastName").css("visibility", "visible");
            }

            $("#<%=rblSearch.ClientID%>").click(function () {
                listValue = $(this).find(":checked").val();
                if (listValue == 0) {
                    $("#<%=lblSearchName.ClientID%>").text("Guest Mobile No");
                    //$("#trGuestLastName").css("display", "none");
                    $("#trGuestLastName").css("visibility", "hidden");
                }
                else if (listValue == 1) {
                    //alert("1")
                    $("#<%=lblSearchName.ClientID%>").text("Guest EMail Id");
                    //$("#trGuestLastName").css("display", "none");
                    $("#trGuestLastName").css("visibility", "hidden");
                }
                else if (listValue == 2) {
                    //alert("2")
                    $("#<%=lblSearchName.ClientID%>").text("Guest First Name");
                    //$("#trGuestLastName").css("display", "block");
                    $("#trGuestLastName").css("visibility", "visible");
                }
            });

            $("#<%=txtSearchKey.ClientID%>").keyup(function () {
                if (listValue == 0) {
                    var strPass = $("#<%=txtSearchKey.ClientID%>").val();
                    var strLength = strPass.length;
                    var lchar = strPass.charAt((strLength) - 1);
                    var cCode = CalcKeyCode(lchar);
                    if (cCode < 46 || cCode > 57 || cCode == 47) {
                        var myNumber = strPass.substring(0, (strLength) - 1);
                        $("#<%=txtSearchKey.ClientID%>").val(myNumber);
                        alert("Enter only numeric value.")
                    }
                }
            });

            $("#<%=bntSearch.ClientID%>").click(function () {

                if ($("#<%=txtSearchKey.ClientID%>").val() == "" && listValue == 0) {
                    alert("Enter guest mobile nubmer");
                    $("#<%=txtSearchKey.ClientID%>").focus();
                    return false;
                }
                else if (isNaN($("#<%=txtSearchKey.ClientID%>").val()) && listValue == 0) {
                    alert("Enter only numeric value");
                    $("#<%=txtSearchKey.ClientID%>").focus();
                    return false;
                }
                else if ($("#<%=txtSearchKey.ClientID%>").val().length < 10 && listValue == 0) {
                    alert("Mobile number should not be less than 10 digits.");
                    $("#<%=txtSearchKey.ClientID%>").focus();
                    return false;
                }
                else if (!ValidateEmail($("#<%=txtSearchKey.ClientID%>").val()) && listValue == 1) {
                    alert("Invalid email address.");
                    $("#<%=txtSearchKey.ClientID%>").focus();
                    return false;
                }

                //                else if ($("#<%=txtSearchKey.ClientID%>").val().length > 10) {
                //                    alert("Enter 10 digit mobile number.");
                //                    $("#<%=txtSearchKey.ClientID%>").focus();
                //                    return false;
                //                }
            });



        };
        function CalcKeyCode(aChar) {
            var character = aChar.substring(0, 1);
            var code = aChar.charCodeAt(0);
            return code;
        }

        function ValidateEmail(email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
        };

    </script>
    <asp:UpdatePanel ID="udpSearch" runat="server">
        <ContentTemplate>
            <table class="booking_cor" cellpadding="0" cellspacing="0" border="1" width="40%">
                <tr>
                    <td class="bookings_h" colspan="4" align="center">
                        <b>Search Guest</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:Label ID="lblMesaage" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="white-space: nowrap;" align="center">
                        <asp:RadioButtonList ID="rblSearch" runat="server" RepeatDirection="Horizontal" CellPadding="5"
                            CellSpacing="5" BorderStyle ="None" BorderColor="Black" CssClass ="bk" >
                            <asp:ListItem Value="0" Text="Mobile No" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="1" Text="EMail Id"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Name"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                  <tr>
                    <td style="width: 30%;">
                        <b>
                            <asp:Label ID="Label1" runat="server" Text="Inplant Name"></asp:Label></b>
                    </td>
                    <td style="width: 40%;" >
                       <asp:DropDownList ID="ddlInplantName" runat="server" ToolTip="Select Inplant Name"></asp:DropDownList>
                    </td>
                       <td style="width: 10%;">
                       <asp:RequiredFieldValidator ID="rvddlInplantName" runat="server" ControlToValidate="ddlInplantName"
                            ErrorMessage="*" InitialValue="0" ForeColor="Red" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 20%;">
                      
                    </td>
                   
                </tr>
                <tr>
                    <td style="width: 30%;">
                        <b>
                            <asp:Label ID="lblSearchName" runat="server" Text="Guest Mobile No"></asp:Label></b>
                    </td>
                    <td style="width: 40%;">
                        <asp:TextBox ID="txtSearchKey" runat="server" MaxLength="100"></asp:TextBox>
                    </td>
                    <td style="width: 10%;">
                        <asp:RequiredFieldValidator ID="rfvMobileNo" runat="server" ControlToValidate="txtSearchKey"
                            ErrorMessage="*" ForeColor="Red" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </td>
                    <td style="width: 20%;">
                        <asp:Button ID="bntSearch" runat="server" Text="Search" OnClick="bntSearch_Click" />
                    </td>
                </tr>
                <tr id="trGuestLastName" style="visibility: hidden">
                    <td style="width: 30%;">
                        <b>
                            <asp:Label ID="lblGuestLastName" runat="server" Text="Guest Last Name"></asp:Label></b>
                    </td>
                    <td style="width: 40%;">
                        <asp:TextBox ID="txtGuestLastName" runat="server" MaxLength="50"></asp:TextBox>
                    </td>
                    <td style="width: 10%;">
                        &nbsp;
                    </td>
                    <td style="width: 20%;">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <asp:UpdateProgress ID="updProgress" runat="server" AssociatedUpdatePanelID="udpSearch"
                DynamicLayout="false">
                <ProgressTemplate>
                    <img src="../Images/wait.gif" alt="Loading..." />
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:GridView ID="grvSearchGuest" runat="server" AutoGenerateColumns="False" DataKeyNames="ClientCoID,ClientCoIndivID"
                BorderStyle="Solid" BorderWidth="2px" CellPadding="2" OnRowCommand="grvSearchGuest_RowCommand">
                <Columns>
                    <asp:BoundField HeaderText="Guest Name" DataField="GuestName"  ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField HeaderText="Guest EmailID" DataField="EmailID" ItemStyle-HorizontalAlign="Left"/>
                    <asp:BoundField HeaderText="Guest Mobile No" DataField="GuestMobileNo" ItemStyle-HorizontalAlign="Left"/>
                    <asp:TemplateField HeaderText="Go">
                        <ItemTemplate>
                            <asp:Button ID="bntNext" runat="server" Text="Next" CommandName="GoNext" CommandArgument='<%#((GridViewRow)Container).RowIndex %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle BackColor="White" ForeColor="#330099" />
                <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                <SortedAscendingCellStyle BackColor="#FEFCEB" />
                <SortedAscendingHeaderStyle BackColor="#AF0101" />
                <SortedDescendingCellStyle BackColor="#F6F0C0" />
                <SortedDescendingHeaderStyle BackColor="#7E0000" />
            </asp:GridView>
            <%-- <asp:Panel runat="server" ID="pnlGuestDetails" Visible="false">
        <table cellpadding="1" cellspacing="1" border="1">
            <tr>
                <td class="left">
                    <b>Guest Name</b>
                </td>
                <td class="left">
                    <asp:Label ID="lblGuestName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="left">
                    <b>Guest EmailID</b>
                </td>
                <td class="left">
                    <asp:Label ID="lblGuestEmailId" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="left">
                    <b>Guest Mobile No</b>
                </td>
                <td class="left">
                    <asp:Label ID="lblGuestMobileNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="bntNext" runat="server" Text="Next" OnClick="bntNext_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
