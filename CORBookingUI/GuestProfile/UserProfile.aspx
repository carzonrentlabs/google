﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/CORMaster.master" AutoEventWireup="true"
    CodeFile="UserProfile.aspx.cs" Inherits="Guest_Profile_UserProfile" %>

<asp:Content ID="ContentPage" ContentPlaceHolderID="MainContent" runat="Server">
 <script type="text/javascript">
     $(function () {
         $("#liBooking").removeClass('active')
         $("#liBookingEdit").removeClass('active')
         $("#liBookingCancelDetails").removeClass('active')
         $("#liBookingUsreProfile").addClass('active')
         $("#liBookingCancelSearch").removeClass('active')
     });
    
    </script>
    <br />
    <table  cellpadding="2" cellspacing="2" border="1" width="40%">
        <tr>
            <td class="bookings_h" colspan="3" align="center">
                <b>Guest Profile </b>
            </td>
        </tr>
        <tr>
            <td class="left">
                <b>Guest Name</b>
            </td>
            <td class="left">
                <asp:Label ID="lblGuestName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="left">
                <b>Contact No</b>
            </td>
            <td class="left">
                <asp:Label ID="lblContactNo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="left">
                <b>Email</b>
            </td>
            <td class="left">
                <asp:Label ID="lblGuestEmail" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="left">
                <b>Company Name</b>
            </td>
            <td class="left">
                <asp:Label ID="lblCompany" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="left">
                <b>City Name</b>
            </td>
            <td class="left">
                <asp:Label ID="lblCity" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="left">
                <b>Employee Code</b>
            </td>
            <td class="left">
                <asp:Label ID="lblEmpCode" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="left">
                <b>Payment Mode</b>
            </td>
            <td class="left">
                <asp:Label ID="lblPaymentMode" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
