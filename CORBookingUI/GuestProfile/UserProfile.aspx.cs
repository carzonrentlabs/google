﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.ObjectFramework;
using COR.BusinessFramework;

public partial class Guest_Profile_UserProfile : System.Web.UI.Page
{
    private OL_LoginUser objLoginUserOL = null;
    private BL_LoginUser obLoginUserBL = null;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            FillGuestUserDetails(Convert.ToInt32(Session["ClientCoIndivID"].ToString()));
        }
    }
    private void FillGuestUserDetails(int ClientCoIndivId)
    {
        objLoginUserOL = new OL_LoginUser();
        obLoginUserBL = new BL_LoginUser();
        try
        {
            objLoginUserOL.ClientCoIndivID = ClientCoIndivId;
            objLoginUserOL = obLoginUserBL.GetGuestProfile(objLoginUserOL);
            if (objLoginUserOL != null)
            {
                if (objLoginUserOL.ObjDataTable.Rows.Count > 0)
                {
                    lblGuestName.Text = objLoginUserOL.ObjDataTable.Rows[0]["GuestName"].ToString();
                    lblContactNo.Text = objLoginUserOL.ObjDataTable.Rows[0]["ContactNo"].ToString();
                    lblGuestEmail.Text = objLoginUserOL.ObjDataTable.Rows[0]["Email"].ToString();
                    lblCompany.Text = objLoginUserOL.ObjDataTable.Rows[0]["CompanyName"].ToString();
                    lblCity.Text = objLoginUserOL.ObjDataTable.Rows[0]["CityName"].ToString();
                    lblEmpCode.Text = objLoginUserOL.ObjDataTable.Rows[0]["EmpCode"].ToString();
                    lblPaymentMode.Text = objLoginUserOL.ObjDataTable.Rows[0]["PaymentMode"].ToString();
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }
}