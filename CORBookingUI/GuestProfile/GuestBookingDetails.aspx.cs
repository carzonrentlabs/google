﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.ObjectFramework;
using COR.SolutionFramework;
using COR.BusinessFramework;

public partial class GuestProfile_GuestBookingDetails : System.Web.UI.Page
{
    private OL_Booking objBookingOL = null;
    private BL_Booking objBookingBL = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Visible = false;
    }
    protected void btnGetBookingDetails_Click(object sender, EventArgs e)
    {

        try
        {
            BindBookingDetails(Convert.ToInt32(Session["ClientCoIndivID"]));
        }
        catch (Exception Ex)
        {

            ErrorLogClass.LogErrorToLogFile(Ex, "An error occured");
        }
    }

    private void BindBookingDetails(int ClientCoIndivId)
    {
        objBookingOL = new OL_Booking();
        objBookingBL = new BL_Booking();
        lblMessage.Visible = false;
        try
        {
            objBookingOL.ClientCoIndivID = ClientCoIndivId;
            objBookingOL.PickupDate = Convert.ToDateTime(txtPickUpdate.Text);
            objBookingOL.DropOffDate = Convert.ToDateTime(txtPickupToDate.Text);
            objBookingOL = objBookingBL.GetGuestBookingDetails(objBookingOL);
            if (objBookingOL != null && objBookingOL.ObjDataTable.Rows.Count > 0)
            {
                gvBooking.DataSource = objBookingOL.ObjDataTable;
                gvBooking.DataBind();

            }
            else
            {
                gvBooking.DataSource = null;
                gvBooking.DataBind();
                lblMessage.Visible = true;
                lblMessage.Text = "Booking not available.";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception Ex)
        {
            ErrorLogClass.LogErrorToLogFile(Ex, "An error occured");
        }
    }
    protected void gvBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblPriority = (Label)e.Row.FindControl("lblBookingStatus");
            LinkButton lblLinkStatus = (LinkButton)e.Row.FindControl("lnkButtion");
            if (Convert.ToString(lblPriority.Text) == "Close")
            {
                lblLinkStatus.Text = "View";
            }

        }

    }
}