<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PasswordRecovery.aspx.cs"
    Inherits="PasswordRecovery" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Password Recovery</title>
    <script src="../Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="frmRecovery" runat="server">
    <div>
        <table>
            <tr>
                <td colspan="2" align="left">
                    <a href="">
                        <img src="../Images/COR-logo.gif" border="0" style="height: 58px; width: 213px" alt="" />
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <center>
        <div style="width: 400px" runat="server" id="divPasswordRecovery">
            <fieldset style="border-color: Green">
                <legend><b>Password Recovery</b></legend>
                <table>
                    <tr>
                        <td colspan="2">
                            <h2>
                                Password Recovery
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Use the form below to reset your password.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Msg" runat="server" ForeColor="maroon" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 22px">
                            Login ID:
                            <asp:TextBox ID="txtLoginId" runat="server" AutoPostBack="true" />
                            <asp:RequiredFieldValidator ID="UsernameRequiredValidator" runat="server" ControlToValidate="txtLoginId"
                                ForeColor="Maroon" Display="Dynamic"  ErrorMessage="Required" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="ResetPasswordButton" Text="Reset Password" runat="server" Enabled="false" />
                            &nbsp;
                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" CausesValidation="false" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
    </form>
</body>
</html>
