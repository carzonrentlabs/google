﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="Login_ChangePassword"
    MasterPageFile="~/MasterPage/CORMaster.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="ContentPage" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Scripts>
            <asp:ScriptReference Path="../Scripts/jquery-1.4.1.min.js" />
            <asp:ScriptReference Path="../Scripts/jquery.validate.pack.js" />
        </Scripts>
    </asp:ScriptManagerProxy>
    <div style="width: 800px; margin: 0 auto;">
        <table cellspacing="2" cellpadding="1" width="100%" border="0">
            <tr>
                <td colspan="3" align="center" style="padding: 10px 0;">
                    <b class="heading">Change Password</b>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Label ID="lbl_message" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="25%" style="text-align: right">
                    <b>
                        <label for="tb_CurrentPassword">
                            Current password:</label></b>
                </td>
                <td width="25%">
                    <asp:TextBox ID="tb_CurrentPassword" runat="server" TextMode="Password" Width="200"></asp:TextBox>
                </td>
                <td width="50%">
                    <asp:RequiredFieldValidator ID="rfvCurrentPassword" runat="server" ControlToValidate="tb_CurrentPassword"
                        Display="Dynamic" ErrorMessage="Enter current password" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <b>
                        <label for="tb_NewPassword">
                            New password:
                        </label>
                    </b>
                </td>
                <td>
                    <asp:TextBox ID="tb_NewPassword" runat="server" TextMode="Password" Width="200"></asp:TextBox>
                    <cc1:PasswordStrength ID="PasswordStrength1" runat="server" DisplayPosition="RightSide"
                        StrengthIndicatorType="BarIndicator" TargetControlID="tb_NewPassword" PrefixText="Stength:"
                        Enabled="true" RequiresUpperAndLowerCaseCharacters="true" MinimumLowerCaseCharacters="1"
                        MinimumUpperCaseCharacters="1" MinimumSymbolCharacters="1" MinimumNumericCharacters="1"
                        PreferredPasswordLength="8" TextStrengthDescriptions="VeryPoor; Weak; Average; Strong; Excellent"
                        StrengthStyles="VeryPoor; Weak; Average; Excellent; Strong;" CalculationWeightings="25;25;15;35"
                        BarBorderCssClass="border" HelpStatusLabelID="Label1">
                    </cc1:PasswordStrength>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="rfvNewPassword" runat="server" ControlToValidate="tb_NewPassword"
                        ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}"
                        ErrorMessage="Password must contain: Minimum 8 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character"
                        ForeColor="Red" Display="Dynamic" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <b>
                        <label for="tb_ConfirmNewPassword">
                            Confirm new password:
                        </label>
                    </b>
                </td>
                <td>
                    <asp:TextBox ID="tb_ConfirmNewPassword" runat="server" TextMode="Password" Width="200"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rfvConfirmNewPassword" runat="server" ControlToValidate="tb_ConfirmNewPassword"
                        Display="Dynamic" ErrorMessage="Enter confirm new password" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="comparePasswords" runat="server" ControlToCompare="tb_NewPassword"
                        ControlToValidate="tb_ConfirmNewPassword" ErrorMessage="New password and confirm new password should be same." ForeColor="Red"
                        Display="Dynamic" Type ="String" />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center; padding: 5px 0;">
                    <%--<asp:Label ID="Label1" runat="server" Font-Bold="true" Font-Size="Small" ForeColor="Brown"></asp:Label>--%>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td colspan="2" align="left">
                    <asp:Button ID="btn_Save" TabIndex="3" runat="server" ToolTip="Save your new password"
                        EnableViewState="False" Text="Update" OnClick="btn_Save_Click"></asp:Button>
                    &nbsp;&nbsp;
                    <input id="Reset" type="reset" value="Reset" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
