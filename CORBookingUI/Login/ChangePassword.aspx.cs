﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.BusinessFramework;

public partial class Login_ChangePassword : System.Web.UI.Page
{
    BL_ChangePassword objChangePasswordBL = null;
    OL_LoginUser objLoginUserOL = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ClientCoIndivID"].ToString().Equals("") || string.IsNullOrEmpty(Convert.ToString(Session["ClientCoIndivID"])))
        {
            Response.Redirect("~/Login.aspx",false);
        }
    }
    protected void btn_Save_Click(object sender, EventArgs e)
    {
        objChangePasswordBL = new BL_ChangePassword();
        objLoginUserOL = new OL_LoginUser();
        try
        {
            if (tb_CurrentPassword.Text.ToString().Contains(' ') || tb_NewPassword.Text.ToString().Contains(' '))
            {
                lbl_message.Text = "";
                lbl_message.Style.Value = "color:red";
                lbl_message.Text = "All fields should be without blank space.";
                return;
            }
            else if (tb_NewPassword.Text.ToString()=="")
            {
                lbl_message.Text = "";
                lbl_message.Style.Value = "color:red";
                lbl_message.Text = "Enter new password.";
                return;
            }
            else if (tb_NewPassword.Text.ToString().Length < 8)
            {
                lbl_message.Text = "";
                lbl_message.Style.Value = "color:red";
                lbl_message.Text = "New Password shold not be less than 8 characters.";
                return;
            }
            else if (tb_NewPassword.Text.Trim() != tb_ConfirmNewPassword.Text.Trim())
            {
                lbl_message.Text = "";
                lbl_message.Style.Value = "color:red";
                lbl_message.Text = "New password and confirm new password should be same.";
                return;
            }
            else if (tb_NewPassword.Text.Trim() == tb_ConfirmNewPassword.Text.Trim())
            {
                objLoginUserOL.ClientCoIndivID = Convert.ToInt32(Session["ClientCoIndivID"]);
                objLoginUserOL = objChangePasswordBL.CheckCurrentPassword(objLoginUserOL);
                if (string.IsNullOrEmpty(objLoginUserOL.Password.ToString()))
                {
                    lbl_message.Text = "";
                    lbl_message.Style.Value = "color:red";
                    lbl_message.Text = "Enter the correct old password.";
                    return;
                }
                else if (objLoginUserOL.Password.ToString().Trim() != tb_CurrentPassword.Text.ToString().Trim())
                {
                    lbl_message.Text = "";
                    lbl_message.Style.Value = "color:red";
                    lbl_message.Text = "Enter the correct old password.";
                    return;
                }
                else
                {
                    objLoginUserOL.Password = tb_NewPassword.Text.ToString().Trim();
                    objLoginUserOL = objChangePasswordBL.ChangePassword(objLoginUserOL);
                    if (objLoginUserOL.LoginStatus.ToString().Trim() == "Invalid")
                    {
                        lbl_message.Text = "";
                        lbl_message.Style.Value = "color:red";
                        lbl_message.Text = "New password not updated.";
                    }
                    else
                    {
                        lbl_message.Text = "";
                        lbl_message.Style.Value = "color:red";
                        lbl_message.Text = "New password has been successfully updated.";
                    }

                }
            }
        }
        catch (Exception Ex)
        {
            ErrorLogClass.LogErrorToLogFile(Ex,"an error occured.");
            throw new Exception (Ex.Message);
        }

    }
}