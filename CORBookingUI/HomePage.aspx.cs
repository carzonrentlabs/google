﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.ObjectFramework;
using COR.BusinessFramework;
using System.Drawing;

public partial class HomePage : System.Web.UI.Page
{
    private OL_LoginUser objLoginUserOL = null;
    private BL_LoginUser obLoginUserBL = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetLocation(Convert.ToInt32(Session["ClientCoIndivID"]));   
         
        }
    }

    private void GetLocation(int sysUserId)
    {
        objLoginUserOL = new OL_LoginUser();
        obLoginUserBL = new BL_LoginUser();
        try
        {
            objLoginUserOL.SysUserId = sysUserId;
            objLoginUserOL = obLoginUserBL.GetRegistedLocation(objLoginUserOL);
            if (!string.IsNullOrEmpty(objLoginUserOL.UserName) && objLoginUserOL.UserName != null)
            {
              lblMessage.Visible = true;
              lblMessage.Text = "Welcome  Mr. " + objLoginUserOL.UserName.ToString() +" "+DateTime.Now.ToShortDateString () +" "+ DateTime.Now.ToShortTimeString() ;
              lblMessage.ForeColor = Color.Red;
            }
            else
            {
               lblMessage.Visible = false;
            }
        }
        catch (Exception)
        {
            
            throw;
        }
    }
}