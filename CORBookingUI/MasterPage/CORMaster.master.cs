﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using COR.SolutionFramework;
using COR.ObjectFramework;
using COR.BusinessFramework;
using System.Drawing;

public partial class MasterPage_CORMaster : System.Web.UI.MasterPage
{

    private OL_LoginUser objLoginUserOL = null;
    private BL_LoginUser obLoginUserBL = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToInt16(Session["Implant"]) == 1)
        {
            hlChangePassword.NavigateUrl = "../GuestProfile/GuestSearch.aspx";
            hlChangePassword.Text = "Search Guest";
        }
        else
        {
            hlChangePassword.NavigateUrl = "../Login/ChangePassword.aspx";
            hlChangePassword.Text = "Change Password";
        }
            
        
        if (!Page.IsPostBack)
        {
            GetLocation(Convert.ToInt32(Session["ClientCoIndivID"]));  //Get Location
        }
        Page_PreInit(sender, e);
        this.CheckSessionTimeout(); 
    }

    
    private void GetLocation(int sysUserId)
    {
        objLoginUserOL = new OL_LoginUser();
        obLoginUserBL = new BL_LoginUser();
        try
        {
            objLoginUserOL.SysUserId = sysUserId;
            objLoginUserOL = obLoginUserBL.GetRegistedLocation(objLoginUserOL);
            if (!string.IsNullOrEmpty(objLoginUserOL.UserName) && objLoginUserOL.UserName != null)
            {
                lblWelcomeMessage.Visible = true;
                lblWelcomeMessage.Text = "Welcome  Mr. " + objLoginUserOL.UserName.ToString() + " " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                lblWelcomeMessage.ForeColor = Color.Red;
            }
            else
            {
                lblWelcomeMessage.Visible = false;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

   
    private void CheckSessionTimeout()
    {
        string msgSession = "Warning: Within next 3 minutes, if you do not do anything, " +
            " our system will redirect to the login page. Please save changed data.";

        //time to remind, 3 minutes before session ends
        int int_MilliSecondsTimeReminder = (this.Session.Timeout * 1000) - 3 * 1000;

        //time to redirect, 5 milliseconds before session ends
        int int_MilliSecondsTimeOut = (this.Session.Timeout * 1000) - 5;

        string str_Script = @" var myTimeReminder, myTimeOut; " +
            " clearTimeout(myTimeReminder); " +
            " clearTimeout(myTimeOut); " +
            "var sessionTimeReminder = " + int_MilliSecondsTimeReminder.ToString() + "; " +
            "var sessionTimeout = " + int_MilliSecondsTimeOut.ToString() + ";" +
            "function doReminder(){ alert('" + msgSession + "'); }" +
            "function doRedirect(){ window.location.href='../Login.aspx'; }" +
            " myTimeReminder=setTimeout('doReminder()', sessionTimeReminder); myTimeOut=setTimeout('doRedirect()',sessionTimeout); ";
        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "CheckSessionOut", str_Script, true);
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Request.UserAgent.Contains("AppleWebKit"))
            Request.Browser.Adapters.Clear();
    }
}
