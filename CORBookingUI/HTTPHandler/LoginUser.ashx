﻿<%@ WebHandler Language="C#" Class="LoginUser" %>

using System;
using System.Web;
using System.Data;
using COR.ObjectFramework;
using COR.BusinessFramework;
using System.Web.SessionState;

public class LoginUser : IHttpHandler, IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) 
    {
        context.Response.ContentType = "text/plain";
        string username = context.Request.QueryString["username"].ToString();
        string Password = context.Request.QueryString["Password"].ToString();
        OL_LoginUser objLoginUserOL = new OL_LoginUser();
        BL_LoginUser objLoginUserBL=new BL_LoginUser ();
        LoginValidate loginLog = new LoginValidate();
        objLoginUserOL.LoginId = username.ToString();
        objLoginUserOL.Password = Password.ToString();
        objLoginUserOL = objLoginUserBL.AuthenticateLoginId(objLoginUserOL);
        if (objLoginUserOL.LoginStatus.Equals("valid") && Convert.ToInt32(objLoginUserOL.ClientCoIndivID) > 0)
        {
            context.Session["ClientCoIndivID"] = objLoginUserOL.ClientCoIndivID.ToString();
            context.Session["ClientCoId"] = objLoginUserOL.ClientCoId.ToString();          
            context.Session["fin_year"] = "2010-2011";
            context.Session["Implant"] = 0; //For Guest
            //DataTable dtInsert = loginLog.InsertLoginDetail(objLoginUserOL.LoginId.ToString().Trim(), context.Request.ServerVariables["remote_addr"]);
            context.Response.Write(objLoginUserOL.LoginStatus.ToString());
            context.Response.End();
        }
        else
        {
            //context.Response.Write(objLoginUserOL.LoginStatus.ToString());
            context.Response.Write("Invalid Id or Passowrd.");
            context.Response.End();
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}