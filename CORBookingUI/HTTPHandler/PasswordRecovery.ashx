﻿<%@ WebHandler Language="C#" Class="PasswordRecovery" %>

using System;
using System.Web;
using COR.ObjectFramework;
using COR.BusinessFramework;
using COR.SolutionFramework;

public class PasswordRecovery : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        OL_LoginUser objLoginUserOL = new OL_LoginUser();
        BL_LoginUser objLoginUserBL = new BL_LoginUser();        
        context.Response.ContentType = "text/plain";
        string userId = context.Request.QueryString["userId"].ToString();
        objLoginUserOL.LoginId = userId.ToString();
        objLoginUserOL.ClientCoId = Convert.ToInt32(GeneralMethos.Decrypt(HttpUtility.UrlDecode(context.Request.QueryString["ClientCoId"])));
        objLoginUserOL=objLoginUserBL.GetValidateUserIdForPasswordRecovery(objLoginUserOL);
        if (objLoginUserOL.LoginStatus.Equals("Valid"))
        {
            context.Response.Write(objLoginUserOL.LoginStatus.ToString());
            context.Response.End();
        }
        else
        {
            context.Response.Write("Invalid Id.");
            context.Response.End();
        }        
        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}