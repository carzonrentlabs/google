﻿<%@ WebHandler Language="C#" Class="ImplantLogin" %>

using System;
using System.Web;
using System.Data;
using COR.ObjectFramework;
using COR.BusinessFramework;
using System.Web.SessionState;

public class ImplantLogin : IHttpHandler,IRequiresSessionState  {
    
    public void ProcessRequest (HttpContext context) 
    {
        context.Response.ContentType = "text/plain";

        string username = context.Request.QueryString["username"].ToString();
        string Password = context.Request.QueryString["Password"].ToString();
        OL_LoginUser objLoginUserOL = new OL_LoginUser();
        BL_LoginUser objLoginUserBL = new BL_LoginUser();
        objLoginUserOL.LoginId = username.ToString();
        objLoginUserOL.Password = Password.ToString();
        
        objLoginUserOL = objLoginUserBL.AuthenticateImplantLoginId(objLoginUserOL);
        if (objLoginUserOL.LoginStatus.Equals("valid") && Convert.ToInt32(objLoginUserOL.ClientCoId) > 0)
        {
            context.Session["ClientCoId"] = objLoginUserOL.ClientCoId.ToString();
            context.Session["fin_year"] = "2010-2011";
            context.Session["Implant"] = 1; //For Implant
            context.Response.Write(objLoginUserOL.LoginStatus.ToString());
            context.Response.End();
        }
        else
        {
            context.Response.Write("Invalid Id or Passowrd.");
            context.Response.End();
        }
        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}