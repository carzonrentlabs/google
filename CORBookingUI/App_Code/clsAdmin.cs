using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using System.Collections;
using System.Data.OleDb;
using COR.ObjectFramework;
using COR.SolutionFramework;
using System.Net;
//using Sys.Data.layer;


///<summary>
///Summary description for clslAdmin
///</summary>
///
namespace ReportingTool
{
    public class clsAdmin
    {
        public int Retval;
        public string dbUser = "carzonrent";
        public string dbPWD = "CarZ#58B9";
        public clsAdmin()
        {
            // TODO: Add constructor logic here
        }

        public static void SaveFile(string strPath, byte[] bData)
        {

            FileStream NewFile;
            NewFile = new FileStream(strPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
            NewFile.Write(bData, 0, bData.Length);
            NewFile.Close();
        }


        public string SendMail_RegisteredGuest(OL_Registration objRegistrationOL)
        {
            string From = "Reserve Team<services@carzonrent.com>";
            string MailTo = objRegistrationOL.EmailId;
            string Subject = "Welcome to Carzonrent.";
            StringBuilder MailBody = new StringBuilder();
            MailBody = MailBody.Append("Dear " + objRegistrationOL.FirstName + " " + objRegistrationOL.LastName + ",");
            MailBody = MailBody.Append("<br/>");
            MailBody = MailBody.Append("<br/>");
            MailBody = MailBody.Append("We welcome you to carzonrent. ");
            MailBody = MailBody.Append("Please find below your password to access your account at Carzonrent.");
            MailBody = MailBody.Append("<br/>");
            MailBody = MailBody.Append("<br/>");
            MailBody = MailBody.Append("Password: " + objRegistrationOL.Password);
            MailBody = MailBody.Append("<br/>");
            MailBody = MailBody.Append("Please Login to the below URL to update your password and perform online transaction.");
            MailBody = MailBody.Append("<br/>");
            MailBody = MailBody.Append("<br/>");
            //MailBody = MailBody.Append("You may visit Carzonrent portal to do transaction online on the following link.");
            //MailBody = MailBody.Append("<br/>");
            //MailBody = MailBody.Append("http://insta.carzonrent.com/Corbooking/Login.aspx");
            //MailBody = MailBody.Append("http://corporate.carzonrent.com/CorporateSite/Corporatelogin.asp");
            if (objRegistrationOL.ClientCoId == 2806)
            {
                MailBody = MailBody.Append("http://insta.carzonrent.com/Corbooking/Login.aspx");
            }
            else
            {
                MailBody = MailBody.Append("https://corporate.carzonrent.com/CorporateSite/Corporatelogin.asp");
            }
            MailBody = MailBody.Append("<br/>");
            MailBody = MailBody.Append("<br/>");
            MailBody = MailBody.Append("Warm Regards,");
            MailBody = MailBody.Append("<br/>");
            MailBody = MailBody.Append("<br/>");
            MailBody = MailBody.Append("Carzonrent (I) Pvt Ltd");
            MailBody = MailBody.Append("<br/>");
            MailBody = MailBody.Append("<br/>");
            string MailStatus = Mail(From, MailTo, Subject, MailBody.ToString());

            return MailStatus;
        }
        /// <summary>
        /// Mail Method.
        /// </summary>
        /// <param name="From"></param>
        /// <param name="To"></param>
        /// <param name="Subject"></param>
        /// <param name="Body"></param>
        public string Mail(string From, string To, string Subject, string Body)
        {
            string MailStatus = "";
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.To.Add(To);
                message.Bcc.Add("techreports@carzonrent.com,ankit.verma@carzonrent.com");
                message.Subject = Subject;
                message.From = new System.Net.Mail.MailAddress(From);
                message.IsBodyHtml = true;
                message.Body = Body;
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp-relay.gmail.com", 587);
                smtp.EnableSsl = false;
                smtp.Credentials = new NetworkCredential("booking@carzonrent.com", "Carz@1212");
                smtp.Send(message);
                MailStatus = "Success";
            }
            catch (Exception ex)
            {
                MailStatus = ex.ToString();
                ErrorLogClass.LogErrorToLogFile(ex, "an error occured");
            }

            return MailStatus;
        }

        //public DataSet getClientConame()
        //{
        //    try
        //    {
        //        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetClientCoID");
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}


        //public DataSet GetClientCoAddID(int ClientCoID)
        //{
        //    SqlParameter[] ObjParam = new SqlParameter[1];
        //    ObjParam[0] = new SqlParameter("@ClientCoID", SqlDbType.Int);
        //    ObjParam[0].Direction = ParameterDirection.Input;
        //    ObjParam[0].Value = ClientCoID;
        //    return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetClientCoAddID", ObjParam);
        //}


        //public string BulkGuest_Upload(int ClientCoId, int ClientCoAddId, string Fname, string Mname, string Lname, string Phone1, string EmailId, string PaymentTerms, string Remarks, Boolean Active, Boolean IsVIP, Boolean PreAuthNotRequire, string Gender, string EmpCode, int UploadStatus, string UploadType)
        //{
        //    SqlParameter[] sqlParam = new SqlParameter[16];

        //    sqlParam[0] = new SqlParameter("@ClientCoId", ClientCoId);
        //    sqlParam[1] = new SqlParameter("@ClientCoAddId", ClientCoAddId);
        //    sqlParam[2] = new SqlParameter("@Fname", Fname);
        //    sqlParam[3] = new SqlParameter("@Mname", Mname);
        //    sqlParam[4] = new SqlParameter("@Lname", Lname);
        //    sqlParam[5] = new SqlParameter("@Phone1", Phone1);
        //    sqlParam[6] = new SqlParameter("@EmailId", EmailId);
        //    sqlParam[7] = new SqlParameter("@PaymentTerms", PaymentTerms);
        //    sqlParam[8] = new SqlParameter("@Remarks", Remarks);
        //    sqlParam[9] = new SqlParameter("@Active", Active);
        //    sqlParam[10] = new SqlParameter("@IsVIP", IsVIP);
        //    sqlParam[11] = new SqlParameter("@PreAuthNotRequire", PreAuthNotRequire);
        //    //sqlParam[12] = new SqlParameter("@FacilitatorId", FacilitatorId);
        //    sqlParam[12] = new SqlParameter("@Gender", Gender);
        //    sqlParam[13] = new SqlParameter("@EmpCode", EmpCode);
        //    sqlParam[14] = new SqlParameter("@UploadStatus", UploadStatus);
        //    sqlParam[15] = new SqlParameter("@UploadType", UploadType);

        //    DataSet ds = DbConnect.GetDataSet("Bl_SP_BulkGuest_Upload", sqlParam);
        //    return ds.Tables[0].Rows[0]["Status"].ToString();
        //}



        // public DataSet GetBulk_status(int BranchID)
        //{
        //    SqlParameter[] sqlParam = new SqlParameter[1];
        //    sqlParam[0] = new SqlParameter("@BranchID", BranchID);

        //    return DbConnect.GetDataSet("Bl_sp_getBulkStatus", sqlParam);
        //}


        //public DataSet GetBulk_SaveGuest(int UploadStatus)
        //{
        //    SqlParameter[] sqlParam = new SqlParameter[1];
        //    sqlParam[0] = new SqlParameter("@UploadStatus", UploadStatus);

        //    return DbConnect.GetDataSet("bl_sp_getGuestDetails", sqlParam);
        //}


        public DataSet ExportFromExcelToDataSet(string strPath, bool v_bHeader, string v_sSheetName)
        {
            DataSet myDataset = new DataSet();
            string strConn;
            OleDbDataAdapter myData;
            string strfilename;

            if (v_bHeader)
            {
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                          "Data Source=" + strPath + ";" +
                          "Extended Properties='Excel 8.0;HDR=Yes;FMT=Delimited'";
                // "Extended Properties='Excel 8.0;HDR=YES'";
            }

            else
            {
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                          "Data Source=" + strPath + ";" +
                          "Extended Properties='Excel 8.0;HDR=Yes;FMT=Delimited'";
                // "Extended Properties='Excel 8.0;HDR=NO'";
            }

            // v_sSheetName is the name of the user who created the file

            strfilename = "Sheet1$";

            if (v_sSheetName == "")
            {
                //myData = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", strConn);

                myData = new OleDbDataAdapter("SELECT * FROM [" + strfilename + "]", strConn);
            }
            else
            {
                // myData = new OleDbDataAdapter("SELECT * FROM [" + v_sSheetName + "$]", strConn);
                myData = new OleDbDataAdapter("SELECT * FROM [" + strfilename + "]", strConn);
            }

            myData.TableMappings.Add("Table", v_sSheetName);
            myData.Fill(myDataset);

            return myDataset;

        }
    }
}